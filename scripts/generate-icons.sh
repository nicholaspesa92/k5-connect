#!/bin/bash
#
# Copyright (C) 2014 Wenva <lvyexuwenfa100@126.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

set -e

#SRC_FILE="$1"
#DST_PATH="$2"
#ICON_PATH= "$(cd "$(dirname "$0")/../src/assets/icons"; pwd";
#ICON_PATH="$(cd "$(dirname "$0")/../src/assets/icons"; pwd)/"
ICON_FILE="icon.svg"
SCRIPT_PATH="`dirname \"$0\"`"
SRC_FILE="$(cd "$(dirname "$0")/../src/assets/icons/sources"; pwd)/$(basename $ICON_FILE)"
DST_PATH="$(cd "$(dirname "$0")/../src/assets/icons"; pwd)/"

VERSION=1.0.0

info() {
     local green="\033[1;32m"
     local normal="\033[0m"
     echo -e "[${green}INFO${normal}] $1"
}

error() {
     local red="\033[1;31m"
     local normal="\033[0m"
     echo -e "[${red}ERROR${normal}] $1"
}

usage() {
cat << EOF
VERSION: $VERSION
USAGE:
    $0 srcfile dstpath

DESCRIPTION:
    This script generates all known app icons.

    This script is depend on ImageMagick. So you must install ImageMagick first
    You can use 'brew install ImageMagick' to install it

    On Windows I have no idea... ;)
EOF
}

missing_source() {
cat << EOF
VERSION: $VERSION

MISSING SOURCE FILE $SRC_FILE

EOF
}

# Check ImageMagick
command -v convert >/dev/null 2>&1 || { error >&2 "The ImageMagick is not installed. Please use brew to install it first."; exit -1; }

## Check param
#if [ $# != 2 ];then
#    usage
#    exit -1
#fi
#
## Check dst path whether exist.
#if [ ! -d "$DST_PATH" ];then
#    mkdir -p "$DST_PATH"
#fi
if [ ! -e "$SRC_FILE" ];then
  missing_source
  exit -1
fi
#echo $SRC_FILE
#echo $DST_PATH
#exit 0

# Generate, refer to:https://developer.apple.com/library/ios/qa/qa1686/_index.html

info 'Copy app-icon.svg'
cp "$SRC_FILE" "$DST_PATH/app-icon.svg"

info 'Generate android-chrome-512x512.png'
convert -background none -size 512x512 "$SRC_FILE" "$DST_PATH/android-chrome-512x512.png"

info 'Generate android-chrome-256x256.png'
convert -background none -size 256x256 "$SRC_FILE" "$DST_PATH/android-chrome-256x256.png"

info 'Generate android-chrome-192x192.png'
convert -background none -size 192x192 "$SRC_FILE" "$DST_PATH/android-chrome-192x192.png"

info 'Generate app-con-32x32.png'
convert -background none -size 32x32 "$SRC_FILE" "$DST_PATH/app-icon-32x32.png"

info 'Generate app-con-16x16.png'
convert -background none -size 16x16 "$SRC_FILE" "$DST_PATH/app-icon-16x16.png"

info 'Generate apple-touch-icon.png'
convert -background none -size 180x180 "$SRC_FILE" "$DST_PATH/apple-touch-icon.png"

info 'Generate apple-touch-icon-120x120.png'
convert -background none -size 120x120 "$SRC_FILE" "$DST_PATH/apple-touch-icon-120x120.png"

info 'Generate apple-touch-icon-152x152.png'
convert -background none -size 152x152 "$SRC_FILE" "$DST_PATH/apple-touch-icon-152x152.png"

info 'Generate apple-touch-icon-180x180.png'
convert -background none -size 180x180 "$SRC_FILE" "$DST_PATH/apple-touch-icon-180x180.png"

info 'Generate apple-touch-icon-150x150.png'
convert -background none -size 150x150 "$SRC_FILE" "$DST_PATH/mstile-150x150.png"

info 'Generate favicon.ico @64px,48px,32px,16px'
convert -density 1024x1024 -background transparent $SRC_FILE -define icon:auto-resize=64,48,32,16 -colors 256 "$DST_PATH/../../favicon.ico"

info 'Generate Done.'
