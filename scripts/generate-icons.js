#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const cloudinary = require('cloudinary');
const fetch = require('node-fetch');

const defaultIcon = 'https://firebasestorage.googleapis.com/v0/b/connect-assets/o/app-defaults%2Ficons%2Fconsumer-app.png?alt=media&token=cdf3296a-a809-4790-a9db-4179e44ec239';
const imageSrc = process.env.APP_ICON_URL || defaultIcon;

cloudinary.config({
    cloud_name: 'cconnect',
    api_key: '977482764544573',
    api_secret: 'Dr9GyeUMb4xY__TNNJj4_ItBglM'
});

const specs = [
    {
        name: "app-icon-32x32",
        type: "png",
        size: 32
    },
    {
        name: "app-icon-64x64",
        type: "png",
        size: 64
    },
    {
        name: "app-icon-128x128",
        type: "png",
        size: 128
    },
    {
        name: "app-icon-192x192",
        type: "png",
        size: 192
    },
    {
        name: "app-icon-256x256",
        type: "png",
        size: 256
    },
    {
        name: "app-icon-512x512",
        type: "png",
        size: 512
    },
    {
        name: "favicon",
        type: "ico",
        size: 32
    },
];

const destPath = path.resolve(__dirname, '../src/assets/icons');
let tasks = [];
for (spec of specs) {
    tasks.push(createImage(imageSrc, destPath, spec));
}
Promise.all(tasks)
    .then(() => {
        console.log('Done');
    })
    .catch(error => {
        logError(error);
        throw new Error(error);
    });

async function createImage(srcPath, destPath, spec) {
    return new Promise(async (resolve, reject) => {
        const data = await getImageData(srcPath, spec);
        const destFile = path.resolve(destPath, spec.name + '.' + spec.type);
        const dest = fs.createWriteStream(destFile);
        data.body.pipe(dest);
        dest.on("finish", () => {
            console.log('Created', destFile);
            resolve();
        });
        dest.on("error", reject);
    });
}

async function getImageData(imageURL, spec) {
    let width, height;
    if (Array.isArray(spec.size)) {
        width = spec.size[0];
        height = spec.size[1];
    } else {
        width = height = spec.size;
    }
    const url = cloudinary.url(imageURL, {
        width,
        height,
        type: "fetch",
        effect: "make_transparent",
        fetch_format: spec.type,
        crop: "fill"
    })
    // return url;
    return await fetch(url)
}

function logError(error) {
    // console.error(error);
}

async function getBusinessIcon(businessId) {
    const imageSrc = 'https://firebasestorage.googleapis.com/v0/b/cannafowebsite.appspot.com/o/businesses%2FsXL4qoOzUtBtSNwA5sXY%2Ficon%2F19143054_176814056185844_3276535500418895697_o.png?alt=media&token=bc55d004-8421-4031-92df-3bfeffba5c95';
    return imageSrc;
}
// const foo = cloudinary.url( iconSrc, { 
//     width: 100, 
//     height: 150, 
//     type: "fetch",
//     crop: "fill" 
// })
// console.log(foo);
