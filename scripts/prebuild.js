#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const ejs = require('ejs');

const environmentFilesDirectory = path.join(__dirname, '../src/environments');
const targetEnvironmentTemplateFileName = 'environment.ts.template';
const targetEnvironmentProductionFileName = 'environment.prod.ts';
const targetEnvironmentStagingFileName = 'environment.staging.ts';

// Define default values in case there are no defined ones,
// but you should define only non-crucial values here,
// because build should fail if you don't provide the correct values
// for your production environment
const revision = process.env.BITBUCKET_COMMIT || require('child_process')
  .execSync('git rev-parse --short HEAD')
  .toString().trim()

const defaultStagingEnvValues = {
    PRODUCTION_BUILD: true,
    BUILD_COMMIT: process.env.BITBUCKET_COMMIT || `s-${revision}`,
    BUILD_NUMBER: process.env.BITBUCKET_BUILD_NUMBER || '000',
    BUSINESS_ID: process.env.DEVELOPMENT_BUSINESS_ID || 'sXL4qoOzUtBtSNwA5sXY',
    FUNCTIONS_URL: 'https://us-central1-cannafowebsite.cloudfunctions.net',
    FIREBASE_CONFIG: {
        apiKey: 'AIzaSyBoYFmIvCdV82PN5hC2ZWrmp8-tb8LH70c',
        authDomain: 'cannafowebsite.firebaseapp.com',
        databaseURL: 'https://cannafowebsite.firebaseio.com',
        projectId: 'cannafowebsite',
        storageBucket: 'cannafowebsite.appspot.com',
        messagingSenderId: '567687561731'
    }
};

const defaultProductionEnvValues = {
    PRODUCTION_BUILD: true,
    BUILD_COMMIT: process.env.BITBUCKET_COMMIT || `p-${revision}`,
    BUILD_NUMBER: process.env.BITBUCKET_BUILD_NUMBER || '000',
    BUSINESS_ID: process.env.PRODUCTION_BUSINESS_ID || 'sXL4qoOzUtBtSNwA5sXY',
    FUNCTIONS_URL: 'https://us-central1-connect-admin-18d73.cloudfunctions.net',
    FIREBASE_CONFIG: {
        apiKey: 'AIzaSyDilRsTdUzHqB6J2y2W-0Np8y2LaXz1p1s',
        authDomain: 'connect-admin-18d73.firebaseapp.com',
        databaseURL: 'https://connect-admin-18d73.firebaseio.com',
        projectId: 'connect-admin-18d73',
        storageBucket: 'connect-admin-18d73.appspot.com',
        messagingSenderId: '1350153448'
    }
};

// Load template file
const environmentTemplate = fs.readFileSync(
    path.join(environmentFilesDirectory, targetEnvironmentTemplateFileName),
    { encoding: 'utf-8' }
);

// Generate output data
const outputStaging = ejs.render(environmentTemplate, Object.assign({}, defaultStagingEnvValues, process.env))
    // Remove empty lines from comments
    .replace(/^\s*[\r\n]/gm, '')
    // Indent stringified braces
    .replace(/^(},?)$/gm, '  $1');

const outputProduction = ejs.render(environmentTemplate, Object.assign({}, defaultProductionEnvValues, process.env))
    // Remove empty lines from comments
    .replace(/^\s*[\r\n]/gm, '')
    // Indent stringified braces
    .replace(/^(},?)$/gm, '  $1');

// console.log(output);
// Write environment file
fs.writeFileSync(path.join(environmentFilesDirectory, targetEnvironmentStagingFileName), outputStaging);
fs.writeFileSync(path.join(environmentFilesDirectory, targetEnvironmentProductionFileName), outputProduction);

process.exit(0);