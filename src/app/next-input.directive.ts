import { Directive, ElementRef, Input, HostListener, Renderer, ViewChild } from '@angular/core';

@Directive({
  selector: '[appNextInput]'
})
export class NextInputDirective {

  private el: ElementRef;

  constructor(private _el: ElementRef, public renderer: Renderer) {
      this.el = this._el;
  }
  @HostListener('keyup', ['$event']) onKeyUp(e: any) {

    console.log(e);
    if (this.el.nativeElement.name !== '6' && this.el.nativeElement.value !== '') {
        e.path[4].nextSibling.nextSibling.children[0].children[0].children[0].children[0].focus();
    } else if (this.el.nativeElement.name === '6' && this.el.nativeElement.value !== '') {
        console.log('Submit form');
    }

    if (e.key === 'Backspace' && this.el.nativeElement.name !== '1') {
        e.path[4].previousSibling.previousSibling.children[0].children[0].children[0].children[0].focus();
        // e.path[4].previousSibling.children[0].children[0].children[0].children[0].value = null;
    }
  }

}
