import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable ,  Subscription } from 'rxjs';

import { LocationService } from './../../services/location.service';
import { AuthenticationService } from './../../services/authentication.service';
import { MenuService } from './../../services/menu.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';

import { BusinessLocation } from './../../models/businesslocation';
import { Menu } from './../../models/menu';

import { environment } from './../../../environments/environment';

import { locationSvg, productSvg, rewardSvg, activitySvg } from './../../ui-elements/home-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  day: any;

  lId: string;
  locations$: Observable<BusinessLocation[]>;
  locations: BusinessLocation[];
  locSub: Subscription;
  openTime: any;
  closeTime: any;
  hours: any[];
  lat: any;
  lon: any;
  location: any;

  menu$: any|Observable<Menu[]>;
  menu: any|Menu[];
  menuSub: Subscription;

  bSettings$: Observable<any>;
  bSettings: any;
  bSettingsSub: Subscription;

  subs: any;

  weedplaceholder: string;
  cloudinaryBase: string;
  googleNavigateBase: string;

  svgs: any[];

  constructor(
    private router: Router,
    private aRoute: ActivatedRoute,
    private ls: LocationService,
    private as: AuthenticationService,
    private ms: MenuService,
    private urlS: UrlSanitizerService
  ) {
    this.day = new Date().getDay();
    this.weedplaceholder = environment.weedPlaceholder;
    this.hours = [];
    this.cloudinaryBase = 'https://res.cloudinary.com/cconnect/image/fetch/c_fill,h_200,w_250/f_webp/';
    this.googleNavigateBase = 'https://www.google.com/maps/dir/?api=1';
  }

  ngOnInit() {
    /*
    this.aRoute.params.subscribe((params) => {
      this.lId = params['lId'];
      this.startSubs();
    });
    */

    this.lId = localStorage.getItem('lId');
    if (this.lId) {
      this.combined();
    } else {
      this.nested();
    }

    this.svgs = [
      this.urlS.getSanitizedHtml(locationSvg),
      this.urlS.getSanitizedHtml(productSvg),
      this.urlS.getSanitizedHtml(rewardSvg),
      this.urlS.getSanitizedHtml(activitySvg)
    ];
  }

  startSubs() {
    this.locations$ = this.ls.locationsAsObservableWithId();
    this.menu$ = this.ms.getMenuItems(this.lId);
    this.bSettings$ = this.as.getCurrentBusinessSettings();
    this.subs = Observable.combineLatest(this.locations$, this.menu$, this.bSettings$);
    this.subs.subscribe(([l, m, s]) => {
      this.handleLocations(l);
      this.handleMenu(m);
      this.bSettings = s;
    });
  }

  combined() {
    this.locations$ = this.ls.locationsAsObservableWithId();
    // this.menu$ = this.ms.getMenuItems(this.lId);
    this.menu$ = this.ms.getBusinessMenuItems(this.lId);
    this.bSettings$ = this.as.getCurrentBusinessSettings();
    this.subs = Observable.combineLatest(this.locations$, this.menu$, this.bSettings$);
    this.subs.subscribe(([l, m, s]) => {
      this.handleLocations(l);
      this.handleMenu(m);
      this.bSettings = s;
    });
  }

  nested() {
    this.locations$ = this.ls.locationsAsObservableWithId();
    this.bSettings$ = this.as.getCurrentBusinessSettings();
    this.subs = Observable.combineLatest(this.locations$, this.bSettings$);
    this.subs.subscribe(([l, s]) => {
      this.lId = l[0].id;
      this.handleLocations(l);
      // this.menu$ = this.ms.getMenuItems(l[0].id);
      this.menu$ = this.ms.getBusinessMenuItems(this.lId);
      this.menu$.subscribe((m) => {
        this.handleMenu(m);
      });
      this.bSettings = s;
    });
  }

  endSubs() {
    if (this.subs) { this.subs.unsubscribe(); }
  }

  handleMenu(menu) {
    const rm = menu.slice(0, 4);
    rm.map((item) => {
      if (item.photoUrl) {
        item.photoUrl = this.urlS.getSanitizedStyle(item.photoUrl);
      } else {
        item.photoUrl = this.urlS.getSanitizedStyle(this.ms.getBackDrop(item));
      }
      item.variations = this.ms.sortAndOrderVariations(item.variations);
      const noNum = item.name.replace(/[0-9]/g, '').trim().charAt(0);
      noNum === '' ? item.letter = item.category.trim().charAt(0) : item.letter = noNum;
    });
    this.menu = rm;
  }

  handleLocations(locations) {
    this.locations = locations;
    const x = locations.filter(item => item.id === this.lId);
    this.location = x[0];
    this.openTime = this.formatTime(x[0].hours[this.days[this.day]].openTime);
    this.closeTime = this.formatTime(x[0].hours[this.days[this.day]].closeTime);
    this.hours = [];
    for (let i = 0; i < Object.keys(x[0].hours).length; i++) {
      const day = {
        day: this.days[i],
        open: x[0].hours[this.days[i]].open,
        openTime: this.formatTime(x[0].hours[this.days[i]].openTime),
        closeTime: this.formatTime(x[0].hours[this.days[i]].closeTime)
      };
      this.hours.push(day);
    }
    if (x[0].location) {
      this.lat = +x[0].location.latitude ? +x[0].location.latitude : null;
      this.lon = +x[0].location.longitude ? +x[0].location.longitude : null;
    }
  }

  sanitizeImage(url) {
    if (url === undefined || url === null || url === '') { return this.urlS.getSanitizedStyle(''); }
    return this.urlS.getSanitizedStyle(url);
  }

  getFirstPrice(variations) {
    return this.ms.sortAndOrderVariations(variations)[0];
  }

  formatTime(time) {
    const t = time.split(':');
    return new Date().setHours(t[0], t[1]);
  }

  breakupText(text) {
    return text.split('<br>')[0].replace(/-/g, '<br>');
  }

  gotoLocation() {
    if (this.locations.length === 1) {
      this.router.navigate(['consumer/locations', this.locations[0].id]);
    } else {
      this.router.navigate(['consumer/locations']);
    }
  }

}
