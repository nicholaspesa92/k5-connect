import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MatButtonModule, MatIconModule, MatCardModule, MatSnackBarModule
} from '@angular/material';
import { SwiperModule } from 'angular2-useful-swiper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PhoneNumberPipe } from '../../shared/phone-number.pipe';
import { HomeComponent } from './home.component';
import { ElementRef } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { AuthenticationService } from './../../services/authentication.service';
import { LocationService } from './../../services/location.service';
import { MenuService } from './../../services/menu.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../../environments/environment';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockSwiper: any;

  beforeEach(async(() => {
    (<any>window).Swiper = (element: any, options: any) => {
        mockSwiper = {
            element: element,
            options: options,
            update() {
            }
        };
        spyOn(mockSwiper, 'update');
        return mockSwiper;
    };

    spyOn((<any>window), 'Swiper').and.callThrough();

    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        PhoneNumberPipe
      ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatSnackBarModule,
        RouterTestingModule,
        SwiperModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        AgmCoreModule.forRoot({
          apiKey: environment.firebase.apiKey,
          libraries: ['places']
        }),
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig)
      ],
      providers: [
        AuthenticationService,
        LocationService,
        MenuService,
        UrlSanitizerService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
