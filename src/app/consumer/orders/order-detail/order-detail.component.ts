import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { OrderService } from './../../../services/order.service';

import { Order } from './../../../models/order';

import * as vivus from 'vivus';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit, AfterViewInit {

  lId: string;
  id: string;
  order$: Observable<Order>;
  order: Order;

  statusText: any[];
  currentStatus: string;

  fillPill: boolean;

  constructor(
    private os: OrderService,
    private aRoute: ActivatedRoute
  ) {
    this.buildStatusText();
    this.fillPill = false;
  }

  ngOnInit() {
    this.aRoute.params.subscribe((params) => {
      this.lId = params['lId'];
      this.id = params['id'];

      this.order$ = this.os.getOrder(this.lId, this.id);
      this.order$.subscribe((order) => {
        this.order = order;
        this.setStatusText(order.status);
      });
    });
  }

  ngAfterViewInit() {

  }

  cancelOrder(id) {
    this.os.cancelOrder(id)
      .then((res) => {
      }).catch((error) => {
      });
  }

  buildStatusText() {
    this.statusText = [
      'Your order has been received and we will begin filling it shortly!',
      'Your order is being filled now!',
      'Your order is ready! Come pick it up!'
    ];
  }
  setStatusText(status) {
    switch (status) {
      case 'new':
        this.currentStatus = this.statusText[0];
        break;
      case 'pending':
        this.currentStatus = this.statusText[1];
        break;
      case 'ready':
        this.currentStatus = this.statusText[2];
        break;
    }
  }

  isLiveOrder() {
    return (this.order.status === 'new' || this.order.status === 'pending' || this.order.status === 'ready')
      && this.order.canceled === false && this.order.archived === false;
  }

  animate() {

  }

}
