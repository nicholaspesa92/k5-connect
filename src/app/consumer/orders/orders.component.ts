import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from './../../services/authentication.service';
import { OrderService } from './../../services/order.service';
import { LocationService } from './../../services/location.service';

import { Order } from './../../models/order';
import { BusinessLocation } from '../../models/businesslocation';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  lId: string;
  uid: string;

  orders$: Observable<Order[]>;
  orders: Order[];

  activeOrders: Order[];
  pastOrders: Order[];
  order: Order;

  authFlag: boolean;
  statuses: any[];

  path: string;

  locations$: Observable<BusinessLocation[]>;
  locations: BusinessLocation[];

  constructor(
    private as: AuthenticationService,
    private os: OrderService,
    private ls: LocationService,
    private aRoute: ActivatedRoute,
    private router: Router
  ) {
    this.statuses = ['new', 'pending', 'ready', 'complete', 'rejected'];
    this.path = 'orders';
    this.orders = [];
    this.activeOrders = [];
    this.pastOrders = [];
  }

  ngOnInit() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.uid = user.uid;
          this.authFlag = true;
          this.startSubs();
        } else if (user.isAnonymous) {
          this.authFlag = false;
        }
      }
    });
  }

  startSubs() {
    this.locations$ = this.ls.locationsAsObservableWithId();
    this.locations$.subscribe((locations) => {
      this.locations = locations;
      this.lId = localStorage.getItem('lId');
      if (this.lId) {
        this.getOrders(this.lId, this.uid);
      } else {
        // No Orders
      }
    })
  }

  getOrders(lId, uid) {
    this.orders$ = this.os.getOrders(lId, uid);
    this.orders$.subscribe((orders) => {
      this.orders = orders;
      this.getLiveOrders(orders);
      this.getPastOrders(orders);
      console.log(this.orders);
    });
  }

  getLiveOrders(orders) {
    this.activeOrders = orders.filter(item => this.liveOrder(item));
    this.order = this.activeOrders[0];
  }

  getPastOrders(orders) {
    this.pastOrders = orders.filter(item => this.pastOrder(item));
  }

  liveOrder(item) {
    return item.status !== this.statuses[3] && item.status !== this.statuses[4] && item.canceled === false && item.archived === false;
  }

  pastOrder(item) {
    return item.status === this.statuses[3] || item.status === this.statuses[4] || (item.archived === true && item.canceled === false);
  }

  goto(id) {
    this.router.navigate(['consumer', this.lId, 'orders', id]);
  }

}
