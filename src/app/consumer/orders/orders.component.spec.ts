import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule, MatStepperModule, MatSnackBarModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { OrdersComponent } from './orders.component';
import { BottomSheetComponent } from './../../bottom-sheet/bottom-sheet.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';

import { AuthenticationService } from './../../services/authentication.service';
import { OrderService } from './../../services/order.service';
import { UrlService } from './../../services/url.service';
import { LocationService } from './../../services/location.service';

import { StatusBarComponent } from './../../ui-elements/status-bar/status-bar.component';

describe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersComponent, BottomSheetComponent, StatusBarComponent ],
      imports: [
        MatIconModule,
        MatStepperModule,
        MatSnackBarModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [
        OrderService,
        AuthenticationService,
        UrlService,
        LocationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
