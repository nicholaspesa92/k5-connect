import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatSnackBarModule,
  MatExpansionModule,
  MatCardModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsComponent } from './details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';

import { AuthenticationService } from './../../../services/authentication.service';
import { LocationService } from './../../../services/location.service';
import { ToolbarService } from './../../../services/toolbar.service';
import { MemberService } from './../../../services/member.service';
import { TitlePipe } from './../../../shared/title.pipe';
import { UrlSanitizerPipe } from './../../../shared/url-sanitizer.pipe';
import { GooglePlacesService } from './../../../services/google-places.service';
import { UrlSanitizerService } from './../../../services/url-sanitizer.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

import { environment } from './../../../../environments/environment';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsComponent, TitlePipe, UrlSanitizerPipe ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatCardModule,
        MatProgressSpinnerModule,
        FlexLayoutModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        AgmCoreModule.forRoot({
          apiKey: environment.firebase.apiKey,
          libraries: ['places']
        }),
        HttpClientModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig)
      ],
      providers: [
        AuthenticationService,
        LocationService,
        ToolbarService,
        MemberService,
        GooglePlacesService,
        UrlSanitizerService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
