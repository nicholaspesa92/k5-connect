import { Component, OnInit, HostBinding, AfterViewInit, ElementRef, ViewChild, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { slideRightToLeft, crazyAnimation } from '../../../shared/animation';
import { Observable } from 'rxjs';

import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { AuthenticationService } from './../../../services/authentication.service';
import { LocationService } from './../../../services/location.service';
import { ToolbarService } from './../../../services/toolbar.service';
import { MemberService } from './../../../services/member.service';
import { GooglePlacesService } from './../../../services/google-places.service';
import { UrlSanitizerService } from './../../../services/url-sanitizer.service';

import { BusinessLocation } from './../../../models/businesslocation';

import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [slideRightToLeft]
})
export class DetailsComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  bgImg = 'https://cdn.shopify.com/s/files/1/1249/2029/t/7/assets/slideshow_3.jpg?16759369221180309307';
  id: number;
  private sub: any;
  rating = 0;
  newRating = 0;

  business: Observable<BusinessLocation>;
  hours = {};
  days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  day;
  hoursWithDates = [];
  openToday: number;
  closeToday: number;
  sanPhoto: any;

  addressUrl: string;
  latlon: string;
  lat: number;
  lon: number;

  mapsApiKey: string;
  styles: any;
  gPlaceId: string;
  placeDetails: any;
  rAvg: number;
  loadingReviews: boolean;
  bSettings$: any;
  bSettings: any;

  reviews: any[];

  lastCheckin: any;

  favoriteLocation: string;

  constructor(
    private route: ActivatedRoute,
    private as: AuthenticationService,
    private ls: LocationService,
    private ms: MemberService,
    private sanitizer: DomSanitizer,
    private ts: ToolbarService,
    private gpService: GooglePlacesService,
    private urlS: UrlSanitizerService,
    private changeDetec: ChangeDetectorRef,
    private router: Router
  ) {
    this.favoriteLocation = '';
  }

  ngOnInit() {
    this.reviews = [];
    this.lat = null;
    this.lon = null;
    this.rAvg = 4.2;
    this.day = new Date().getDay();
    this.loadingReviews = true;
    this.sub = this.route.params.subscribe(params => {
       this.id = params['id']; // (+) converts string 'id' to a number
       this.ls.currentLocation = params['id'];
    });
    this.favoriteLocation = this.ls.favoriteLocation;
    this.getLocationDetails();
    this.getMemberData();
    this.getRating();
    this.mapsApiKey = environment.firebase.apiKey;
    this.gPlaceId = 'ChIJcTYUL0l9x1QR2x4bE2TFZ54';
    this.as.user.subscribe((user) => {
      if (user) {
      }
    });
    this.setMapsStyles();
  }

  ngAfterViewInit() {
    if (this.ts.locationsAmount === 1) { this.ts.setToolbarArrow(true); }
  }

  ngAfterViewChecked() {

  }

  getLocationDetails() {
    this.business = this.ls.getLocationDetails(this.id);
    this.business.subscribe((data: any) => {
      this.addressUrl = data.city + ', ' + data.state;
      // this.latlon = data.location.latitude + ',' + data.location.longitude;
      if (data.location) {
        this.lat = +data.location.latitude ? +data.location.latitude : null;
        this.lon = +data.location.longitude ? +data.location.longitude : null;
      }
      data.photoUrl ? this.sanPhoto = this.urlS.getSanitizedStyle(data.photoUrl) : console.log('no-photo');
      this.ts.setNextTitle(data.name);
      this.constructHoursArray(data.hours);

      data.googlePlaceId ? this.getPlaceDetailsX(data.googlePlaceId) : this.loadingReviews = false;
    });
  }

  constructHoursArray(data) {
    this.openToday = this.convertToDate(data[this.days[this.day]].openTime);
    this.closeToday = this.convertToDate(data[this.days[this.day]].closeTime);
    for (let i = 0; i < Object.keys(data).length; i++) {
      const hoursObj = {};
      hoursObj['open'] = data[this.days[i]].open;
      hoursObj['openTime'] = this.convertToDate(data[this.days[i]].openTime);
      hoursObj['closeTime'] = this.convertToDate(data[this.days[i]].closeTime);
      this.hoursWithDates.push(hoursObj);
    }
  }

  getMemberData() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.ms.getMember(user.uid).subscribe((member) => {
            this.lastCheckin = new Date(member.lastCheckin);
          });
        }
      }
    });
  }

  getPlaceDetailsX(pId) {
    this.as.getUserToken().then((res) => {
      const x = this.gpService.getPlaceDetails(pId, res);
      x.subscribe((place: any) => {
        this.loadingReviews = false;
        this.rAvg = place.result.rating;
        place.result.reviews.map((review) => {
          review.profile_photo_url = this.sanitizeReviewImageStyle(review.profile_photo_url);
        });
        this.placeDetails = place;
        console.log(place);
      });
    }).catch((error) => {
      console.log(error);
      this.loadingReviews = false;
    });
  }

  getPlaceDetails() {
    this.as.getUserToken().then((res) => {
      const x = this.gpService.getGooglePlaceDetails(res);
      x.subscribe((place: any) => {
        this.placeDetails = place;
        this.rAvg = place.result.rating;
        console.log(place);
        this.loadingReviews = false;
      });
    }).catch((error) => {
      console.log(error);
      this.loadingReviews = false;
    });
  }

  getSanitizedStaticMap() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.getMapsUrl());
  }

  getMapsUrl() {
    return 'https://maps.googleapis.com/maps/api/staticmap?'
      + 'center=' + this.latlon
      + '&size=' + this.getStaticMapWidth() + 'x400'
      + '&markers=color:blue%7C' + this.latlon
      + '&key=' + this.mapsApiKey;
  }

  convertToDate(time) {
    const t = time.split(':');
    return new Date().setHours(t[0], t[1]);
  }

  getStaticMapWidth() {
    return window.innerWidth < 960 ? window.innerWidth : 375;
  }

  gotoExternalWebsite(url) {
    const sep = url.split('');
    console.log(sep);
    this.router.navigate([url]);
  }

  setNewRating(num) {
    this.newRating = num;
  }

  getRating() {
    for (let i = 0; i < this.reviews.length; i++) {
      this.rating += this.reviews[i].rating;
    }
    this.rating = (this.rating / this.reviews.length);
    this.rating = Math.round(this.rating * 2) / 2;
  }

  getSanitizedUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.google.com/maps/embed/v1/place?' +
    'key=AIzaSyAT7TgYzWDgrVLzh9eX5Q-KtPPQULsjxsU&q=' + this.addressUrl);
  }

  getScreenWidth() {
    return window.innerWidth;
  }

  getStar(i) {
    return i <= this.rAvg ? 'star' : 'star_border';
  }

  getAuthorStar(i, r) {
    return i <= r ? 'star' : 'star_border';
  }

  getStarWithRating(i, r) {
    return i <= r ? 'star' : 'star_border';
  }

  sanitizeReviewImageStyle(url) {
    return this.urlS.getSanitizedStyle(url);
  }

  setMapsStyles() {
    this.styles = [
      {
          'featureType': 'all',
          'elementType': 'all',
          'stylers': [
              {
                  'hue': '#e7ecf0'
              }
          ]
      },
      {
          'featureType': 'poi',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'poi',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              },
              {
                  'hue': '#0053ff'
              }
          ]
      },
      {
          'featureType': 'poi',
          'elementType': 'labels.icon',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.attraction',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.business',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.government',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.medical',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.park',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.place_of_worship',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.school',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'poi.sports_complex',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'on'
              }
          ]
      },
      {
          'featureType': 'road',
          'elementType': 'all',
          'stylers': [
              {
                  'saturation': -70
              }
          ]
      },
      {
          'featureType': 'transit',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'water',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'simplified'
              },
              {
                  'saturation': -60
              }
          ]
      }
    ];
  }

}
