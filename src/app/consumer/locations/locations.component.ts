import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { slideLeftToRight } from '../../shared/animation';
import { Observable } from 'rxjs';

import { LocationService } from './../../services/location.service';
import { BusinessLocation } from './../../models/businesslocation';
import { ToolbarService } from './../../services/toolbar.service';
import { Review } from './../../models/review';

import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
  animations: [slideLeftToRight]
})
export class LocationsComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  blocations: Observable<BusinessLocation[]>;
  locations: BusinessLocation[];
  totalLocations: number;
  reviews: Observable<Review[]>;

  ylocations: BusinessLocation[];

  placeholder: string;

  constructor(
    private ls: LocationService,
    private router: Router,
    private ts: ToolbarService
  ) {
    this.placeholder = environment.weedPlaceholder;
    this.locations = [];
  }

  ngOnInit() {
    this.blocations = this.ls.locationsAsObservableWithId();
    this.blocations.subscribe((locations) => {
      locations.map((location) => {
        // location.photoUrl ? location.photoUrl = location.photoUrl : location.photoUrl = this.placeholder;
      });
      this.locations = locations;
    });
  }

  getLocationsWithArrayLength() {
    this.ls.locationsAsObservable()
      .subscribe(xlocations => {
        this.ylocations = xlocations;
      });
  }

  gotoLocation(location) {
    this.router.navigate([location.id]);
  }

}
