import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  public messages = [
    {
      id: 1,
      location: 'Portland',
      timestamp: 'now',
      message: 'Special only today $5 off any purchase.' +
      'Come in today before we close at 7pm and mention this offer.' +
      'We look forward to seeing your pretty faces!'
    },
    {
      id: 2,
      location: 'Portland',
      timestamp: 'now',
      message: 'Today $10 off any purchase of $100 or more. Come see us today'
    },
    {
      id: 3,
      location: 'Portland',
      timestamp: 'now',
      message: 'Special only today $5 off any purchase. Come in today before we close at 7pm and mention this offer. '
    },
    {
      id: 4,
      location: 'Portland',
      timestamp: 'now',
      message: 'Only today $5 off any purchase. Come in today and check out our new flower. Also now stocking concentrates. '
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
