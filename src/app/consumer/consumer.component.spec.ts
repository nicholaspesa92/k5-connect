import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatIconModule, MatMenuModule, MatSidenavModule, MatToolbarModule,
  MatListModule, MatCardModule, MatInputModule, MatSelectModule, MatTabsModule, MatDialogModule,
  MatSnackBarModule, MatBottomSheetModule, MatBadgeModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterTestingModule } from '@angular/router/testing';
import { ConsumerComponent } from './consumer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SearchStringService } from '../services/search-string.service';
import { AuthenticationService } from '../services/authentication.service';
import { FiltersService } from '../services/filters.service';
import { ToolbarService } from './../services/toolbar.service';
import { LocationService } from './../services/location.service';
import { UrlService } from './../services/url.service';
import { OrderService } from './../services/order.service';
import { RewardService } from './../services/reward.service';
import { MemberService } from './../services/member.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../environments/environment';

describe('ConsumerComponent', () => {
  let component: ConsumerComponent;
  let fixture: ComponentFixture<ConsumerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumerComponent, NavigationComponent ],
      imports: [
        NoopAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatTabsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatBottomSheetModule,
        MatBadgeModule,
        FlexLayoutModule,
        RouterTestingModule,
        FormsModule,
        AngularFirestoreModule,
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebase),
      ],
      providers: [
        SearchStringService,
        FiltersService,
        ToolbarService,
        AuthenticationService,
        LocationService,
        UrlService,
        OrderService,
        RewardService,
        MemberService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
