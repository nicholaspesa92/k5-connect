import { Component, OnInit } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';

import { AuthenticationService } from './../../services/authentication.service';
import { LocationService } from './../../services/location.service';
import { ToolbarService } from './../../services/toolbar.service';
import { BusinessLocation } from './../../models/businesslocation';
import { UrlService } from './../../services/url.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  businessSettings: Observable<{}>;
  showHome: boolean;
  showArticles: boolean;

  signBtnText: string;
  signBtnFlag: boolean;

  bLocations: Observable<BusinessLocation[]>;
  totalLocations: number;
  singleLocationId: string;

  constructor(
    private as: AuthenticationService,
    private ls: LocationService,
    private ts: ToolbarService,
    private router: Router,
    private us: UrlService
  ) {
    this.showHome = false;
    this.showArticles = true;
    this.signBtnText = 'Sign In';
    this.signBtnFlag = false;
    this.totalLocations = 0;
    this.singleLocationId = '';
  }

  ngOnInit() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (user.isAnonymous) {
          this.signBtnText = 'Sign In';
          this.signBtnFlag = false;
        } else if (!user.isAnonymous) {
          this.signBtnText = 'Sign Out';
          this.signBtnFlag = true;
        }
      }
    });
    this.businessSettings = this.as.getCurrentBusinessSettings();
    this.businessSettings.subscribe((setting: any) => {
      this.showHome = setting.homepage;
      this.showArticles = setting.articles;
    });
    this.getLocationsAmount();
  }

  handleSignBtn() {
    if (this.signBtnFlag) {
      this.as.signOut();
    } else if (!this.signBtnFlag) {
      const navExtras: NavigationExtras = {
        queryParams: { 'redirectUrl' : this.us.lastPath }
      }
      this.router.navigate(['onboarding'], navExtras);
    }
  }

  getLocationsAmount() {
    this.bLocations = this.ls.locationsAsObservableWithId();
    this.bLocations.subscribe(data => {
      this.totalLocations = Object.keys(data).length;
      this.singleLocationId = data[0].id;
      this.ts.locationsAmount = this.totalLocations;
    });
  }

  handleLocationsNavLink() {
    if (this.totalLocations === 1) {
      this.router.navigate([`consumer/locations/${this.singleLocationId}`]);
    } else {
      this.router.navigate(['consumer/locations']);
    }
  }

}
