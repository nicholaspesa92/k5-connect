import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule, MatIconModule, MatListModule, MatSnackBarModule
} from '@angular/material';
import { NavigationComponent } from './navigation.component';
import { AuthenticationService } from './../../services/authentication.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';
import { RouterTestingModule } from '@angular/router/testing';
import { LocationService } from './../../services/location.service';
import { ToolbarService } from './../../services/toolbar.service';
import { UrlService } from './../../services/url.service';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationComponent ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatSnackBarModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [AuthenticationService, LocationService, ToolbarService, UrlService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
