import { Component, OnInit, Inject, Optional } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FiltersService } from '../../services/filters.service';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith ,  map } from 'rxjs/operators';
import 'rxjs-compat/add/operator/first'
import { AuthenticationService } from './../../services/authentication.service';
import { MenuService } from './../../services/menu.service';
import { Menu } from './../../models/menu';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  fg: FormGroup;
  bSettings: any|Observable<{}>;

  menu$: any|Observable<Menu[]>;
  location: string;

  types$: Observable<any>;
  types = [];

  vendors$: Observable<any>;
  filteredVendors: Observable<any>;
  vendors: any[];

  chipFilters: any;
  vendorCtrl: FormControl;

  subs: any;

  constructor(
    @Optional()
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<FilterComponent>,
    public filtersService: FiltersService,
    private fb: FormBuilder,
    private as: AuthenticationService,
    private ms: MenuService,
    private route: ActivatedRoute
  ) {
    this.types = [];
    this.chipFilters = [];
    this.vendors = [];
    this.vendorCtrl = new FormControl();
    if (data) {
      this.location = data.lId;
    }
  }


  ngOnInit() {
    this.createForm();
    this.startSubs();
  }

  handleSubs() {
    this.types$ = this.as.getCurrentBusinessTypes();
    this.menu$ = this.ms.getMenuItems(this.location).first();
    this.vendors$ = this.as.getCurrentBusinessVendors();
    this.types$.subscribe((types) => {
      const t = [];
      types.map((type) => {
        t.push(type.slug);
      });
      this.vendors$.subscribe((vendors) => {
        vendors.map((vendor) => {
          this.vendors.push(vendor.slug);
        });
        this.filteredVendors = this.vendorCtrl.valueChanges
          .pipe(
            startWith(''), map(val => this.filterCtrl(val))
          );
        this.menu$.subscribe((menu) => {
          for (let i = 0; i < t.length; i++) {
            if (this.categoryBinds(menu, t[i]).length > 0) {
              this.types.push(t[i]);
              const fGroup = <FormGroup>this.fg.get('filters');
              fGroup.addControl(t[i], new FormControl(false));
            }
          }
          this.updateForm();
        });
      });
    });
  }

  startSubs() {
    this.types$ = this.as.getCurrentBusinessTypes();
    this.menu$ = this.ms.getMenuItems(this.location).first();
    this.vendors$ = this.as.getCurrentBusinessVendors();
    this.subs = Observable.combineLatest(this.types$, this.menu$, this.vendors$);
    this.subs.subscribe(([types, menu, vendors]) => {
      const t = [];
      types.map((type) => {
        t.push(type.slug);
      });
      vendors.map((vendor) => {
        this.vendors.push(vendor.slug);
      });
      for (let i = 0; i < t.length; i++) {
        if (this.categoryBinds(menu, t[i]).length > 0) {
          this.types.push(t[i]);
          const fGroup = <FormGroup>this.fg.get('filters');
          fGroup.addControl(t[i], new FormControl(false));
        }
      }
      this.updateForm();
    });
  }

  filterCtrl(val) {
    return this.vendors.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  checkSelection(vendor) {
    return this.chipFilters.indexOf(vendor) > -1;
  }

  categoryBinds(menu, type) {
    return menu.filter(item => item.type === type);
  }

  createForm() {
    this.fg = this.fb.group({
      filters: this.fb.group({})
    });
  }

  updateForm() {
    if (this.data) {
      console.log(this.data);
      this.fg.patchValue(this.data);
      // this.data.vendors.length > 0 ? this.chipFilters = this.data.vendors : this.chipFilters = [];
      this.vendorCtrl.patchValue(this.data.vendors);
    }
  }

  buildFormGroup() {
    const ar = [];
    for (let i = 0; i < Object.keys(this.data.filters).length; i ++) {
      ar.push(this.data.filters[i]);
    }
    return this.fb.array(ar);
  }

  get filters() {
    return this.fg.get('filters') as FormArray
  }

  addChipFilter(s) {
    this.chipFilters.push(s);
    this.vendorCtrl.reset('');
  }
  removeChipFilter(s) {
    const i = this.chipFilters.indexOf(s);
    i >= 0 ? this.chipFilters.splice(i, 1) : console.log('no-item');
  }
  endChip(e) {
    console.log(e);
  }

  closeWithSave() {
    console.log(this.vendorCtrl);
    const res = {
      filters: this.fg.value.filters,
      vendors: this.vendorCtrl.value
    }
    this.dialogRef.close(res);
  }

  clearAll() {
    const res = {
      filters: {},
      vendors: []
    };
    this.dialogRef.close(res);
  }

}
