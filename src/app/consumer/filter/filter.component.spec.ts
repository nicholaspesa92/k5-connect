import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatCheckboxModule, MatDialogRef, MAT_DIALOG_DATA, MatSnackBarModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TitlePipe } from '../../shared/title.pipe';
import { FilterComponent } from './filter.component';
import { FiltersService } from '../../services/filters.service';
import { AuthenticationService } from './../../services/authentication.service';
import { MenuService } from './../../services/menu.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../../environments/environment';

describe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterComponent, TitlePipe ],
      imports: [
        BrowserAnimationsModule,
        MatDialogModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatSelectModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [
        FiltersService,
        AuthenticationService,
        { provide: MatDialogRef },
        { provide: MAT_DIALOG_DATA, use: {}},
        MenuService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
