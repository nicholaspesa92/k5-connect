import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  terms: string;

  constructor() { }

  ngOnInit() {
    this.setTerms();
  }

  setTerms() {
    this.terms = 'PLEASE READ THE FOLLOWING TERMS AND CONDITIONS.'
      + 'YOUR ACCEPTANCE OF THESE TERMS IS AN ABSOLUTE CONDITION OF YOUR USE OF THIS WEBSITE AND ITS PRODUCTS AND SERVICES.'
      + 'Shutterstock, Inc d/b/a Bigstock (“Bigstock”) maintains the www.Bigstock.com '
      // tslint:disable-next-line:max-line-length
      + 'website and Bigstock and Bigstockphoto branded sites (e.g., bigstockphoto.com, bigstockphoto.de, etc.) (collectively, the “Site”).'
      + 'Casual users of the Site may browse image files (the “Image(s)”) and license the Images (licensees are hereafter referred to as “Members”). Users, including Members, may also become content contributors (“Contributors”).'
      // tslint:disable-next-line:max-line-length
      + 'Contributors may upload image files to the Site for the purpose of having Bigstock act as Contributor’s agent for licensing image rights to Members, pursuant to the terms of the Bigstock Contributor Agreement.'
      + 'By registering with Bigstock, Members agree to bound by the terms and conditions of Bigstock’s Privacy Statement (which is incorporated herein by this reference) and the following terms and conditions, as applicable (the “Agreement”):'
  }

}
