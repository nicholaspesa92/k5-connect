import { Component, ElementRef, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, Output,
  EventEmitter, DoCheck, ChangeDetectorRef, HostListener, Inject } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSidenav,
  MatSidenavContainer,
  MatSnackBar,
  MatBottomSheet
} from '@angular/material';
import { CdkScrollable } from '@angular/cdk/scrolling';
import { query, animate , style, group, trigger, transition} from '@angular/animations';
import { ActivatedRoute, NavigationEnd, Router, NavigationExtras } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/operator/mergeMap';
import { FilterComponent } from './filter/filter.component';
import { FilterLocationComponent } from './filter-location/filter-location.component';
import { SearchStringService } from '../services/search-string.service';
import { FiltersService } from '../services/filters.service';
import { AuthenticationService } from './../services/authentication.service';
import { ToolbarService } from './../services/toolbar.service';
import { User } from './../models/user';
import { BusinessLocation } from './../models/businesslocation';
import { LocationService } from './../services/location.service';
import { UrlService } from './../services/url.service';
import { OrderService } from './../services/order.service';
import { MemberService } from './../services/member.service';
import { RewardService } from './../services/reward.service';
import { BottomSheetComponent } from './../bottom-sheet/bottom-sheet.component';

const SMALL_WIDTH_BREAKPOINT = 1024;

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.scss']
})
export class ConsumerComponent implements OnInit, OnDestroy, AfterViewInit {

  mobileQuery: MediaQueryList;
  mobileQueryListener: () => void;

  placeholder = './../../../assets/images/placeholder.png';
  showShadow = true;
  title = 'Connect';
  home = false;
  rootPage = true;
  showSearch = false;
  searchObj: any;
  pageData: any;
  search = false;
  locationFilter = false;
  dialogRef: MatDialogRef<FilterComponent> | null;
  filterString = '';
  public filters = [
    {name: 'sativa', selected: true},
    {name: 'indica', selected: true},
    {name: 'hybrid', selected: true}
  ];
  vendors: any;
  filtersx: any;

  searchString: string;

  showHome: boolean;
  showArticles: boolean;
  showFavorite: boolean;
  darkTheme: boolean;

  @ViewChild(MatSidenav) sidenav: MatSidenav;
  @ViewChild(MatSidenavContainer) sidenavContainer: MatSidenavContainer;
  @ViewChild(CdkScrollable) appContent: CdkScrollable;
  @ViewChild('locButton', { read: ElementRef }) locButton: ElementRef;

  userObs: Observable<User>;
  uid;

  bSettings: any|Observable<{}>;

  bLocations: Observable<BusinessLocation[]>;
  locations: any | BusinessLocation[];
  lId: string;
  currentLocation: any | BusinessLocation;
  canOrder: boolean;
  orderAllowed: boolean;
  hasLoyalty: boolean;

  signBtnText: string;
  signBtnFlag: boolean;

  displayName: string;
  toolbarTrans: boolean;

  ordersLength: number;

  member$: Observable<any>;
  member: any;
  rewards$: Observable<any>;
  rewards: any;
  aRewards: number;

  favoriteLocation: any;
  fav: string;

  feature: any;
  discover: boolean;

  allDataSub: any;
  localLocationSub: any;

  returnPath: any;

  constructor(
    private titleService: Title,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public searchService: SearchStringService,
    public filtersService: FiltersService,
    private element: ElementRef,
    private ts: ToolbarService,
    private as: AuthenticationService,
    private ls: LocationService,
    private us: UrlService,
    private os: OrderService,
    private cdr: ChangeDetectorRef,
    private ms: MemberService,
    private rs: RewardService,
    private media: MediaMatcher,
    private sb: MatSnackBar,
    private mb: MatBottomSheet
  ) {
    this.signBtnText = 'Sign In';
    this.signBtnFlag = false;
    this.displayName = '';
    this.toolbarTrans = false;
    this.filtersx = {};
    this.searchObj = {};
    this.showHome = false;
    this.showArticles = false;
    this.showFavorite = false;
    this.darkTheme = false;
    this.ordersLength = 0;
    this.aRewards = 0;
    this.locations = [];
    this.favoriteLocation = {};
    this.fav = '';
    this.mobileQuery = media.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`);
    this.mobileQueryListener = () => cdr.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    this.feature = {
      top: 0,
      left: 0
    };
    this.discover = false;
    this.canOrder = true;
    this.orderAllowed = true;
    this.hasLoyalty = false;
    this.vendors = [];
  }

  ngOnInit() {
    this.getRouteDataOnReload();
    this.getRouteDataOnNavigation();

    this.as.user.subscribe((user) => {
      if (user) {
        if (user.isAnonymous) {
          this.signBtnText = 'Sign In';
          this.signBtnFlag = false;
          this.startAllDataSubs();
        } else if (!user.isAnonymous) {
          this.userObs = this.as.getCurrentUserFromFirebase(user.uid);
          this.uid = user.uid;
          this.signBtnText = 'Sign Out';
          this.signBtnFlag = true;
          this.displayName = user.displayName;
          this.startAllDataSubs(user.uid);
        }
      } else {
        this.as.anonymousLogin();
      }
    });

    this.startToolbarServiceSubs();

    this.os.lengthObs().subscribe((length) => {
      this.ordersLength = length;
    });
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  ngAfterViewInit() {
    /*
    if (this.appContent) {
      this.appContent.elementScrolled().subscribe((scroll) => {
      });
    }
    */
  }

  startAllDataSubs(uid?) {
    this.bLocations = this.ls.getLocations();
    this.bSettings = this.as.getCurrentBusinessSettings();
    this.member$ = this.ms.getMember(uid);

    if (uid) {
      this.allDataSub = Observable.combineLatest(this.bLocations, this.bSettings, this.member$);
      this.allDataSub.subscribe(([locations, settings, member]) => {
        this.handleSettings(settings);
        this.handleLocations(locations);
        this.startRouting(locations);
        this.handleMember(member);
      });
    } else {
      this.allDataSub = Observable.combineLatest(this.bLocations, this.bSettings);
      this.allDataSub.subscribe(([locations, settings]) => {
        this.handleSettings(settings);
        this.handleLocations(locations);
        this.startRouting(locations);
      });
    }
  }

  startRouting(locations) {
    this.lId = localStorage.getItem('lId');
    if (this.lId) {
      const l = this.locations.filter(location => location.id === this.lId);
      if (l.length > 0) {
        this.currentLocation = l[0];
        this.currentLocation.hasOnlineOrdering ? this.canOrder = true : this.canOrder = false;
        this.ls.hasOnlineOrdering = this.canOrder;
        this.hasLoyalty = this.currentLocation.hasLoyalty;
      }
    } else {
      this.lId = locations[0].id;
      this.currentLocation = locations[0];
      this.currentLocation.hasOnlineOrdering ? this.canOrder = true : this.canOrder = false;
      this.ls.hasOnlineOrdering = this.canOrder;
      this.hasLoyalty = this.currentLocation.hasLoyalty;
      localStorage.lId = this.lId;
      localStorage.selectedCategory = 0;
    }
    this.observeLocationChange();
    const cRoute = this.removeUrlParams(this.router.routerState.snapshot.url);
    if (cRoute === '/consumer') { this.router.navigate(['consumer', (this.showHome ? 'home' : 'menu')]); }
  }

  observeLocationChange() {
    if (this.localLocationSub) { this.localLocationSub.unsubscribe(); }
    this.localLocationSub = this.ls.locationChange.asObservable().subscribe((lId) => {
      this.lId = lId;
      const l = this.locations.filter(location => location.id === this.lId);
      if (l.length > 0) {
        this.currentLocation = l[0];
        this.currentLocation.hasOnlineOrdering ? this.canOrder = true : this.canOrder = false;
        this.ls.hasOnlineOrdering = this.canOrder;
        this.hasLoyalty = this.currentLocation.hasLoyalty;
      }
    });
  }

  handleSettings(settings) {
    this.showHome = settings.homepage;
    this.showArticles = settings.articles;
    this.darkTheme = settings.hasOwnProperty('darkTheme') ? settings.darkTheme : false;
  }

  handleLocations(locations) {
    this.locations = locations;
    this.ts.locationsAmount = locations.length;
    this.resetFavoriteLocation(locations);
  }

  handleMember(member) {
    member.hasOwnProperty('orderAllowed') ? this.handleOrderAllowed(member.orderAllowed) : this.handleOrderAllowed(true);
    const cp = member.currentPoints ? member.currentPoints : 0;
    this.rewards$ = this.rs.getAvailableRewards(cp);
    this.rewards$.subscribe((amount) => {
      this.aRewards = amount;
    });
  }

  handleOrderAllowed(flag) {
    this.orderAllowed = flag;
    this.os.orderAllowed = flag;
  }

  handleUrl() {
    const l = this.locations.filter(item => item.id === this.lId);
    if (l.length > 0) {
      this.currentLocation = l[0];
      this.currentLocation.hasOnlineOrdering ? this.canOrder = true : this.canOrder = false;
      this.ls.hasOnlineOrdering = this.canOrder;
      this.hasLoyalty = this.currentLocation.hasLoyalty;
    }

    const cRoute = this.removeUrlParams(this.router.routerState.snapshot.url);
    if (cRoute === '/consumer') { this.router.navigate(['consumer', (this.showHome ? 'home' : 'menu')]); }
  }

  removeUrlParams(url) {
    const q = url.indexOf('?');
    return url.substring(0, q !== -1 ? q : url.length);
  }

  saveFavoriteLocation() {
    if (this.fav === this.ls.currentLocation) { return; }
    this.openFavoriteDialog();
  }

  openFavoriteDialog() {
    const dRef = this.dialog.open(SaveFavoriteDialogComponent);
    dRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.resetFavoriteLocation(this.locations);
        this.favoriteLocation[this.ls.currentLocation] = true;
        this.ms.saveMemberField('favoriteLocation', this.favoriteLocation, this.uid)
          .then((res) => {
          });
      }
    });
  }

  resetFavoriteLocation(locations) {
    locations.map((location) => {
      this.favoriteLocation[location.id] = false;
    });
  }

  getRouteDataOnReload() {
    if (this.route.snapshot.children[0]) {
      const myData = this.route.snapshot.children[0].data;
      if (myData) {
        // this.showShadow = myData.showShadow;
        this.title = myData.title;
        this.rootPage = myData.rootPage;
        this.showSearch = myData.showSearch;
        this.showShadow = myData.showShadow;
        this.home = myData.home;
        this.locationFilter = myData.locationFilter;
        this.toolbarTrans = myData.toolbarTrans;
        this.showFavorite = myData.showFavorite;
        this.returnPath = myData.returnPath;
        console.log(this.returnPath);
      }
    }
  }

  getRouteDataOnNavigation() {
    this.router.events
    // Only continue with navigation end events
      .filter(event => event instanceof NavigationEnd)
      // Swap the observable for the current route
      .map(() => this.route)
      // Loop the routes to find the last activated child
      .map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      })
      // Only continue for the primary router outlet
      .filter(route => route.outlet === 'primary')
      // Limit to just the data object
      .mergeMap(route => route.data)
      .subscribe((event: any) => {
        this.us.lastPath = this.router.url;
        if (event.title) {
          this.title = event.title;
          this.setTitle(event.title);
        }
        // Simple boolean
        this.home = event.home;
        this.rootPage = event.rootPage;
        this.showShadow = event.showShadow;
        this.showSearch = event.showSearch;
        this.locationFilter = event.locationFilter;
        this.toolbarTrans = event.toolbarTrans;
        this.showFavorite = event.showFavorite;
        this.returnPath = event.returnPath;
        console.log(this.returnPath);
        if (this.mobileQuery.matches) { this.sidenav.close(); }
        this.getLocButtonBounds();
        this.search = false;
      });
  }

  getLocButtonBounds() {
    const ld = localStorage.getItem('locationDiscovered');
    if (!ld) {
      if (this.locationFilter && this.locations.length > 1) {
        try {
          const el = this.locButton.nativeElement;
          this.feature.top = el.offsetTop - (350 - (el.offsetWidth / 2));
          this.feature.left = el.offsetLeft - (350 - (el.offsetWidth / 2));
          setTimeout(() => {
            this.discover = true;
          }, 700);
        } catch (err) {
        }
      }
    }
  }

  discoveryClick() {
    this.discover = false;
    localStorage.locationDiscovered = true;
  }

  startToolbarServiceSubs() {
    this.ts.toolbarTitle.subscribe((title) => {
      this.title = title;
    });
    this.ts.toolbarArrow.subscribe((state) => {
      this.rootPage = state;
    });
    this.ts.toolbarShadow.subscribe((flag) => {
      this.showShadow = flag;
    });
    this.ts.toolbarColor.subscribe((color) => {
    });
  }

  isScreenSmall(): boolean {
    return window.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`).matches;
  }

  back() {
    this.returnPath.hasPath ? this.router.navigate([this.returnPath.path]) : this.location.back();
  }

  openFilter() {
    const config = {
      width: '540px',
      data: {
        filters: this.filters,
        vendors: this.vendors,
        lId: this.lId
      }
    }
    const dialogRef = this.dialog.open(FilterComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.filters = result.filters;
        this.vendors = result.vendors;
        const trueFilters = {};
        for (let i = 0; i < Object.keys(result.filters).length; i++) {
          if (result.filters[Object.keys(result.filters)[i]] === true) {
            trueFilters[Object.keys(result.filters)[i]] = true;
          }
        }
        this.filtersService.changeFilter(trueFilters, result.vendors);
      }
    });
  }

  openLocationFilter() {
    const config = {
      width: '540px',
      data: {
        location: this.lId,
        emptyCart: this.ordersLength > 0 ? false : true
      }
    };
    const dialogRef = this.dialog.open(FilterLocationComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['consumer', result.location, 'menu']);
        this.lId = result.location;
      }
    });
  }

  toggleSearch() {
    this.search = !this.search;
    this.searchString = '';
    this.searchChange(this.search);
  }

  searchChange(search): void {
    this.searchService.changeString(this.searchString, search);
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  handleSignBtn() {
    if (this.signBtnFlag) {
      this.as.signOut();
      this.aRewards = 0;
    } else if (!this.signBtnFlag) {
      const navExtras: NavigationExtras = {
        queryParams: { 'redirectUrl' : this.us.lastPath }
      };
      this.router.navigate(['onboarding'], navExtras);
    }
  }

  handleLocationsNavLink() {
    if (this.locations.length === 1) {
      this.router.navigate([`consumer/locations/${this.lId}`]);
    } else {
      this.router.navigate(['consumer/locations']);
    }
  }

  handleNavLinks(link, event) {
    switch (link) {
      case 'home':
        this.router.navigate(['consumer', 'home']);
        break;
      case 'products':
        this.router.navigate(['consumer', 'menu']);
        break;
      case 'locations':
        if (this.locations.length === 1) {
          this.router.navigate([`consumer/locations/${this.lId}`]);
        } else {
          this.router.navigate(['consumer/locations']);
        }
        break;
      case 'rewards':
        this.router.navigate(['consumer/rewards']);
        // tslint:disable-next-line:max-line-length
        if (!this.signBtnFlag) { this.openBottomSheet('insert_emoticon', 'Rewards', 'Login or sign up to see your rewards, gifts and points'); }
        break;
      case 'orders':
        this.signBtnFlag
          ? this.router.navigate(['consumer', 'orders'])
          : this.openBottomSheet('receipt', 'Orders'); // this.snack('Must be logged in', 'OK', { duration: 3000 });
        break;
      case 'activity':
        this.signBtnFlag
          ? this.router.navigate(['consumer/activity'])
          : this.openBottomSheet('timeline', 'Activity'); // this.snack('Must be logged in', 'OK', { duration: 3000 });
        break;
      case 'profile':
        this.signBtnFlag
          ? this.router.navigate(['consumer/profile'])
          : this.openBottomSheet('account_circle', 'Profile'); // this.snack('Must be logged in', 'OK', { duration: 3000 });
        break;
      case 'settings':
        this.signBtnFlag
          ? this.router.navigate(['consumer/settings'])
          : this.openBottomSheet('settings', 'Settings'); // this.snack('Must be logged in', 'OK', { duration: 3000 });
        break;
      case 'help':
        this.router.navigate(['consumer', this.lId, 'support']);
        break;
      case 'cart':
        this.router.navigate(['consumer/cart']);
        break;
    }
    this.closeNav();
  }

  scrolling(event) {
  }

  closeNav() {
    this.discover = false;
    if (this.mobileQuery.matches) {
      this.sidenav.close();
    }
  }

  snack(m, a, c) {
    this.sb.open(m, a, c);
  }

  openBottomSheet(icon?, link?, text?) {
    const config: any = {
      data: {
        icon: icon,
        link: link
      }
    };
    if (text) { config.data.text = text; }
    console.log(config);
    this.mb.open(BottomSheetComponent, config);
  }

}

@Component({
  selector: 'app-save-favorite-dialog',
  template: `
    <h2 matDialogTitle>Favorite Location</h2>
    <mat-dialog-content><p>You are changing your favorite location. Proceed?</p></mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button color="accent" [matDialogClose]="false">CANCEL</button>
      <button mat-raised-button color="accent" [matDialogClose]="true">OK</button>
    <mat-dialog-actions>
  `
})
export class SaveFavoriteDialogComponent {

  constructor(
    public dRef: MatDialogRef<SaveFavoriteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

}
