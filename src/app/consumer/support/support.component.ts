import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

import { AuthenticationService } from './../../services/authentication.service';
import { SupportService } from './../../services/support.service';
import { LocationService } from './../../services/location.service';
import { FiltersService } from './../../services/filters.service';
import { SupportRequest } from './../../models/supportRequest';

import { BusinessLocation } from './../../models/businesslocation';

import { environment } from './../../../environments/environment';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  bId: string;
  lId: string;
  sForm: FormGroup;
  location: any|BusinessLocation;

  days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  day: any;
  hours: any[];

  name: string;
  phone: string;
  email: string;

  flag: boolean;

  priorities = [
    {value: 'low', label: 'Low'},
    {value: 'medium', label: 'Medium'},
    {value: 'high', label: 'High'}
  ];

  constructor(
    private fb: FormBuilder,
    private sps: SupportService,
    private as: AuthenticationService,
    private ls: LocationService,
    private fs: FiltersService,
    private sb: MatSnackBar,
    private route: ActivatedRoute
  ) {
    this.bId = environment.businessId;
    this.name = '';
    this.phone = '';
    this.email = '';
    this.flag = false;
    this.day = new Date().getDay();
    this.hours = [];
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.lId = params['lId'];
    });
    this.as.user.subscribe((auth) => {
      if (auth) {
        this.name = auth.displayName;
        this.phone = auth.phoneNumber;
        this.email = auth.email;
        this.createForm();
      } else {
        this.createForm();
      }
    });
    this.getLocationData();
  }

  createForm() {
    this.sForm = this.fb.group({
      name: [this.name, Validators.required],
      phone: [this.phone, [Validators.required, Validators.pattern(PHONE_REG_EX)]],
      email: [this.email, [Validators.required, Validators.email]],
      subject: ['', Validators.required],
      desc: ['', Validators.required],
      priority: ['', Validators.required]
    });
    this.flag = true;
  }

  getLocationData() {
    this.location = this.ls.getLocationDetails(this.lId);
    this.location.subscribe((location) => {
      this.formatHours(location);
    });
    /*
    const location = localStorage.getItem('location');
    if (location) {
      this.location = this.ls.getLocationDetails(location);
      this.location.subscribe(loc => {
        this.formatHours(loc);
      })
    } else  {
      const locations = this.ls.locationsAsObservableWithId();
      locations.subscribe((locs) => {
        localStorage.setItem('location', locs[0].id);
        this.location = locs[0];
      });
    }
    */
  }

  formatHours(loc) {
    this.hours = [];
    for (let i = 0; i < Object.keys(loc.hours).length; i++) {
      const day = {
        day: this.days[i],
        open: loc.hours[this.days[i]].open,
        openTime: this.convertToDate(loc.hours[this.days[i]].openTime),
        closeTime: this.convertToDate(loc.hours[this.days[i]].closeTime)
      };
      this.hours.push(day);
    }
  }

  sendSupportRequest() {
    const x = this.sForm.value;
    const r = new SupportRequest(x.name, x.phone, x.email, x.subject, x.desc, x.priority);
    this.sps.saveRequest(r)
      .then((ref) => {
        this.snack('Request Sent', 'OK', 2000);
        this.resetForm();
      }).
      catch((error) => {
        this.snack('Error Sending Request', 'OK', 2000);
      });
  }

  resetForm() {
    this.sForm.reset({
      name: '',
      phone: '',
      email: '',
      subject: '',
      desc: '',
      priority: ''
    });
  }

  snack(message, action, duration) {
    this.sb.open(message, action, {
      duration: duration
    });
  }

  convertToDate(time) {
    const t = time.split(':');
    return new Date().setHours(t[0], t[1]);
  }

}
