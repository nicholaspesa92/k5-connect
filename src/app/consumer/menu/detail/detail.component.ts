import {
  Component,
  OnInit,
  AfterViewInit,
  AfterViewChecked,
  HostBinding,
  ViewChild, ElementRef
} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { slideRightToLeft, crazyAnimation } from '../../../shared/animation';
import { Observable } from 'rxjs';
import { MenuService } from '../../../services/menu.service';
import { ToolbarService } from '../../../services/toolbar.service';
import { AuthenticationService } from './../../../services/authentication.service';
import { Menu } from '../../../models/menu';
import { UrlSanitizerService } from './../../../services/url-sanitizer.service';
import { OrderService } from './../../../services/order.service';
import { LocationService } from './../../../services/location.service';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  // animations: [slideRightToLeft]
})
export class DetailComponent implements OnInit, AfterViewInit, AfterViewChecked {
  // @HostBinding('@routeAnimation') routeAnimation = true;
  // @HostBinding('style.display') display = 'block';
  // @HostBinding('style.position') position = 'relative';
  // bgImg = 'http://res.cloudinary.com/dutchie/image/upload/v1499976080/qgb1ptwensidhlcnb04c.jpg';

  @ViewChild('imageLarge', { read: ElementRef }) imageLarge: ElementRef;

  sub: any;
  id: string;
  locationId: string;
  item: Observable<Menu>;
  item$: Menu;
  firstPrice: any;
  sortedPrices: any[];
  sortedSalePrices: any[];
  bSettings: any|Observable<{}>;
  isDispensary: boolean;
  isGrowler: boolean;

  selected: number;
  quantity: number;

  weedPlaceholder: string;

  bigH: number;
  bigW: number;

  headerImage: any;

  hasOnlineOrdering: boolean;

  view: boolean;

  location$: any;
  location: any;

  subs: any;

  constructor(
    private route: ActivatedRoute,
    private ms: MenuService,
    private ts: ToolbarService,
    private as: AuthenticationService,
    private urlS: UrlSanitizerService,
    private os: OrderService,
    private sb: MatSnackBar,
    private ls: LocationService
  ) {
    this.isDispensary = false;
    this.isGrowler = false;
    this.weedPlaceholder = environment.weedPlaceholder;
    this.sortedPrices = [];
    this.sortedSalePrices = [];
    this.selected = 0;
    this.quantity = 1;
    this.view = false;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.locationId = params['lId'];
   });
   // this.startSubs();
   this.startSubsX();
  }

  startSubs() {
    this.item = this.ms.getMenuItem(this.locationId, this.id);
    this.location$ = this.ls.getLocationDetails(this.locationId);
    this.bSettings = this.as.getCurrentBusinessSettings();
    this.subs = Observable.combineLatest([this.item, this.location$, this.bSettings]);
    this.subs.subscribe(([data, location, settings]) => {
      this.handleData(data, location, settings);
    });
  }

  startSubsX() {
    this.item = this.ms.getBusinessMenuItem(this.id);
    this.location$ = this.ls.getLocationDetails(this.locationId);
    this.bSettings = this.as.getCurrentBusinessSettings();
    this.subs = Observable.combineLatest([this.item, this.location$, this.bSettings]);
    this.subs.subscribe(([data, location, settings]) => {
      this.handleData(data, location, settings);
    });
  }

  handleData(data, location, settings) {
    this.location = location;
    this.hasOnlineOrdering = location.hasOnlineOrdering;
    if (settings.hasOwnProperty('isDispensary')) { this.isDispensary = settings.isDispensary; }
    if (settings.hasOwnProperty('isGrowler')) { this.isGrowler = settings.isGrowler; }

    if (data.photoUrl) {
      this.headerImage = this.urlS.getSanitizedStyle(data.photoUrl);
      data.noPhoto = false;
    } else if (!data.photoUrl) {
      this.headerImage = this.urlS.getSanitizedStyle(this.ms.getBackDrop(data));
      data.noPhoto = true;
    }
    this.setToolbarTitle(data.category);
    this.setFirstPrice(data.variations);
    data.variations && data.saleVariations ?
        this.sortPricesAndMatchOnSale(data.variations, data.saleVariations) :
        this.setFirstPrice(data.variations);
    if (data.vendor) { data.vendor = this.ms.replace(data.vendor); }
    if (data.type) { data.type = this.ms.replace(data.type); }
    this.item$ = data;
    console.log(this.item$);
  }

  ngAfterViewInit() {
    // this.hasOnlineOrdering = this.ls.hasOnlineOrdering;
  }

  ngAfterViewChecked() {
    // this.hasOnlineOrdering = this.ls.hasOnlineOrdering;
  }

  setToolbarTitle(name) {
    const pieces = name.split(' ');
    for (let i = 0; i < pieces.length; i++) {
      pieces[i] = pieces[i].charAt(0).toUpperCase() + pieces[i].slice(1);
    }
    this.ts.setNextTitle(pieces.toString().replace(/,/g, ' '));
  }

  sortPricesAndMatchOnSale(variations, saleVariations) {
    this.sortedPrices = this.ms.sortAndOrderSaleVariations(variations);
    for (let i = 0; i < this.sortedPrices.length; i++) {
      if (saleVariations[this.sortedPrices[i].name] > 0) {
        this.sortedPrices[i].price = saleVariations[this.sortedPrices[i].name];
      }
      this.sortedPrices[i].name = this.replace(this.sortedPrices[i].name);
    }
  }

  setFirstPrice(variations) {
    if (variations === null || variations === undefined) { this.firstPrice = {}; return; }
    this.sortedPrices = this.ms.sortAndOrderVariations(variations);
    this.firstPrice = this.sortedPrices[0];
    for (let i = 0; i < this.sortedPrices.length; i++) {
      this.sortedPrices[i].name = this.replace(this.sortedPrices[i].name);
    }
  }

  sortSalePrices(variations) {
    this.sortedSalePrices = this.ms.sortAndOrderVariations(variations);
  }

  addItemToCart(item) {
    this.os.addItem(item, this.sortedPrices[this.selected], this.quantity);
    this.snack(`${item.name} added to your cart`, 'OK', 2000);
  }

  getUrl(url) {
    if (url === '' || url === null || url === undefined) { return this.weedPlaceholder; }
    return url;
  }

  getScreenWidth() {
    return window.innerWidth;
  }

  sanitizeImage(url) {
    if (url === '' || url === null || url === undefined) { return this.urlS.getSanitizedStyle(this.weedPlaceholder); }
    return this.urlS.getSanitizedStyle(url);
  }

  replace(string) {
    if (string.indexOf('-') > -1) {
      return this.replaceDash(string);
    } else if (string.indexOf('_') > -1) {
      return this.replaceUnderscore(string);
    } else {
      return string;
    }
  }
  replaceDash(string) {
    return string.replace(/-/g, ' ');
  }
  replaceUnderscore(string) {
    return string.replace(/_/g, ' ');
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

}
