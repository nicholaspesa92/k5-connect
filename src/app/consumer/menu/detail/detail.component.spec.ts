import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule, MatIconModule, MatListModule, MatCardModule, MatTabsModule, MatSelectModule, MatSnackBarModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailComponent } from './detail.component';
import { TitlePipe } from '../../../shared/title.pipe';
import { MenuService } from '../../../services/menu.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { ToolbarService } from '../../../services/toolbar.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FormsModule } from '@angular/forms';
import { UrlSanitizerService } from './../../../services/url-sanitizer.service';
import { OrderService } from './../../../services/order.service';
import { LocationService } from './../../../services/location.service';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

import { environment } from '../../../../environments/environment';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailComponent, TitlePipe ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatSelectModule,
        MatSnackBarModule,
        FlexLayoutModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        FormsModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig)
      ],
      providers: [
        MenuService,
        AuthenticationService,
        ToolbarService,
        UrlSanitizerService,
        OrderService,
        LocationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
