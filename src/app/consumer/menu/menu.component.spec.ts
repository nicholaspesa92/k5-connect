import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule, MatIconModule, MatListModule, MatCardModule, MatTabsModule,
  MatSnackBarModule, MatProgressSpinnerModule, MatBadgeModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu.component';
import { MenuByCategoryPipe } from '../../shared/menu-by-category.pipe';
import { SearchMenuPipe } from '../../shared/search-menu.pipe';
import { FilterMenuPipe } from '../../shared/filter-menu.pipe';
import { GroupByPipe } from '../../shared/group-by.pipe';
import { SearchStringService } from '../../services/search-string.service';
import { FiltersService } from '../../services/filters.service';
import { MenuService } from '../../services/menu.service';
import { LocationService } from '../../services/location.service';
import { AuthenticationService } from '../../services/authentication.service';
import { OrderService } from './../../services/order.service';
import { TitlePipe } from '../../shared/title.pipe';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { ToolbarService } from './../../services/toolbar.service';
import { ProductItemComponent } from './../../ui-elements/product-item/product-item.component';
import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';
import { VariationsPipe } from './../../pipes/variations.pipe';
import { VirtualScrollModule } from 'angular2-virtual-scroll';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

import { environment } from './../../../environments/environment';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MenuComponent,
        MenuByCategoryPipe,
        SearchMenuPipe,
        FilterMenuPipe,
        GroupByPipe,
        TitlePipe,
        ProductItemComponent,
        VariationsPipe
      ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatTabsModule,
        MatBadgeModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        FlexLayoutModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig),
        VirtualScrollModule
      ],
      providers: [
        SearchStringService,
        FiltersService,
        MenuService,
        AuthenticationService,
        LocationService,
        UrlSanitizerService,
        ToolbarService,
        OrderService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
