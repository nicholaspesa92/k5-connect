import { Component, OnInit, Input, Output,
  AfterViewInit, HostBinding, OnDestroy, HostListener,
  ViewChild, ElementRef } from '@angular/core';
import { slideLeftToRight } from '../../shared/animation';
import { Observable ,  Subscription } from 'rxjs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { SearchStringService } from '../../services/search-string.service';
import { FiltersService } from '../../services/filters.service';
import { MenuService } from '../../services/menu.service';
import { LocationService } from '../../services/location.service';
import { Menu } from '../../models/menu';
import { BusinessLocation } from '../../models/businesslocation';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { AuthenticationService } from './../../services/authentication.service';
import { OrderService } from './../../services/order.service';
import { environment } from './../../../environments/environment';
import { ToolbarService } from './../../services/toolbar.service';
import { VirtualScrollComponent } from 'angular2-virtual-scroll';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
  // animations: [slideLeftToRight]
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('tabGroup', {read: ElementRef}) tabGroup: ElementRef;
  @ViewChild('normItem', {read: ElementRef}) normItem: ElementRef;

  searchString: any = {text: ''};
  searching: boolean;
  filters: any = [];
  subscription: Subscription;
  subscription2: Subscription;
  locationFilter: any = [];
  locationSub: Subscription;
  locationId: number;
  private sub: any;
  zeroItems: boolean;

  menu: Observable<Menu[]>;
  menuSub: any;
  menu$: any|Menu[];
  bareMenu: any[];
  firstPrice: any[];
  specials: any[];

  public products = [];

  weedPlaceholder: string;
  selectedTab: number;

  bSettings: Observable<any>;
  categories: any[];

  backdrops: any[];
  dropUrls: any[];

  // NEW MENU //
  bMenu: any;
  bMenuSub: any;

  vBuffer: number;

  subs: any;
  selectedMenu: any;

  types$: Observable<any[]>;
  types: any[];
  vendors$: Observable<any[]>;
  vendors: any[];

  showFilters: boolean;
  filterSelection: number;

  lId: any;
  locations$: Observable<BusinessLocation[]>;
  locations: any;
  sLocation;

  selectedCategory: number;

  filter: any;
  calcAmount: number;

  firstLoad: boolean;

  ordersLength: any;

  @ViewChild(VirtualScrollComponent) vs: VirtualScrollComponent;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public searchService: SearchStringService,
    private ms: MenuService,
    private ls: LocationService,
    public filtersService: FiltersService,
    private urlS: UrlSanitizerService,
    private as: AuthenticationService,
    private ts: ToolbarService,
    private os: OrderService
  ) {
    this.categories = [];
    this.weedPlaceholder = environment.weedPlaceholder;
    this.zeroItems = false;
    this.bareMenu = [];
    this.searching = false;
    this.specials = [];
    this.menu$ = [];
    this.backdrops = [];
    this.selectedTab = -1;
    this.vBuffer = 5;
    this.showFilters = false;
    this.filterSelection = 0;
    this.types = [];
    this.vendors = [];

    this.firstLoad = false;
    this.ordersLength = this.os.orderItems.length;
  }

  ngOnInit() {
    this.filters = this.filtersService.getFiltersArr();
    this.filter = this.filtersService.filter;
    console.log(this.filter);
    this.locations$ = this.ls.locationsAsObservableWithId();
    this.types$ = this.as.getCurrentBusinessTypes();
    this.vendors$ = this.as.getCurrentBusinessVendors();
    this.subs = Observable.combineLatest(this.locations$, this.types$, this.vendors$);
    this.subs.subscribe(([l, t, v]) => {
      this.types = t;
      this.vendors = v;

      this.locations = l;
      this.lId = localStorage.getItem('lId');
      if (this.lId) {
        this.selectedCategory = +localStorage.getItem('selectedCategory');
        this.sLocation = l.filter(location => this.lId === location.id)[0];
        // this.getMenuWithCats(this.lId);
        this.getBusinessMenu(this.lId);
      } else {
        this.viewFilters(2);
      }
    });
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  startSubs() {
    this.subscription = this.searchService.getString().subscribe(searchString => {
      this.searching = searchString.searching;
      this.searchString = searchString;
    });
  }

  endSubs() {
    this.subscription.unsubscribe();
  }

  getMenuWithCats(id) {
    this.menu$ = [];
    this.selectedMenu = {};
    this.categories = [];
    this.zeroItems = false;
    if (this.menuSub) { this.menuSub.unsubscribe(); }
    if (this.subscription || this.subscription2 || this.locationSub) { this.endSubs(); }
    this.menu = this.ms.getMenuItems(id);
    this.menuSub = this.menu.subscribe((menu) => {
      this.bareMenu = menu.sort(this.abc);
      this.calculateFilters();
      const s = this.getSpecials(this.applyFilters(menu));
      const specials = {
        key: 'specials',
        value: s,
        filteredValue: s
      };
      const m = this.ms.transformArray(this.applyFilters(menu), 'category');
      if (s.length > 0) { m.unshift(specials); }
      m.map((item) => {
        this.categories.push({ text: item.key, amount: item.filteredValue.length });
      });
      this.menu$ = m;
      this.onFilterChange();
      this.menu$.length < 1 ? this.zeroItems = true : this.zeroItems = false;
      this.selectedTab = +localStorage.getItem('selectedTab');
      this.firstLoad = true;
      console.log(this.menu$);
    });
    this.startSubs();
  }

  getBusinessMenu(lId) {
    this.menu$ = [];
    this.selectedMenu = {};
    this.categories = [];
    this.zeroItems = false;
    this.bMenu = this.ms.getBusinessMenuItems(lId);
    this.bMenu.subscribe((menu) => {
      this.bareMenu = menu.sort(this.abc);
      this.calculateFilters();
      const s = this.getSpecials(this.applyFilters(menu));
      const specials = {
        key: 'specials',
        value: s,
        filteredValue: s
      };
      const m = this.ms.transformArray(this.applyFilters(menu), 'category');
      if (s.length > 0) { m.unshift(specials); }
      m.map((item) => {
        this.categories.push({ text: item.key, amount: item.filteredValue.length });
      });
      this.menu$ = m;
      this.onFilterChange();
      this.menu$.length < 1 ? this.zeroItems = true : this.zeroItems = false;
      this.selectedTab = +localStorage.getItem('selectedTab');
      this.firstLoad = true;
    });
    this.startSubs();
  }

  viewFilters(i) {
    if (i === 0 && !this.selectedMenu) { return; }
    this.filterSelection = i;
    this.showFilters = true;
  }

  hideFilters() {
    this.showFilters = false;
  }

  categorySelection(i) {
    localStorage.selectedCategory = i;
    this.selectedCategory = i;
    this.selectedMenu = this.menu$[i];
    this.showFilters = false;
  }

  locationSelection(location) {
    if (this.lId === location.id) { this.showFilters = false; return; }
    if (this.ordersLength > 0) { this.ordersLength = 0; this.os.clearCart(); }
    this.firstLoad = false;
    this.lId = location.id;
    this.sLocation = location;

    localStorage.lId = this.lId;
    localStorage.selectedCategory = 0;
    this.selectedCategory = 0;

    this.filter.types = [];
    this.filter.vendors = [];

    // this.getMenuWithCats(this.lId);
    this.getBusinessMenu(this.lId);
    this.showFilters = false;
    this.ls.nextLocation(this.lId);
  }

  typeSelection(e) {
    if (e.option.selected) {
      this.filter.types.push(e.option.value);
    } else {
      this.filter.types.splice(this.filter.types.indexOf(e.option.value), 1);
    }
    this.calculateFilters();
  }

  vendorSelection(e) {
    if (e.option.selected) {
      this.filter.vendors.push(e.option.value);
    } else {
      this.filter.vendors.splice(this.filter.vendors.indexOf(e.option.value), 1);
    }
    this.calculateFilters();
  }

  calculateFilters() {
    const x = this.bareMenu.filter((item) => {
      return (this.filter.types.length > 0 && this.filter.types.indexOf(item.type) > -1)
          || (this.filter.vendors.length > 0 && this.filter.vendors.indexOf(item.vendor) > -1);
    });
    this.filter.types.length < 1 && this.filter.vendors.length < 1 ? this.calcAmount = this.bareMenu.length : this.calcAmount = x.length;
  }

  isTypeSelected(val) {
    return (this.filter.types.length > 0 && this.filter.types.indexOf(val) > -1);
  }

  isVendorSelected(val) {
    return (this.filter.vendors.length > 0 && this.filter.vendors.indexOf(val) > -1);
  }

  clearFilters() {
    this.filter.types = [];
    this.filter.vendors = [];
    this.calculateFilters();
    this.onFilterChange();
  }

  onFilterChange() {
    this.categories = [];
    if (this.filter.types.length < 1 && this.filter.vendors.length < 1) {
      this.menu$.map((items) => {
        items.filteredValue = items.value;
        this.categories.push({ text: items.key, amount: items.filteredValue.length });
      });
      this.selectedMenu = this.menu$[this.selectedCategory];
    } else {
      this.menu$.map((items) => {
        items.filteredValue = items.value.filter((item) => {
          return (this.filter.types.length > 0 && this.filter.types.indexOf(item.type) > -1)
            || (this.filter.vendors.length > 0 && this.filter.vendors.indexOf(item.vendor) > -1);
        });
        this.categories.push({ text: items.key, amount: items.filteredValue.length });
      });
      this.selectedMenu = this.menu$[this.selectedCategory];
    }
    this.hideFilters();
  }

  applyFilters(menu) {
    let construct = [];
    if (this.filters === undefined || this.filters === null) { return menu; }
    if (Object.keys(this.filters.filters).length === 0 && this.filters.vendors.length === 0) { return menu; }
    const f = this.filters.filters;
    const v = this.filters.vendors;
    const x = menu.filter((item) => {
      return (Object.keys(f).length > 0 ? f.hasOwnProperty(item.type) : true) && (v.length > 0 ? v.indexOf(item.vendor) > -1 : true)
    });
    construct = construct.concat(x);
    return construct;
  }

  getSpecials(menu) {
    return menu.filter((item) => {
      return item.onSale === true && item.hasOwnProperty('saleVariations');
    });
  }

  gotoItem(id, i) {
    console.log(i);
    this.filtersService.filter = this.filter;
    this.router.navigate([`consumer/${this.lId}/menu/${id}`]);
  }

  tabChange(i) {
    console.log('Tab Change: ', i);
    this.selectedTab = i
    localStorage.selectedTab = this.selectedTab;
  }

  indexChange(e) {
    this.selectedTab = e.index;
    localStorage.selectedTab = this.selectedTab;
  }

  search(query) {
    if (query) {
      return this.bareMenu.filter(item => item['name'].toLowerCase().indexOf(query.toLowerCase()) > -1).length;
    }
  }

  getFirstPrice(variations) {
    return this.ms.sortAndOrderVariations(variations)[0];
  }

  compareWithOrder(a, b) {
    return this.categories.indexOf(a.category) - this.categories.indexOf(b.category);
  }

  abc(a, b) {
    return a.name < b.name ? -1 : 1;
  }

  abcKey(a, b) {
    return a.key < b.key ? -1 : 1;
  }

  swipe(event) {
    console.log(event);
    event.preventDefault();
    if (this.selectedTab === 0 && event.additionalEvent === 'panright') { return; }
    if ((this.selectedTab + 1) === this.menu$.length && event.additionalEvent === 'panleft') { return; }
    switch (event.additionalEvent) {
      case 'panleft':
        this.selectedTab += 1;
        break;
      case 'panright':
        this.selectedTab -= 1;
        break;
    }
  }

}
