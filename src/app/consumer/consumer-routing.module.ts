import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerComponent } from './consumer.component';
import { ActivityComponent } from './activity/activity.component';
import { ArticlesComponent } from './articles/articles.component';
import { RewardsComponent } from './rewards/rewards.component';
import { MenuComponent } from './menu/menu.component';
import { LocationsComponent } from './locations/locations.component';
import { SupportComponent } from './support/support.component';
import { MessagesComponent } from './messages/messages.component';
import { DetailsComponent } from './locations/details/details.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DetailComponent } from './menu/detail/detail.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { SettingsComponent } from './settings/settings.component';
import { EditComponent } from './settings/edit/edit.component';
import { FilterComponent } from './filter/filter.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { HomeComponent } from './home/home.component';
import { BottomSheetComponent } from './../bottom-sheet/bottom-sheet.component';
import { CartComponent } from './cart/cart.component';
import { OrdersComponent } from './orders/orders.component';
import { RemoveComponent } from './cart/remove/remove.component';
import { OrderDetailComponent } from './orders/order-detail/order-detail.component';
import { StatusBarComponent } from './../ui-elements/status-bar/status-bar.component';
import { ProductItemComponent } from './../ui-elements/product-item/product-item.component';
import { ArticleDetailComponent } from './articles/article-detail/article-detail.component';

import { AuthGuard } from './../guards/auth.guard';

const routes: Routes = [
    {
      path: '',
      component: ConsumerComponent,
      children: [
        // {path: '', redirectTo: 'menu', pathMatch: 'full' },
        {
          path: 'home',
          component: HomeComponent,
          data: {
            title: 'Home',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: true,
            locationFilter: false,
            toolbarTrans: true,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/home',
          component: HomeComponent,
          data: {
            title: 'Home',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: true,
            locationFilter: false,
            toolbarTrans: true,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'articles',
          component: ArticlesComponent,
          data: {
            title: 'Articles',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'articles/:id',
          component: ArticleDetailComponent,
          data: {
            title: 'Articles',
            rootPage: false,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'messages',
          component: MessagesComponent,
          data: {
            title: 'Messages',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'activity',
          component: ActivityComponent,
          data: {
            title: 'Activity',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'rewards',
          component: RewardsComponent,
          data: {
            title: 'Rewards',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'locations',
          component: LocationsComponent,
          data: {
            title: 'Locations',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'locations/:id',
          component: DetailsComponent,
          data: {
            title: 'Location Details',
            rootPage: false,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: true,
            showFavorite: true,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'menu',
          component: MenuComponent,
          data: {
            title: 'Products',
            rootPage: true,
            showShadow: false,
            showSearch: true,
            locationFilter: true,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/menu',
          component: MenuComponent,
          data: {
            title: 'Products',
            rootPage: true,
            showShadow: false,
            showSearch: true,
            locationFilter: true,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'menu/:locationId/:id',
          component: DetailComponent,
          data: {
            title: 'Product',
            rootPage: false,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: true,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/menu/:id',
          component: DetailComponent,
          data: {
            title: 'Product',
            rootPage: false,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: true,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/support',
          component: SupportComponent,
          data: {
            title: 'Help',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'terms',
          component: TermsComponent,
          data: {
            title: 'Terms of Service',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'privacy',
          component: PrivacyComponent,
          data: {
            title: 'Privacy Policy',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'profile',
          component: ProfileComponent,
          data: {
            title: 'Profile',
            rootPage: true,
            showShadow: false,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          },
          canActivate: [AuthGuard]
        },
        {
          path: 'profile/:option',
          component: ProfileEditComponent,
          data: {
            title: 'Profile Edit',
            rootPage: false,
            showShadow: true,
            showSearch: false,
            home: false,
            field: '',
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'settings',
          component: SettingsComponent,
          data: {
            title: 'Settings',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'cart',
          component: CartComponent,
          data: {
            title: 'Cart',
            rootPage: false,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/orders',
          component: OrdersComponent,
          data: {
            title: 'Orders',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: 'orders',
          component: OrdersComponent,
          data: {
            title: 'Orders',
            rootPage: true,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: false,
              path: ''
            }
          }
        },
        {
          path: ':lId/orders/:id',
          component: OrderDetailComponent,
          data: {
            title: 'Order Detail',
            rootPage: false,
            showShadow: true,
            showSearch: false,
            home: false,
            locationFilter: false,
            toolbarTrans: false,
            showFavorite: false,
            returnPath: {
              hasPath: true,
              path: 'consumer/orders'
            }
          }
        }
      ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConsumerRoutingModule {}
