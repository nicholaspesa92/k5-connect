import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './../../services/authentication.service';
import { ActivityService } from './../../services/activity.service';
import { MemberService } from './../../services/member.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { Activity } from '../../models/activity';
import { Member } from '../../models/member';
import { User } from '../../models/user';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  // bgImg = '//cdn.shopify.com/s/files/1/1249/2029/t/7/assets/slideshow_1.jpg';
  bgImg = 'https://cdn.allbud.com/image/upload/s--rrF3LImT--/c_limit,h_600,w_800/v1419022952/' +
  'images/dispensary/high-grade-organics/22/high-grade-inside-store.jpg';

  activities: Observable<Activity[]>;
  activitiesAmount: number;
  member: Observable<Member>;
  user: Observable<User>;
  memberId: string;

  settings$: Observable<any>;
  settings: any;
  activityBanner: any;

  loggedIn: boolean;

  constructor(
    private as: AuthenticationService,
    private acts: ActivityService,
    private ms: MemberService,
    private urlS: UrlSanitizerService
  ) {
    this.loggedIn = true;
    this.activityBanner = '';
  }

  ngOnInit() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.memberId = user.uid;
          this.activities = this.acts.getActivity();
          this.activities.subscribe((activities) => {
            this.activitiesAmount = Object.keys(activities).length;
          })
          this.member = this.ms.getMember(this.memberId);
          this.startSettingsSub();
        } else if (user.isAnonymous) {
          this.loggedIn = false;
        }
      } else  {
        this.loggedIn = false;
      }
    });
  }

  startSettingsSub() {
    this.settings$ = this.acts.getSettings();
    this.settings$.subscribe((settings) => {
      this.settings = settings;
      this.activityBanner = this.urlS.getSanitizedStyle(settings.activityBanner);
    });
  }

}
