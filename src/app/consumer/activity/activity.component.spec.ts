import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule, MatListModule, MatCardModule, MatSnackBarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GroupByPipe } from '../../shared/group-by.pipe';
import { GroupDatePipe } from '../../shared/group-date.pipe';
import { ActivityComponent } from './activity.component';
import { BottomSheetComponent } from './../../bottom-sheet/bottom-sheet.component';

import { AuthenticationService } from './../../services/authentication.service';
import { MemberService } from './../../services/member.service';
import { ActivityService } from './../../services/activity.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../../environments/environment';

describe('ActivityComponent', () => {
  let component: ActivityComponent;
  let fixture: ComponentFixture<ActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GroupByPipe,
        ActivityComponent,
        GroupDatePipe,
        BottomSheetComponent
      ],
      imports: [
        MatIconModule,
        MatListModule,
        MatCardModule,
        FlexLayoutModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [AuthenticationService, ActivityService, MemberService, UrlSanitizerService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
