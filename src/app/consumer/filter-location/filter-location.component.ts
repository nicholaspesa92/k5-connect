import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { FiltersService } from '../../services/filters.service';
import { LocationService } from '../../services/location.service';
import { BusinessLocation } from '../../models/businesslocation';
import { OrderService } from './../../services/order.service';

@Component({
  selector: 'app-filter-location',
  templateUrl: './filter-location.component.html',
  styleUrls: ['./filter-location.component.scss']
})
export class FilterLocationComponent implements OnInit {

  locations: Observable<BusinessLocation[]>;
  locationFilters: FormGroup;
  selectedLocation: string;

  emptyCart: boolean;

  constructor(
    @Optional()
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<FilterLocationComponent>,
    private ls: LocationService,
    private fb: FormBuilder,
    public filtersService: FiltersService,
    private os: OrderService
  ) {
    if (data) {
      this.emptyCart = data.emptyCart;
    }
  }

  ngOnInit() {
    this.createForm();
    this.setForm();
    this.locations = this.ls.locationsAsObservableWithId();
  }
  setForm() {
    if (this.data) {
      this.locationFilters.setValue({
        location: this.data.location
      });
    }
  }
  createForm() {
    this.locationFilters = this.fb.group({
      location: '',
    })
  }

  revert() {
    this.setForm();
    const filter = this.locationFilters.value;
    this.dialogRef.close(filter);
  }

  onSubmit() {
    if (!this.emptyCart) { this.os.clearCart(); }
    const filter = this.locationFilters.value;
    this.dialogRef.close(filter);
  }

}
