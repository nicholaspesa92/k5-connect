import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatRadioModule,
  MatSnackBarModule,
  MatIconModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FiltersService } from '../../services/filters.service';
import { LocationService } from '../../services/location.service';
import { AuthenticationService } from '../../services/authentication.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';
import { FilterLocationComponent } from './filter-location.component';
import { RouterTestingModule } from '@angular/router/testing';
import { OrderService } from './../../services/order.service';

describe('FilterLocationComponent', () => {
  let component: FilterLocationComponent;
  let fixture: ComponentFixture<FilterLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterLocationComponent ],
      imports: [
        MatDialogModule,
        MatRadioModule,
        MatSnackBarModule,
        MatIconModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [
        FiltersService,
        LocationService,
        AuthenticationService,
        OrderService,
        { provide: MatDialogRef },
        { provide: MAT_DIALOG_DATA, use: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
