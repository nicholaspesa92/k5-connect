import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { ArticleService } from './../../services/article.service';
import { Article } from './../../models/article';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  articles: Observable<Article[]>;

  size: number;

  grow: boolean;

  constructor(
    private arts: ArticleService,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute
  ) {
    this.size = 1;
    this.grow = false;
    this.route.params.subscribe((params) => {
    });
  }

  ngOnInit() {
    this.articles = this.arts.getArticlesWithId();
    this.articles.subscribe((articles) => {
      this.size = articles.length;
    });
  }

  sanitizeImage(url) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
  }

  animate() {
    setTimeout(() => {
      this.grow = true;
    }, 5000);
  }

}
