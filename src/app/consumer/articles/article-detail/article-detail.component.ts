import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { ArticleService } from './../../../services/article.service';
import { Article } from './../../../models/article';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit {

  sub: any;
  id: string;

  article: Observable<Article>;

  constructor(
    private route: ActivatedRoute,
    private arts: ArticleService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getArticle(this.id);
    });
  }

  getArticle(id) {
    this.article = this.arts.getArticle(id);
  }

  sanitizeImage(url) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
  }

}
