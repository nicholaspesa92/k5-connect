import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatListModule, MatIconModule, MatDialogModule,
  MatDialogRef, MAT_DIALOG_DATA, MatSlideToggleModule,
  MatExpansionModule, MatCheckboxModule, MatSnackBarModule,
  MatCardModule
} from '@angular/material';
import { TitlePipe } from '../../shared/title.pipe';
import { SelectedOptionPipe } from '../../shared/selected-option.pipe';
import { SettingsComponent } from './settings.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BottomSheetComponent } from './../../bottom-sheet/bottom-sheet.component';

import { AuthenticationService } from './../../services/authentication.service';
import { LocationService } from './../../services/location.service';
import { MemberService } from './../../services/member.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SettingsComponent,
        TitlePipe,
        SelectedOptionPipe,
        BottomSheetComponent
      ],
      imports: [
        MatListModule,
        MatIconModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [
        { provide: MatDialogRef },
        { provide: MAT_DIALOG_DATA, use: {}},
        AuthenticationService,
        MemberService,
        LocationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
