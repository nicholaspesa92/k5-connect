import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';

import { EditComponent } from './edit/edit.component';
import { AuthenticationService } from './../../services/authentication.service';
import { MemberService } from './../../services/member.service';
import { LocationService } from './../../services/location.service';
import { Member } from './../../models/member';
import { BusinessLocation } from './../../models/businesslocation';
import { environment } from './../../../environments/environment';

import * as firebase from 'firebase';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public settings;

  selectedOption: string;

  bId: string;

  mId: string;
  member$: any|Observable<Member>;
  locations$: any|Observable<BusinessLocation[]>;

  bLocations: any|BusinessLocation[];
  uLocations: any|BusinessLocation[];
  locForm: FormGroup;
  locArr: any[] = [];
  oldLocStructure: boolean;

  bSettings: any|Observable<{}>;
  bProducts: any;
  uProducts: any;
  prodForm: FormGroup;

  cats$: Observable<any>;

  saved: boolean;
  changes: boolean;

  loggedIn: boolean;

  start: boolean;
  path: string;

  members$: Observable<Member[]>;
  members: Member[];

  skip = '9sIB8kHmTkhw0xkY6o3HH86qvGH2';

  db: any;
  batch: any;

  constructor(
    public dialog: MatDialog,
    private as: AuthenticationService,
    private ms: MemberService,
    private ls: LocationService,
    private sb: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.bId = environment.businessId;
    this.mId = '';
    this.saved = false;
    this.changes = false;
    this.loggedIn = true;
    this.start = false;
    this.path = 'settings';
  }

  ngOnInit() {
    this.db = firebase.firestore();
    this.batch = this.db.batch();
    this.setSettings();
    this.createLocForm();
    this.createProdForm();
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.handleUserSubscription(user);
        } else if (user.isAnonymous) {
          this.loggedIn = false;
        }
      } else if (!user) {
        this.loggedIn = false;
      }
    });
  }

  getMembers(locations) {
    this.members$ = this.ms.getAllMembers();
    this.members$.subscribe((members) => {
      members.map((member) => {
        const path = `businesses/${this.bId}/members`;
        const mref = this.db.collection(path).doc(member.id);
        const newData = {};
        locations.map(location => {
          newData[location.id] = false;
        });
        if (member.followedLocations) {
          member.followedLocations.map(loc => {
            newData[loc] = true;
          });
        }
        this.batch.update(mref, { followedLocations: newData });
      });
      this.batch.commit()
        .then((res) => {
          console.log(res);
          console.log('SUCCESS');
        }).catch((error) => {
          console.log(error);
        });
    });
  }

  handleUserSubscription(user) {
    this.mId = user.uid;
    this.member$ = this.ms.getMember(this.mId);
    this.member$.subscribe((member) => {
      // this.uLocations = member.followedLocations ? member.followedLocations : {};
      this.uProducts = member.interestedProducts ? member.interestedProducts : {};
      if (member.followedLocations) {
        this.uLocations = member.followedLocations;
        member.followedLocations instanceof Array ? this.oldLocStructure = true : this.oldLocStructure = false;
      } else {
        this.uLocations = {};
        this.oldLocStructure = false;
      }
      console.log(this.uLocations instanceof Array);

      this.locations$ = this.ls.locationsAsObservableWithId();
      this.locations$.subscribe((locations) => {
        this.bLocations = locations ? locations : [];

        if (this.oldLocStructure) {
          console.log('old structure');
          this.createBusinessArray();
        } else {
          console.log('new structure');
          locations.map((location) => {
            const lGroup = <FormGroup>this.locForm;
            lGroup.addControl(location.id, new FormControl(false));
          });
          this.updateLocForm(this.uLocations);
        }

        // Batch Writing New Data Structure for Members Locations //
        // this.getMembers(this.bLocations);
        // End Batch Write //

        this.bSettings = this.as.getCurrentBusinessCategories();
        this.bSettings.subscribe((categories) => {
          this.bProducts = categories;
          categories.map((category) => {
            const pGroup = <FormGroup>this.prodForm;
            pGroup.addControl(category.slug, new FormControl(false));
          });
          this.updateProdForm(this.uProducts);
          this.start = true;
        });
      });
    });
  }

  onToggle(key, event) {
    this.saveNewValue(key, event.checked);
  }

  createLocForm() {
    this.locForm = this.fb.group({});
  }

  updateLocForm(data) {
    this.locForm.patchValue(data);
    console.log(this.locForm.value);
  }

  createProdForm() {
    this.prodForm = this.fb.group({});
  }

  updateProdForm(data) {
    this.prodForm.patchValue(data);
  }

  saveSettings() {
    const x = {
      followedLocations: this.oldLocStructure ? this.uLocations : this.locForm.value,
      interestedProducts: this.prodForm.value
    }

    this.ms.saveSettings(x, this.mId)
      .then(() => {
        this.resetSaved();
      })
      .catch(() => {
        this.snack('Error', 'OK', 2000);
      });
  }

  resetSaved() {
    this.saved = true;
    setTimeout(() => {
      this.saved = false;
      this.changes = false;
    }, 1000);
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

  lchanged(e, id) {
    this.changes = true;
    if (e.checked) {
      this.uLocations.push(id);
    } else if (!e.checked) {
      const x = this.uLocations.indexOf(id);
      this.uLocations.splice(x, 1);
    }
    console.log(this.uLocations);
  }

  pchanged() {
    this.changes = this.uProducts === this.prodForm.value ? false : true;
  }

  saveNewValue(key, value) {
    this.ms.saveMemberField(key, value, this.mId)
      .then(() => {

      })
      .catch(() => {
        this.snack('Error', 'OK', 2000);
      });
  }

  openEditSetting(setting) {
    console.log(setting);
    const config = {
      data: {
        items: setting
      }
    };
    const ref = this.dialog.open(EditComponent, config);
    ref.afterClosed().subscribe((result) => {
      // console.log(result);
    });
  }

  async checkExists() {
    const x = await this.as.isMemberX();
    console.log(x);
  }

  // Followed Locations Grouping (Old Data Structure, Delete When Ready) //
  createBusinessArray() {
    this.locArr = [];
    for (let i = 0; i < this.bLocations.length; i++) {
      const l = this.bLocations[i];
      const x = {
        name: l.name, id: l.id, follows: false
      }
      this.locArr.push(x);
    }
    this.createLocFormOld();
  }

  createLocFormOld() {
    this.locForm = this.fb.group({
      locations: this.buildLocations()
    });
    this.start = true;
  }

  buildLocations() {
    const ar = this.locArr.map(loc => {
      return this.fb.control(this.containsLocation(loc) ? true : false);
    });
    return this.fb.array(ar);
  }

  containsLocation(loc) {
    let exists = false;
    if (!this.uLocations) { return false; }
    for (let i = 0; i < this.uLocations.length; i++) {
      if (loc.id === this.uLocations[i]) {
        exists = true;
        break;
      }
    }
    return exists;
  }

  get locations() {
    return this.locForm.get('locations') as FormArray;
  }
  // End Followed Locations Grouping //

  // Interested Products Grouping (Old Data Structure, Delete When Ready) //
  createProdFormOld() {
    this.prodForm = this.fb.group({
      products: this.buildProducts()
    });
    this.start = true;
    console.log(this.locations.controls);
  }

  buildProducts() {
    const ar = this.bProducts.map(prod => {
      return this.fb.control(this.containsProduct(prod) ? true : false);
    });
    return this.fb.array(ar);
  }

  containsProduct(prod) {
    let exists = false;
    if (!this.uProducts) { return false; }
    for (let i = 0; i < this.uProducts.length; i++) {
      if (prod === this.uProducts[i]) {
        exists = true;
        break;
      }
    }
    return exists;
  }

  get products() {
    return this.prodForm.get('products') as FormArray;
  }
  // End Interested Products Grouping //

  setSettings() {
    this.settings = [
      {
        name: 'product interests',
        options: [
          {name: 'concentrates', selected: true},
          {name: 'edibles', selected: true},
          {name: 'flowers', selected: true},
        ],
        icon: 'tag_faces'
      },
      {
        name: 'medical or recreational',
        options: [
          {name: 'medical', selected: false},
          {name: 'recreational', selected: true},
        ],
        icon: 'local_florist'
      },
      {
        name: 'choose to follow locations',
        options: [
          {name: 'bend', selected: true},
          {name: 'portland', selected: true},
          {name: 'seattle', selected: false},
        ],
        icon: 'store'
      },
      {
        name: 'notifications',
        options: [
          {name: 'articles', selected: true},
          {name: 'promotions', selected: true},
          {name: 'rewards', selected: true},
        ],
        icon: 'notifications'
      }
    ];
  }

}

/* createBusinessArray() {
    for (let i = 0; i < this.bLocations.length; i++) {
      const l = this.bLocations[i];
      const x = {
        name: l.name, id: l.id, follows: false
      }
      this.locArr.push(x);
    }
    this.createLocForm();
  }

  createLocForm() {
    this.locForm = this.fb.group({
      locations: this.buildLocations()
    });
    console.log(this.locForm.value);
  }

  buildLocations() {
    const ar = this.locArr.map(loc => {
      console.log(loc);
      return this.fb.control(this.arrayContains(loc) ? true : false);
    });
    return this.fb.array(ar);
  }

  arrayContains(loc) {
    let exists = false;
    for (let i = 0; i < this.uLocations.length; i++) {
      if (loc.id === this.uLocations[i]) {
        exists = true;
        break;
      }
    }
    return exists;
  }

  get locations() {
    const x = this.locForm.value;
    return x.locations;
  } */

    /* mergeLocations() {
    const newArr = []
    for (let i = 0; i < this.bLocations.length; i++) {
      const x = this.bLocations[i];
      let exists = false;
      for (let j = 0; j < this.uLocations.length; j++) {
        const y = this.uLocations[j];
        if (x.id === y.id) {
          exists = true;
          break;
        }
        if (x.id !== y.id) {
          const z = {
            name: x.name,
            id: x.id,
            follows: false
          }
          exists = false;
        }
      }
      if (!exists) {
        const newl = {
          follows: false,
          id: x.id,
          name: x.name
        }
        this.uLocations.push(newl);
      }
    }
    console.log(this.uLocations);
  } */

  /*createForm() {
    this.locForm = this.fb.group({
      locations: []
    });
  }

  patchForm() {
    this.locForm.patchValue({
      locations: this.uLocations
    });
    console.log(this.locForm.value);
  }*/

