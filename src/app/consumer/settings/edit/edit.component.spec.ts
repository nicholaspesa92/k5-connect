import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatListModule, MatIconModule, MatDialogModule, MatCheckboxModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TitlePipe } from '../../../shared/title.pipe';
import { EditComponent } from './edit.component';

class MdDialogRefMock {
}

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditComponent, TitlePipe ],
      imports: [
        FormsModule,
        MatListModule,
        MatIconModule,
        MatDialogModule,
        MatCheckboxModule
      ],
      providers: [
        { provide: MatDialogRef },
        { provide: MAT_DIALOG_DATA, use: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
