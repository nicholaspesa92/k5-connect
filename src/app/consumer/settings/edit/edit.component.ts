import { Component, OnInit, Inject, Optional, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {

  chosenLocations: any[] = [];

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditComponent>
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onCheck(i, event) {
    this.data.items[i].follows = event.checked;
  }

}
