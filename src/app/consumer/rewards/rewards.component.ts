import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Observable ,  Subscription } from 'rxjs';
import 'rxjs-compat'
import { FormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { RewardService } from './../../services/reward.service';
import { FiltersService } from './../../services/filters.service';
import { AuthenticationService } from './../../services/authentication.service';
import { MemberService } from './../../services/member.service';
import { GiftService } from './../../services/gift.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { Reward } from './../../models/reward';
import { Member } from './../../models/member';
import { Gift } from './../../models/gift';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.scss']
})
export class RewardsComponent implements OnInit, OnDestroy {

  member$: Observable<Member>;
  member: Member;
  memberPoints = 0;

  rewards$: Observable<Reward[]>;
  rewards: Reward[];

  gifts: Gift[];
  gifts$: Observable<Gift[]>;

  settings$: Observable<any>;
  settings: any;
  rewardsBanner: any;

  loggedIn: boolean;
  showRewardTip: number;

  subs: any;

  state: any;

  constructor(
    private sanitizer: DomSanitizer,
    private rs: RewardService,
    private as: AuthenticationService,
    private ms: MemberService,
    public filtersService: FiltersService,
    private gs: GiftService,
    private sb: MatSnackBar,
    private urlS: UrlSanitizerService,
    private bpo: BreakpointObserver
  ) {
    this.loggedIn = true;
    this.showRewardTip = -1;
    this.rewards = [];
    this.rewardsBanner = '';
    this.gifts = [];

    const layout = this.bpo.observe(['(max-width: 600px)']);
    layout.subscribe((state) => {
      this.state = state;
      console.log(this.state);
    })
  }

  ngOnInit() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          console.log(user.uid);
          this.loggedIn = true;
          this.zipSubs(user.uid);
        } else if (user.isAnonymous) {
          this.loggedIn = false;
          this.getRewards();
        }
      } else {
        this.loggedIn = false;
      }
    });
  }

  ngOnDestroy() {
    try {
      this.subs.unsubscribe();
    } catch (err) { }
  }

  zipSubs(uid) {
    this.member$ = this.ms.getMember(uid);
    this.gifts$ = this.gs.getGiftsFromMember(uid);
    this.rewards$ = this.rs.getRewards();
    this.settings$ = this.rs.getSettings();
    this.subs = Observable.combineLatest(this.member$, this.gifts$, this.rewards$, this.settings$)
      .subscribe(([m, g, r, s]) => {
        console.log(m, g, r, s);
        if (m.hasOwnProperty('currentPoints')) { this.memberPoints = m.currentPoints; }
        this.gifts = g;
        this.rewards = r;
        this.settings = s;
        this.rewardsBanner = this.urlS.getSanitizedStyle(s.rewardsBanner);
      });
  }

  getRewards() {
    this.rewards$ = this.rs.getRewards();
    this.rewards$.subscribe((rewards) => {
      this.rewards = rewards;
    });
  }

  showTip() {
    this.snack('Visit our location to receive rewards', 'OK', 3000);
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

}
