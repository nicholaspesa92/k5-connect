import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatIconModule, MatCardModule, MatInputModule, MatSelectModule, MatProgressSpinnerModule,
  MatSnackBarModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { RewardsComponent } from './rewards.component';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationService } from './../../services/authentication.service';
import { LocationService } from './../../services/location.service';
import { RewardService } from './../../services/reward.service';
import { MemberService } from './../../services/member.service';
import { FiltersService } from '../../services/filters.service';
import { CategoryMatchPipe } from './../../pipes/category-match.pipe';
import { GiftService } from './../../services/gift.service';
import { BottomSheetComponent } from './../../bottom-sheet/bottom-sheet.component';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../../environments/environment';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

describe('RewardsComponent', () => {
  let component: RewardsComponent;
  let fixture: ComponentFixture<RewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RewardsComponent,
        CategoryMatchPipe,
        BottomSheetComponent
      ],
      imports: [
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        FlexLayoutModule,
        FormsModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig)
      ],
      providers: [
        AuthenticationService,
        LocationService,
        RewardService,
        MemberService,
        FiltersService,
        GiftService,
        UrlSanitizerService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
