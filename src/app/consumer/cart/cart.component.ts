import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { RemoveComponent } from './remove/remove.component';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { AuthenticationService } from './../../services/authentication.service';
import { OrderService } from './../../services/order.service';

import { Order } from './../../models/order';
import { OrderItem } from './../../models/order-item';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  lId: string;

  orderItems: OrderItem[];
  total: number;

  authFlag: boolean;
  user$: any;
  user: any;
  uid: string;

  sending: boolean;

  orderAllowed: boolean;

  constructor(
    private as: AuthenticationService,
    private os: OrderService,
    private dialog: MatDialog,
    private sb: MatSnackBar,
    private router: Router,
    private aRoute: ActivatedRoute
  ) {
    this.total = 0;
    this.sending = false;
    this.orderAllowed = true;
  }

  ngOnInit() {
    this.lId = localStorage.getItem('lId');
    this.orderAllowed = this.os.orderAllowed;
    this.getOrderItems();
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.uid = user.uid;
          this.getUser(user.uid);
          this.authFlag = true;
        } else if (user.isAnonymous) {
          this.authFlag = false;
        }
      }
    });
  }

  getOrderItems() {
    this.orderItems = this.os.getCart();
    this.calculateTotal();
  }

  calculateTotal() {
    this.total = 0;
    this.orderItems.map((item) => {
      this.total += (item.price * item.quantity);
    });
  }

  getUser(uid) {
    this.user$ = this.as.getCurrentUserFromFirebase(uid);
    this.user$.subscribe((user) => {
      this.user = user;
    });
  }

  sendOrder() {
    if (this.user) {
      this.sending = true;
      const o = {
        user: this.user,
        userId: this.uid,
        createdAt: new Date(),
        products: this.orderItems,
        total: this.total,
        status: 'new',
        canceled: false,
        archived: false
      };
      this.os.createOrder(this.lId, o)
        .then((order) => {
          this.sending = false;
          this.orderItems = [];
          this.os.clearCart();
          this.router.navigate(['consumer', this.lId, 'orders', order.id]);
        }).catch((error) => {
          this.sending = false;
          this.snack('Error creating order, please try again', 'OK', 3000);
        });
    }
  }

  addQuantity(item) {
    item.quantity += 1;
    this.total += item.price;
  }
  subtractQuantity(item) {
    item.quantity -= 1;
    this.total -= item.price;
  }

  removeItem(item) {
    const config = {
      data: item
    };
    const dRef = this.dialog.open(RemoveComponent, config);
    dRef.afterClosed().subscribe((res) => {
      console.log(res);
      if (res === true) {
        console.log('removal');
        this.os.removeItem(item);
        this.getOrderItems();
      } else if (res === false) {

      }
    });
  }

  confirmItemRemoval(item) {
    const config = {
      data: item
    };
    const dRef = this.dialog.open(RemoveComponent, config);
    dRef.afterClosed().subscribe((res) => {
      if (res === true) {
        this.os.removeItem(item);
        this.getOrderItems();
      } else if (res === false) {

      }
    });
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

  gotoLogin() {
    const navExtras: NavigationExtras = {
      queryParams: { 'redirectUrl': this.router.url }
    }
    this.router.navigate(['onboarding'], navExtras);
  }

  gotoMenu() {
    this.router.navigate(['consumer/menu']);
  }
}
