import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule, MatSnackBarModule, MatButtonModule, MatDialogModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';

import { AuthenticationService } from './../../services/authentication.service';
import { OrderService } from './../../services/order.service';

import { CartComponent } from './cart.component';

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatSnackBarModule,
        MatButtonModule,
        MatDialogModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      declarations: [ CartComponent ],
      providers: [
        AuthenticationService,
        OrderService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
