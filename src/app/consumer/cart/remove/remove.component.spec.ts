import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatCheckboxModule, MatDialogRef, MAT_DIALOG_DATA, MatSnackBarModule, MatButtonModule } from '@angular/material';

import { RemoveComponent } from './remove.component';

describe('RemoveComponent', () => {
  let component: RemoveComponent;
  let fixture: ComponentFixture<RemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveComponent ],
      imports: [
        MatDialogModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatButtonModule
      ],
      providers: [
        { provide: MatDialogRef },
        { provide: MAT_DIALOG_DATA, use: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
