import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatInputModule,
  MatDatepickerModule,
  MatSelectModule,
  MatSnackBarModule,
  MatIconModule
} from '@angular/material';
import { Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileEditComponent } from './profile-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MemberService } from './../../../services/member.service';
import { WindowService } from './../../../services/window.service';
import { PhoneNumberPipe } from './../../../shared/phone-number.pipe';

import { AuthenticationService } from './../../../services/authentication.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-test-dummy',
  template: `<p>TEST</p>`
})
export class TestDummyComponent { }

describe('ProfileEditComponent', () => {
  let component: ProfileEditComponent;
  let fixture: ComponentFixture<ProfileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileEditComponent, PhoneNumberPipe, TestDummyComponent ],
      imports: [
        MatInputModule,
        MatDatepickerModule,
        MatSelectModule,
        MatSnackBarModule,
        MatIconModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          { path: 'consumer/profile', component: TestDummyComponent }
        ]),
        FormsModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
      ],
      providers: [MemberService, AuthenticationService, WindowService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
