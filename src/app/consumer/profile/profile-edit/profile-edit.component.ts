import { Component,  OnInit, HostBinding, AfterViewInit, AfterContentInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms'
import { MatSnackBar } from '@angular/material';
import { slideRightToLeft } from '../../../shared/animation';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as firebase from 'firebase';

import { AuthenticationService } from './../../../services/authentication.service';
import { MemberService } from './../../../services/member.service';
import { WindowService } from './../../../services/window.service';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss'],
  animations: [slideRightToLeft]
})
export class ProfileEditComponent implements OnInit, AfterViewInit, AfterContentInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  fieldName: string;
  fieldValue: any;
  fieldType: number;

  fieldNameAlt: string;

  startDate = new Date(1996,  0,  1);

  aForm: FormGroup;
  address = {};
  street: string;
  city: string;
  state: string;
  zip: number;

  states = [];

  SINGLE_LINE_TEXT = 0;
  DATE = 1;
  ADDRESS = 2;
  PASSWORD = 3;
  PHONE = 4;

  pwForm: FormGroup;
  oldPassword: string;
  newPassword: string;
  emailForPw: string;

  pnForm: FormGroup;
  vForm: FormGroup;
  vHash: string;
  vCode: string;
  wRef: any;
  step: number;

  constructor(
    private ms: MemberService,
    private as: AuthenticationService,
    private router: Router,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private ws: WindowService,
    private location: Location
  ) {
    this.fieldName = '';
    this.fieldValue = '';
    this.fieldType = 0;

    this.oldPassword = '';
    this.newPassword = '';
    this.emailForPw = '';

    this.step = 0;
  }

  ngOnInit() {
    this.fieldName = this.ms.getFieldName();
    this.fieldValue = this.ms.getFieldValue();
    this.fieldType = this.ms.getFieldType();

    if (!this.fieldType) { this.router.navigate(['consumer/profile']); }

    this.setStates();

    if (this.fieldType === this.DATE) {
      if (this.fieldValue) { this.startDate = this.fieldValue; }
    }
    if (this.fieldType === this.ADDRESS) {
      if (this.fieldValue) {
        this.createAForm(this.fieldValue.street, this.fieldValue.city, this.fieldValue.state, this.fieldValue.zip);
      } else if (!this.fieldValue) {
        this.createAForm('', '', '', '');
      }
    }
    if (this.fieldType === this.PASSWORD) {
      this.emailForPw = this.ms.getEmailForPw();
      this.createPwForm();
    }
    if (this.fieldType === this.PHONE) {
      this.createPnForm();
      this.step = 1;
    }

    this.fieldNameAlt = this.fieldName.replace(/([a-z])([A-Z])/g, '$1 $2').replace(/^./, function(str) { return str.toUpperCase() });
  }

  ngAfterViewInit() {
    // if (this.fieldType === this.PHONE) { this.createRecaptcha(); };
    try {
      this.createRecaptcha();
    } catch (error) {
    }
  }

  ngAfterContentInit() {

  }

  createAForm(street, city, state, zip) {
    this.aForm = this.fb.group({
      street: [street, Validators.required],
      city: [city, Validators.required],
      state: [state, Validators.required],
      zip: [zip, Validators.required]
    });
  }

  createPwForm() {
    this.pwForm = this.fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required]
    });
  }

  createPnForm() {
    this.pnForm = this.fb.group({
      phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_REG_EX)]]
    });
    this.createVForm();
  }

  createVForm() {
    this.vForm = this.fb.group({
      1: ['', Validators.required],
      2: ['', Validators.required],
      3: ['', Validators.required],
      4: ['', Validators.required],
      5: ['', Validators.required],
      6: ['', Validators.required]
    });
  }

  createRecaptcha() {
    this.wRef = this.ws.windowRef;
    this.wRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible'
    });
    this.wRef.recaptchaVerifier.render();
  }

  verificationTransition(step) {
    this.step = step;
  }

  updateField() {
    if (this.fieldType === this.ADDRESS) {
      const af = this.aForm.value;
      this.fieldValue = {
        street: af.street,
        city: af.city,
        state: af.state,
        zip: af.zip
      };
      this.as.updateUser(this.fieldName, this.fieldValue);
    } else {
      this.as.updateUser(this.fieldName, this.fieldValue);
    }
    this.snackBar.open('Profile Updated', 'OK', { duration: 2000 });
    this.goBack();
  }

  updatePassword() {
    const pwf = this.pwForm.value;
      this.as.emailSignIn(this.emailForPw, pwf.oldPassword)
        .then((user) => {
          this.as.updatePassword(pwf.newPassword)
            .then((success) => {
              this.createPwForm();
              this.snackBar.open('Password successfully reset', 'OK', {
                duration: 2000
              }).afterDismissed().subscribe(() => {
                this.goBack();
              });
            })
            .catch((error) => {
              this.snackBar.open('Error resetting password', 'OK', {
                duration: 2000
              });
            });
        })
        .catch((error) => {
          this.snackBar.open('Old Password is Incorrect, Please Try Again', 'OK', {
            duration: 3000
          }).afterDismissed().subscribe(() => {
            // this.as.signOut().then(this.router.navigate)
          });
        });
  }

  sendVCode() {
    const number = '+1' + this.pnForm.value.phoneNumber;
    const appVerifier = this.wRef.recaptchaVerifier;
    this.as.phoneUpdate(number, appVerifier)
      .then((vHash) => {
        this.vHash = vHash;
        this.verificationTransition(2);
      })
      .catch((error) => {
        this.snack('Error With Phone Number', 'OK', 2000);
      });
  }

  verifyCode() {
    const vCode = this.concatCode();
    this.as.phoneUpdateVerifier(this.vHash, vCode)
      .then((success) => {
        this.snack('Successfully Updated Phone Number', 'OK', 3000);
        this.as.updateUser('phoneNumber', this.pnForm.value.phoneNumber);
        this.goBack();
      })
      .catch((error) => {
        if (error.code === 'auth/credential-already-in-use') {
          this.snack('That Phone Number is in use on another account', 'OK', 3000);
        } else  {
          this.snack('Verrification Error, Please Try Again', 'OK', 3000);
        }
        this.verificationTransition(1);
      });
  }

  concatCode() {
    const x = this.vForm.value;
    let code = '';
    for (let i = 0; i < 6; i++) {
      code += x[i + 1];
    }
    return code;
  }

  snack(message, action, duration) {
    this.snackBar.open(message, action, { duration: duration });
  }

  goBack() {
    this.router.navigate(['consumer/profile']);
  }

  setStates() {
    this.states = [
      {name: 'Alabama', abbrev: 'AL'},
      {name: 'Alaska', abbrev: 'AK'},
      {name: 'Arizona', abbrev: 'AZ'},
      {name: 'Arkansas', abbrev: 'AR'},
      {name: 'California', abbrev: 'CA'},
      {name: 'Colorado', abbrev: 'CO'},
      {name: 'Connecticut', abbrev: 'CT'},
      {name: 'Delaware', abbrev: 'DE'},
      {name: 'District of Columbia', abbrev: 'DC'},
      {name: 'Florida', abbrev: 'FL'},
      {name: 'Georgia', abbrev: 'GA'},
      {name: 'Hawaii', abbrev: 'HI'},
      {name: 'Idaho', abbrev: 'ID'},
      {name: 'Illinois', abbrev: 'IL'},
      {name: 'Indiana', abbrev: 'IN'},
      {name: 'Iowa', abbrev: 'IA'},
      {name: 'Kansa', abbrev: 'KS'},
      {name: 'Kentucky', abbrev: 'KY'},
      {name: 'Lousiana', abbrev: 'LA'},
      {name: 'Maine', abbrev: 'ME'},
      {name: 'Maryland', abbrev: 'MD'},
      {name: 'Massachusetts', abbrev: 'MA'},
      {name: 'Michigan', abbrev: 'MI'},
      {name: 'Minnesota', abbrev: 'MN'},
      {name: 'Mississippi', abbrev: 'MS'},
      {name: 'Missouri', abbrev: 'MO'},
      {name: 'Montana', abbrev: 'MT'},
      {name: 'Nebraska', abbrev: 'NE'},
      {name: 'Nevada', abbrev: 'NV'},
      {name: 'New Hampshire', abbrev: 'NH'},
      {name: 'New Jersey', abbrev: 'NJ'},
      {name: 'New Mexico', abbrev: 'NM'},
      {name: 'New York', abbrev: 'NY'},
      {name: 'North Carolina', abbrev: 'NC'},
      {name: 'North Dakota', abbrev: 'ND'},
      {name: 'Ohio', abbrev: 'OH'},
      {name: 'Oklahoma', abbrev: 'OK'},
      {name: 'Oregon', abbrev: 'OR'},
      {name: 'Pennsylvania', abbrev: 'PA'},
      {name: 'Rhode Island', abbrev: 'RI'},
      {name: 'South Carolina', abbrev: 'SC'},
      {name: 'South Dakota', abbrev: 'SD'},
      {name: 'Tennessee', abbrev: 'TN'},
      {name: 'Texas', abbrev: 'TX'},
      {name: 'Utah', abbrev: 'UT'},
      {name: 'Vermont', abbrev: 'VT'},
      {name: 'Virginia', abbrev: 'VA'},
      {name: 'Washington', abbrev: 'WA'},
      {name: 'West Virginia', abbrev: 'WV'},
      {name: 'Wisconsin', abbrev: 'WI'},
      {name: 'Wyoming', abbrev: 'WY'}
    ];
  }

}
