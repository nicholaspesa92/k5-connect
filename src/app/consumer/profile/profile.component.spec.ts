import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatListModule,
  MatIconModule,
  MatSnackBarModule,
  MatDatepickerModule,
  MatInputModule
} from '@angular/material';
import { ProfileComponent } from './profile.component';
import { PhoneNumberPipe } from './../../shared/phone-number.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BottomSheetComponent } from './../../bottom-sheet/bottom-sheet.component';

import { MemberService } from './../../services/member.service';
import { AuthenticationService } from './../../services/authentication.service';
import { ToolbarService } from './../../services/toolbar.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from './../../../environments/environment';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        PhoneNumberPipe,
        BottomSheetComponent
      ],
      imports: [
        MatListModule,
        MatIconModule,
        MatSnackBarModule,
        MatDatepickerModule,
        MatInputModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [MemberService, AuthenticationService, ToolbarService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
