import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatSnackBar, MatRipple } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { AngularFireStorage } from 'angularfire2/storage';
import 'rxjs-compat';

import { MemberService } from './../../services/member.service';
import { AuthenticationService } from './../../services/authentication.service';
import { ToolbarService } from './../../services/toolbar.service';

import { Member } from './../../models/member';
import { User } from './../../models/user';

import { environment } from './../../../environments/environment';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {

  member: Observable<Member>;
  user: Observable<User>;
  memberObj: Member;
  userObj: User;
  currentPoints: number;

  fuser: Observable<any>;
  combinedUser: any;

  loggedIn: boolean;
  path: string;

  businessId: string;
  memberId: string;

  uid: string;
  memberDocPath: string;

  testMemOb: Observable<Member>;

  fieldEditable: number;

  displayName: FormControl;
  email: FormControl;
  phoneNumber: FormControl;
  birthday: FormControl;

  percent: Observable<number>;
  downloadUrl: Observable<string>;

  up = false;

  @ViewChild(MatRipple) ripple: MatRipple;
  centered = false;
  disabled = false;
  unbounded = false;
  rounded = true;
  radius = 700;
  rippleSpeed = 0.1;
  rippleColor = '';

  @ViewChild('main') main: ElementRef;
  @ViewChild('vdname') vdname: ElementRef;
  @ViewChild('vemail') vemail: ElementRef;
  @ViewChild('vbday') vbday: ElementRef;

  constructor(
    private ms: MemberService,
    private as: AuthenticationService,
    private router: Router,
    private snackBar: MatSnackBar,
    private ts: ToolbarService,
    private cdr: ChangeDetectorRef,
    private storage: AngularFireStorage
  ) {
    this.businessId = environment.businessId;
    this.memberDocPath = `businesses/${this.businessId}/members`;
    this.fieldEditable = 0;
    this.setUpFormControls();
    this.up = true;
    this.loggedIn = true;
    this.path = 'profile';
  }

  ngOnInit() {
    this.as.user.subscribe((user) => {
      if (user) {
        if (!user.isAnonymous) {
          this.loggedIn = true;
          this.memberId = user.uid;
          this.member = this.ms.getMember(this.memberId);
          this.fuser = this.as.getCurrentUserFromFirebase(user.uid);
          this.combinedUser = Observable.combineLatest(this.member, this.fuser)
            .subscribe(([m, u]) => {
              this.memberObj = m;
              this.userObj = u;
              this.currentPoints = m.currentPoints;
              this.displayName = new FormControl(u.displayName, Validators.required);
              this.email = new FormControl(u.email, [Validators.required, Validators.email]);
              this.phoneNumber = new FormControl(u.phoneNumber, [Validators.required, Validators.pattern(PHONE_REG_EX)]);
              const x = new Date(u.birthday);
              this.birthday = new FormControl(x, Validators.required);
            });
        } else if (user.isAnonymous) {
          this.loggedIn = false;
        }
      } else if (!user) {
        this.loggedIn = false;
      }
    });
    this.ts.setToolbarShadow(false);
  }

  ngAfterViewInit() {
  }

  setUpFormControls() {
    this.displayName = new FormControl('', Validators.required);
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.phoneNumber = new FormControl('', [Validators.required, Validators.pattern(PHONE_REG_EX)]);
    this.birthday = new FormControl('', Validators.required);
  }

  editField(name, value, type) {
    this.ms.setFieldName(name);
    this.ms.setFieldValue(value);
    this.ms.setFieldType(type);
  }

  editPwField(name, value, type, email) {
    this.ms.setFieldName(name);
    this.ms.setFieldValue(value);
    this.ms.setFieldType(type);
    this.ms.setEmailForPw(email);
  }

  facebookLinking() {
    this.as.facebookLogin()
      .then((success) => {
        this.openSnack('Facebook Linked', 'OK', 2000);
        this.as.updateUser('facebookLinked', true);
      })
      .catch((error) => {
        if (error.code === 'auth/provider-already-linked') {
          this.openSnack('Facebook account already linked', 'OK', 2000);
          this.as.updateUser('facebookLinked', true);
        } else {
          this.openSnack('Error Linking Facebook, Please try again', 'OK', 2000);
          this.as.updateUser('facebookLinked', false);
        }
      });
  }
  googleLinking() {
    if (this.userObj.googleLinked) { this.openSnack('Google Account Already Linked', 'OK', 2000); return; }
    this.as.googleLogin()
      .then((success) => {
        this.openSnack('Google Linked', 'OK', 2000);
        this.as.updateUser('googleLinked', true);
      })
      .catch((error) => {
        if (error.code === 'auth/provider-already-linked') {
          this.openSnack('Google Account already linked', 'OK', 2000);
          this.as.updateUser('googleLinked', true);
        } else if (error.code === 'auth/credential-already-in-use') {
          this.openSnack('Google Account linked to another account', 'OK', 5000);
          this.as.updateUser('googleLinked', false);
        } else {
          this.openSnack('Error Linking Google, Please try again', 'OK', 2000);
          this.as.updateUser('googleLinked', false);
        }
      });
  }

  inlineEdit(i) {
    this.fieldEditable = i;
    this.cdr.detectChanges();
    switch (i) {
      case 1:
        this.dnameScroll();
        break;
      case 2:
        this.emailScroll();
        break;
      case 3:
        this.bdayScroll();
        break;
    }
  }
  cancelInlineEdit() {
    this.fieldEditable = 0;
  }
  updateInlineEdit(key, value) {
    if (key === 'displayName') {
      this.as.updateUserDisplayName(value.value)
        .then(() => {
          this.updateFirebaseUserAfterAuth(key, value.value);
        })
        .catch((error) => {
          this.openSnack('Error Updating Field', 'OK', 2000);
        });
    } else if (key === 'email') {
      this.as.updateUserEmail(value.value)
        .then(() => {
          this.updateFirebaseUserAfterAuth(key, value.value);
        })
        .catch((error) => {
          this.openSnack('Error Updating Field', 'OK', 2000);
        });
    } else if (key === 'phoneNumber') {
      this.as.updateUserPhoneNumber(value.value)
        .then(() => {
          this.updateFirebaseUserAfterAuth(key, value.value);
        })
        .catch((error) => {
          this.openSnack('Error Updating Field', 'OK', 2000);
        });
    } else if (key === 'birthday') {
      this.updateFirebaseUserAfterAuth(key, value.value);
    }
  }

  updateFirebaseUserAfterAuth(key, value) {
    this.as.updateUser(key, value);
    this.fieldEditable = 0;
    const x = key.replace(/([a-z])([A-Z])/g, '$1 $2').replace(/^./, function(str) { return str.toUpperCase() });
    this.openSnack(x + ' ' + 'Updated', 'OK', 2000);
  }

  uploadImage(event) {
    const file = event.target.files[0];
    const path = `users/${this.memberId}/${this.slugName(this.userObj.displayName)}`;
    const task = this.storage.upload(path, file);

    this.percent = task.percentageChanges();
    this.percent.subscribe((percent) => {
    });
    this.downloadUrl = task.downloadURL();
    this.downloadUrl.subscribe((url) => {
      this.updateFirebaseUserAfterAuth('photoUrl', url);
    });
  }

  openSnack(message, action, duration) {
    this.snackBar.open(message, action, {
      duration: duration
    });
  }

  slugName(name) {
    return name.replace(/ /g, '-').toLowerCase();
  }

  checkEditable(n) {
    return n !== this.fieldEditable;
  }
  checkAnim(n) {
    return n === this.fieldEditable;
  }

  scrollUp(eTop) {
    this.main.nativeElement.scrollTo(0, eTop, 500);
  }

  dnameScroll() {
    this.scrollUp(this.vdname.nativeElement.offsetTop);
  }
  emailScroll() {
    this.scrollUp(this.vemail.nativeElement.offsetTop);
  }
  bdayScroll() {
    this.scrollUp(this.vbday.nativeElement.offsetTop);
  }

}
