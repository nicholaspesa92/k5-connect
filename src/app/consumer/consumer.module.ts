import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ConsumerRoutingModule } from './consumer-routing.module';
import {
  MatButtonModule, MatIconModule, MatMenuModule, MatSidenavModule, MatToolbarModule,
  MatListModule, MatCardModule, MatInputModule, MatSelectModule, MatTabsModule, MatDatepickerModule, MatNativeDateModule,
  MatDialogModule, MatCheckboxModule, MatChipsModule, MatRadioModule, MatProgressSpinnerModule,
  MatProgressBarModule, MatSnackBarModule, MatSlideToggleModule, MatExpansionModule, MatTooltipModule, MatRippleModule,
  MatStepperModule, MatAutocompleteModule, MatBottomSheetModule, MatBadgeModule
} from '@angular/material';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SwiperModule } from 'angular2-useful-swiper';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { TitlePipe } from '../shared/title.pipe';
import { SharedModule } from '../shared/shared.module';
import { SelectedOptionPipe } from '../shared/selected-option.pipe';
import { MenuByCategoryPipe } from '../shared/menu-by-category.pipe';
import { SearchMenuPipe } from '../shared/search-menu.pipe';
import { FilterMenuPipe } from '../shared/filter-menu.pipe';
import { GroupByPipe } from '../shared/group-by.pipe';
import { PhoneNumberPipe } from '../shared/phone-number.pipe';
import { CurrencyPipe } from '@angular/common';

import { SearchStringService } from '../services/search-string.service';
import { FiltersService } from '../services/filters.service';
import { FocusDirective } from '../focus.directive';

import { ConsumerComponent } from './consumer.component';
import { ActivityComponent } from './activity/activity.component';
import { ArticlesComponent } from './articles/articles.component';
import { RewardsComponent } from './rewards/rewards.component';
import { MenuComponent } from './menu/menu.component';
import { LocationsComponent } from './locations/locations.component';
import { SupportComponent } from './support/support.component';
import { MessagesComponent } from './messages/messages.component';
import { DetailsComponent } from './locations/details/details.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DetailComponent } from './menu/detail/detail.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { SettingsComponent } from './settings/settings.component';
import { EditComponent } from './settings/edit/edit.component';
import { FilterComponent } from './filter/filter.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { HomeComponent } from './home/home.component';
import { BottomSheetComponent } from './../bottom-sheet/bottom-sheet.component';
import { CartComponent } from './cart/cart.component';
import { OrdersComponent } from './orders/orders.component';
import { RemoveComponent } from './cart/remove/remove.component';
import { OrderDetailComponent } from './orders/order-detail/order-detail.component';
import { StatusBarComponent } from './../ui-elements/status-bar/status-bar.component';
import { ProductItemComponent } from './../ui-elements/product-item/product-item.component';

import { LocationService } from './../services/location.service';
import { AuthenticationService } from './../services/authentication.service';
import { RewardService } from './../services/reward.service';
import { ActivityService } from './../services/activity.service';
import { ToolbarService } from './../services/toolbar.service';
import { MemberService } from './../services/member.service';
import { MenuService } from './../services/menu.service';
import { SupportService } from './../services/support.service';
import { GiftService } from './../services/gift.service';
import { WindowService } from './../services/window.service';
import { ArticleService } from './../services/article.service';
import { UrlSanitizerService } from './../services/url-sanitizer.service';
import { GooglePlacesService } from './../services/google-places.service';
import { OrderService } from './../services/order.service';

import { AuthGuard } from './../guards/auth.guard';
import { HomeGuard } from './../guards/home.guard';
import { FilterLocationComponent } from './filter-location/filter-location.component';
import { ArticleDetailComponent } from './articles/article-detail/article-detail.component';
import { SaveFavoriteDialogComponent } from './consumer.component';
import { environment } from 'environments/environment.prod';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

export class HammerConfig extends HammerGestureConfig {
  overrrides = <any>{
    'swipe': { velocity: 0.4, threshold: 20, direction: Hammer.DIRECTION_ALL },
    'pinch': { enable: false },
    'rotate': { enable: false },
    'pan': { velocity: 0.4, threshold: 20, enable: true, direction: Hammer.DIRECTION_ALL }
  }
}

import { VirtualScrollModule } from 'angular2-virtual-scroll';

@NgModule({
  imports: [
    SharedModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatDialogModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatTooltipModule,
    MatRippleModule,
    MatStepperModule,
    MatAutocompleteModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    ConsumerRoutingModule,
    CommonModule,
    LazyLoadImagesModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: environment.firebase.apiKey,
      libraries: ['places']
    }),
    CloudinaryModule.forRoot(cloudinary, cloudinaryConfig),
    ScrollDispatchModule,
    MatBottomSheetModule,
    VirtualScrollModule,
    MatBadgeModule
  ],
  exports: [RouterModule],
  declarations: [
    ConsumerComponent,
    ArticlesComponent,
    ActivityComponent,
    RewardsComponent,
    MenuComponent,
    LocationsComponent,
    MessagesComponent,
    DetailsComponent,
    NavigationComponent,
    DetailComponent,
    ProfileComponent,
    SupportComponent,
    TermsComponent,
    PrivacyComponent,
    SettingsComponent,
    EditComponent,
    TitlePipe,
    SelectedOptionPipe,
    MenuByCategoryPipe,
    FilterMenuPipe,
    PhoneNumberPipe,
    FilterComponent,
    ProfileEditComponent,
    HomeComponent,
    FilterLocationComponent,
    BottomSheetComponent,
    ArticleDetailComponent,
    CartComponent,
    OrdersComponent,
    RemoveComponent,
    OrderDetailComponent,
    SaveFavoriteDialogComponent,
    StatusBarComponent,
    ProductItemComponent
  ],
  entryComponents: [
    EditComponent,
    FilterComponent,
    FilterLocationComponent,
    RemoveComponent,
    SaveFavoriteDialogComponent,
    BottomSheetComponent
  ],
  providers: [
    SearchStringService,
    FiltersService,
    LocationService,
    AuthenticationService,
    RewardService,
    ActivityService,
    ToolbarService,
    AuthGuard,
    MemberService,
    MenuService,
    HomeGuard,
    SupportService,
    GiftService,
    WindowService,
    ArticleService,
    UrlSanitizerService,
    GooglePlacesService,
    OrderService,
    CurrencyPipe,
    { provide: HAMMER_GESTURE_CONFIG, useClass: HammerConfig }
  ]
})
export class ConsumerModule { }
