import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { OnboardingGuard } from './onboarding.guard';
import {
  MatSnackBarModule,
  MatTabsModule,
  MatProgressBarModule,
  MatSelectModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { SetupComponent } from './../onboarding/setup/setup.component';
import { TextMaskModule } from 'angular2-text-mask';

import { AuthenticationService } from './../services/authentication.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../environments/environment';

describe('OnboardingGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule,
        MatSnackBarModule,
        MatTabsModule,
        MatProgressBarModule,
        MatSelectModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        TextMaskModule,
        MatDatepickerModule,
        MatNativeDateModule
      ],
      providers: [OnboardingGuard, AuthenticationService]
    });
  });

  it('should ...', inject([OnboardingGuard], (guard: OnboardingGuard) => {
    expect(guard).toBeTruthy();
  }));
});
