import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';



import { AuthenticationService } from './../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private as: AuthenticationService,
    private router: Router
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.as.currentUserObservable
      .take(1)
      .map(authState => !!authState)
      .do(auth => this.handleAuth(auth));
  }

  handleAuth(auth) {
    return !auth ? this.router.navigate(['onboarding']) : true;
    /*if (!auth && !this.as.checkIfUserExists()) {
      return this.router.navigate(['onboarding']);
    } else if (auth && !this.as.checkIfUserExists()) {
      return true;
    } else if (auth && this.as.checkIfUserExists()) {
      return this.router.navigate(['consumer']);
    }*/
  }
}
