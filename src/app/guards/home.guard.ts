import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from './../services/authentication.service';

@Injectable()
export class HomeGuard implements CanActivate {

  bSettings: Observable<{}>;
  res: any;

  constructor(
    private as: AuthenticationService,
    private router: Router
  ) {
    this.res = false;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.bSettings = this.as.getCurrentBusinessSettings();
    this.bSettings.subscribe((setting: any) => {
      if (!setting.homepage) { this.res = this.router.navigate(['consumer/menu']); }
      if (setting.homepage) { this.res = true; }
    });
    return this.res;
  }
}
