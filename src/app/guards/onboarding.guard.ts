import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable ,  Observer } from 'rxjs';

import { AuthenticationService } from './../services/authentication.service';
import { SetupComponent } from './../onboarding/setup/setup.component';

@Injectable()
export class OnboardingGuard implements CanDeactivate<SetupComponent> {

  constructor(
    private as: AuthenticationService,
    private router: Router
  ) { }

  canDeactivate(comp: SetupComponent) {
    if (comp.deactivate()) {
      return true;
    } else {
      return Observable.create((observer: Observer<boolean>) => {
        comp.canLeave().subscribe((result) => {
          observer.next(result);
        });
      });
    }
  }
}
