import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';

import { AuthenticationService } from './../services/authentication.service';
import { UrlService } from './../services/url.service';

@Component({
  selector: 'app-bottom-sheet',
  templateUrl: './bottom-sheet.component.html',
  styleUrls: ['./bottom-sheet.component.scss']
})
export class BottomSheetComponent implements OnInit {

  bSettings: Observable<{}>;

  @Input() returnPath: string;

  config: any;
  baseText: string;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private bsRef: MatBottomSheetRef<BottomSheetComponent>,
    private router: Router,
    private as: AuthenticationService,
    private us: UrlService
  ) {
    this.baseText = 'You must have an account to view this page';
  }

  ngOnInit() {
    this.bSettings = this.as.getCurrentBusinessSettings();
    if (this.data) {
      this.config = this.data;
    }
  }

  gotoLogin() {
    const navExtras: NavigationExtras = {
      queryParams: { 'redirectUrl': this.us.lastPath }
    }
    this.router.navigate(['onboarding'], navExtras);
    this.bsRef.dismiss();
  }

}
