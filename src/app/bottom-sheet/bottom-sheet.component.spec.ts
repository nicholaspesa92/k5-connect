import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA, MatIconModule } from '@angular/material';

import { BottomSheetComponent } from './bottom-sheet.component';
import { AuthenticationService } from './../services/authentication.service';
import { UrlService } from './../services/url.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { environment } from './../../environments/environment';

describe('BottomSheetComponent', () => {
  let component: BottomSheetComponent;
  let fixture: ComponentFixture<BottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomSheetComponent ],
      imports: [
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        MatSnackBarModule,
        MatIconModule
      ],
      providers: [
        AuthenticationService,
        UrlService,
        { provide: MatBottomSheetRef },
        { provide: MAT_BOTTOM_SHEET_DATA, use: {data: {icon: '', link: '', text: ''}}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
