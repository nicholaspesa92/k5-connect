import { Directive, ElementRef, Input, HostListener, Renderer, ViewChild } from '@angular/core';

@Directive({
  selector: '[appTabInput]'
})
export class TabInputDirective {

  private el: ElementRef;

  constructor(private _el: ElementRef, public renderer: Renderer) {
      this.el = this._el;
  }
  @HostListener('keyup', ['$event']) onKeyUp(e: any) {
    if (this.el.nativeElement.name !== '6' && this.el.nativeElement.value !== '') {
        e.path[4].nextSibling.children[0].children[0].children[0].children[0].focus();
    }
    if (e.key === 'Backspace' && this.el.nativeElement.name !== '1') {
        e.path[4].previousSibling.children[0].children[0].children[0].children[0].focus();
    }
  }

}
