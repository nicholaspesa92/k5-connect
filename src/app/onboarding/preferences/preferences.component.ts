import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

import { LocationService } from './../../services/location.service';
import { BusinessLocation } from './../../models/businesslocation';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

  // public locations: BusinessLocation[];

  public locations = [
    {
      id: 1,
      name: 'High Grade Organics - Bend',
      address: '224 SE Davis, Bend, OR',
      distance: '2.1 mi',
      photo_url: 'https://cdn.allbud.com/image/upload/s--rrF3LImT--/c_limit,h_600,w_800/v1419022952/' +
      'images/dispensary/high-grade-organics/22/high-grade-inside-store.jpg'
    },
    {
      id: 2,
      name: 'High Grade Organics - Portland',
      address: '224 SE Davis, Bend, OR',
      distance: '212 mi',
      photo_url: 'https://cdn.shopify.com/s/files/1/1249/2029/t/7/assets/slideshow_3.jpg?16759369221180309307'
    },
    {
      id: 3,
      name: 'High Grade Organics - Seattle',
      address: '224 SE Davis, Bend, OR',
      distance: '396 mi',
      photo_url: 'https://cdn.shopify.com/s/files/1/1249/2029/t/7/assets/slideshow_1.jpg?16759369221180309307'
    }
  ];

  blocations: Observable<BusinessLocation[]>;
  locationsArr: any|BusinessLocation[] = [];

  chosenLocations: any|BusinessLocation[] = [];

  interestedProducts: any[] = [];

  bSettings: Observable<{}>;

  redirectUrl: string;

  constructor(
    private location: Location,
    private locService: LocationService,
    private sb: MatSnackBar,
    private router: Router,
    private as: AuthenticationService,
    private aRoute: ActivatedRoute
  ) {
    this.redirectUrl = '/consumer/home';
  }

  ngOnInit() {
    this.setDefaultInterests();
    this.blocations = this.locService.locationsAsObservableWithId();
    this.blocations.subscribe(locations => {
      locations.map(location => {
        this.locationsArr.push(location.id);
      });
    });
    this.bSettings = this.as.getCurrentBusinessSettings();
  }

  getRedirectUrl() {
    this.redirectUrl = this.aRoute.snapshot.queryParams['redirectUrl'] ? this.aRoute.snapshot.queryParams['redirectUrl'] : '/consumer/home';
  }

  handleCheck(id, e) {
    if (e.checked) {
      this.chosenLocations.push(id);
    } else if (!e.checked) {
      const i = this.chosenLocations.indexOf(id);
      this.chosenLocations.splice(i, 1);
    }
  }

  back() {
    this.location.back();
  }

  saveFollowedLocations() {
    const l = { followedLocations: this.locationsArr, interestedProducts: [] }
    this.locService.saveFollowedLocations(l)
      .then((success) => {
        this.snack('Locations Saved', 'OK', 2000);
        this.router.navigate([this.redirectUrl]);
      })
      .catch((error) => {
        this.snack('Error saving locations, try again', 'OK', 2000);
      })
  }

  snack(message, action, duration) {
    this.sb.open(message, action, {
      duration: duration
    });
  }

  setDefaultInterests() {
    this.interestedProducts = [
      { type: 'Flower', interested: false },
      { type: 'Edible', interested: false },
      { type: 'Concentrate', interested: false }
    ];
  }

  handleCheckX(i, event) {
    const l = {
      id: this.locationsArr[i].id,
      name: this.locationsArr[i].name
    }
    this.locationsArr[i].follows = event.checked;
    if (event.checked) {
      this.chosenLocations.push(l);
    } else if (!event.checked) {
      const index = this.locationsArr.indexOf(l);
      this.chosenLocations.splice(index, 1);
    }
  }

}
