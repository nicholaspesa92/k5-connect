import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatTabsModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSnackBar,
  MatDialogModule,
  MatInputModule,
  MatProgressBarModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule} from 'angular2-text-mask';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { AuthenticationService } from './../../services/authentication.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';
import { WindowService } from './../../services/window.service';
import { LocationService } from './../../services/location.service';

import { SetupComponent } from './setup.component';

describe('SetupComponent', () => {
  let component: SetupComponent;
  let fixture: ComponentFixture<SetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupComponent ],
      imports: [
        BrowserAnimationsModule,
        MatTabsModule,
        MatSelectModule,
        MatSnackBarModule,
        MatDialogModule,
        MatInputModule,
        MatProgressBarModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        TextMaskModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule
      ],
      providers: [
        UrlSanitizerService,
        AuthenticationService,
        WindowService,
        LocationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
