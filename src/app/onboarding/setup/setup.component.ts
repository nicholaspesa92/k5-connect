import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Inject, Renderer } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSidenav, MatSnackBar } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router, NavigationStart } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'rxjs-compat'
import * as firebase from 'firebase';

import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { AuthenticationService } from './../../services/authentication.service';
import { BusinessLocation } from './../../models/businesslocation';
import { WindowService } from './../../services/window.service';
import { LocationService } from './../../services/location.service';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;
const PHONE_MASK = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit, AfterViewInit, OnDestroy {

  placeholder = './../../assets/images/placeholder.png';
  showBack = false;
  showLogo = true;
  redirectUrl: string;
  code: string;
  mask: any;
  char: string;
  progress: boolean;

  steps: string[];
  step: number;
  shouldDeactivate: boolean;

  bSettings: Observable<any>;
  categories$: Observable<any>;
  categories: any;
  interestedProducts: any;

  // FormGroups //
  phoneForm: FormGroup;
  confirmForm: FormGroup;
  infoForm: FormGroup;
  locForm: FormGroup;

  vHash: string;
  vCode: string;

  month: number;
  day: number;
  year: number;
  months: any[];
  days: any[];
  years: any[];

  windowRef: any;

  fsUser: any;

  locations$: Observable<BusinessLocation[]>;
  locations: BusinessLocation[];
  locationsAmount: number;
  autoFollowedLocation: any;

  @ViewChild('focusable') focusable: ElementRef;
  @ViewChild('vRow') vRow: ElementRef;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private urlS: UrlSanitizerService,
    private as: AuthenticationService,
    private ls: LocationService,
    private fb: FormBuilder,
    private ws: WindowService,
    private sb: MatSnackBar,
    private dialog: MatDialog,
    private r2: Renderer
  ) {
    this.months = [];
    this.days = [];
    this.years = [];
    this.code = '+1';
    this.mask = PHONE_MASK;
    this.char = '\u2000';
    this.progress = false;
    this.shouldDeactivate = false;
    this.locations = [];
    this.locationsAmount = 0;
    this.autoFollowedLocation = [];
    this.interestedProducts = {};
  }

  ngOnInit() {
    this.buildTabs();
    this.buildPhoneForm();
    this.buildConfirmForm();
    this.buildInfoForm();
    this.buildLocForm();
    this.buildDateRanges();
    this.getRedirectUrl();
    this.getLocations();
    this.bSettings = this.as.getCurrentBusinessSettings();
  }

  ngAfterViewInit() {
    this.buildRecpatcha();
  }

  ngOnDestroy() {

  }

  buildTabs() {
    this.step = 0;
    this.steps = [ 'number', 'confirm', 'info', 'preferences' ];
  }

  buildPhoneForm() {
    this.phoneForm = this.fb.group({
      code: ['+1'],
      phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_REG_EX)]]
    });
  }

  buildConfirmForm() {
    this.confirmForm = this.fb.group({
      code: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  buildInfoForm() {
    this.infoForm = this.fb.group({
      displayName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthday: [this.subtractDate(), Validators.required]
    });
  }

  buildLocForm() {
    this.locForm = this.fb.group({});
  }

  subtractDate() {
    const d = new Date();
    d.setFullYear(d.getFullYear() - 21);
    return d;
  }

  buildRecpatcha() {
    this.windowRef = this.ws.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible'
    });
    this.windowRef.recaptchaVerifier.render();
  }

  buildDateRanges() {
    const d = new Date();
    let y = d.getFullYear();
    for (let i = 0; i < 60; i ++) {
      this.years.push(y);
      y--;
    }

    for (let i = 0; i < 31; i++) {
      this.days.push(i + 1);
    }

    this.months = [
      { key: 'January', value: 0 },
      { key: 'February', value: 1 },
      { key: 'March', vale: 2 },
      { key: 'April', value: 3 },
      { key: 'May', value: 4 },
      { key: 'June', value: 5 },
      { key: 'July', value: 6 },
      { key: 'August', value: 7 },
      { key: 'September', value: 8 },
      { key: 'October', value: 9 },
      { key: 'November', value: 10 },
      { key: 'December', value: 11 }
    ];
  }

  getRedirectUrl() {
    this.redirectUrl = this.route.snapshot.queryParams['redirectUrl'] ? this.route.snapshot.queryParams['redirectUrl'] : '/consumer/menu';
  }

  getLocations() {
    this.locations$ = this.ls.getLocations().first();
    this.locations$.subscribe((locations) => {
      this.locationsAmount = locations.length;
      this.locations = locations;
      if (locations.length === 1) {
        locations.map(location => {
          this.autoFollowedLocation.push(location.id);
          const lg = <FormGroup>this.locForm;
          lg.addControl(location.id, new FormControl(true));
        });
      } else {
        locations.map((location) => {
          const lg = <FormGroup>this.locForm;
          lg.addControl(location.id, new FormControl(false));
        })
      }
    });
  }

  // Sending code to phone [Step 1] //
  sendVCode() {
    this.start();
    const number = '+1' + this.trimNumber(this.phoneForm.value.phoneNumber);
    const appVerifier = this.windowRef.recaptchaVerifier;
    this.as.phoneLogin(number, appVerifier)
      .then((vHash) => {
        this.stop();
        this.vHash = vHash;
        this.step = 1;
      }).catch((error) => {
        this.stop();
        console.log(error);
      });
  }
  // End Step 1 //

  // Verifying Code [Step 2] //
  verifyCode() {
    this.start();
    const vCode = this.confirmForm.value.code;
    console.log(vCode);
    this.as.phoneVerify(this.vHash, vCode)
      .then((success) => {
        this.subscribeToUser();
      }).catch((error) => {
        this.stop();
        if (error.code === 'auth/invalid-verification-code') {
          this.snack('Invalid Code, Please Try Again', 'OK', 3000);
        } else {
          this.snack('Something Went Wrong, Please Try Again', 'OK', 3000);
        }
      });
  }
  subscribeToUser() {
    const x = this.as.user.first();
    x.subscribe((user) => {
      if (user) {
        this.fsUser = this.as.getCurrentUserFromFirebase(user.uid).first();
        this.fsUser.subscribe((fUser) => {
          this.handleAccountSetup(fUser);
        });
      } else {
        this.stop();
      }
    });
  }
  handleAccountSetup(user) {
    const x = this.as.isMemberY().first();
    x.subscribe((member) => {
      switch (member.exists) {
        case true:
          if (member.appInstalled) {
            this.stop();
            this.shouldDeactivate = true;
            this.router.navigate([this.redirectUrl]);
          } else if (!member.appInstalled) {
            this.stop();
            this.step = 2;
          }
          break;
        case false:
          this.stop();
          this.step = 2;
          break;
      }
    });
  }
  concatCode() {
    const x = this.confirmForm.value;
    let code = '';
    for (let i = 0; i < 6; i++) {
      code += x[i + 1];
    }
    return code;
  }
  // End Step 2 //

  // Finish Account [Step 3] //
  /*
  saveUserDetails() {
    const x = this.infoForm.value;
    const data = {
      emailVerified: true,
      displayName: x.displayName,
      email: x.email,
      birthday: this.getBday(x.year, x.month, x.day),
      facebookLinked: false,
      googleLinked: false
    };
    this.as.updateUserDisplayName(data.displayName)
      .then((res) => {
        this.as.updateUserEmail(data.email)
          .then((resx) => {
            this.as.saveUser(data)
              .then((resy) => {
                this.as.saveMember()
                  .then((resz) => {
                    this.step = 3;
                  }).catch((error) => {
                  });
              }).catch((error) => {
              });
          }).catch((error) => {
          });
      }).catch((error) => {
      });
  }
  */
  getBday(year, month, day) {
    return new Date(year, month, day);
  }
  async updateDetails() {
    this.start();
    const x = this.infoForm.value;
    const data = {
      emailVerified: true,
      displayName: x.displayName,
      email: x.email,
      birthday: x.birthday, // this.getBday(x.year, x.month, x.day),
      facebookLinked: false,
      googleLinked: false
    };
    try {
      const dnameres = await this.as.updateUserDisplayName(data.displayName).catch((error) => {console.log(error)});
      const emailres = await this.as.updateUserEmail(data.email).catch((error) => {console.log(error)});
      const usersave = await this.as.saveUser(data).catch((error) => {console.log(error)});
      const membersave = await this.as.saveMember().catch((error) => {console.log(error)});
      this.shouldDeactivate = true;
      this.handleMemberSaved();
    } catch (error) {
      this.stop();
    }
  }
  handleMemberSaved() {
    if (this.locationsAmount === 1) {
      this.autoFollowLocation();
    } else {
      this.stop();
      this.step = 3;
    }
  }
  async autoFollowLocation() {
    this.categories$ = this.as.getCurrentBusinessCategories().first();
    this.categories$.subscribe(async (categories) => {
      this.categories = categories;
      this.categories.map((category) => {
        this.interestedProducts[category.slug] = false;
      });
      const data = {
        followedLocations: this.locForm.value,
        interestedProducts: this.interestedProducts
      };
      /*
      this.ls.saveFollowedLocations(data)
        .then((success) => {
          this.stop();
          this.router.navigate([this.redirectUrl]);
        }).catch((error) => {
          this.stop();
        });
      */
      try {
        await this.ls.saveFollowedLocations(data);
        console.log('saved');
        this.stop();
        this.router.navigate([this.redirectUrl]);
      } catch (err) {
        this.stop();
      }
    });
  }
  // End Step 3 //

  // Preferences [Step 4] //
  handleCheck(id, e) {
    switch (e.checked) {
      case true:
        this.locations.push(id);
        break;
      case false:
        this.locations.splice(this.locations.indexOf(id), 1);
        break;
    }
  }
  saveUserPrefs() {
    this.start();
    this.categories$ = this.as.getCurrentBusinessCategories().first();
    this.categories$.subscribe(async (categories) => {
      this.categories = categories;
      this.categories.map((category) => {
        this.interestedProducts[category.slug] = false;
      });
      const data = {
        followedLocations: this.locForm.value,
        interestedProducts: this.interestedProducts
      };
      /*
      this.ls.saveFollowedLocations(data)
        .then((success) => {
          this.stop();
          this.router.navigate([this.redirectUrl]);
        }).catch((error) => {
          this.stop();
        });
      */
      try {
        await this.ls.saveFollowedLocations(data);
        console.log('saved');
        this.stop();
        this.router.navigate([this.redirectUrl]);
      } catch (err) {
        this.stop();
      }
    });
  }
  // End Step 4 //

  cancel() {
    this.router.navigate([this.redirectUrl]);
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

  next() {
    this.step++;
  }

  previous() {
    this.step--;
  }

  canLeave(): Observable<boolean> {
    const dRef = this.dialog.open(ExitPageDialogComponent);
    /*return dRef.afterClosed().map((result: boolean) => {
      console.log(result);
      return result;
    }).first();*/
    return dRef.afterClosed().first();
  }

  deactivate() {
    return this.shouldDeactivate;
  }

  trimNumber(number) {
    return number.replace(/[^0-9]/g, '');
  }

  logger() {
    const x = this.phoneForm.value.phoneNumber;
  }

  start() {
    this.progress = true;
  }
  stop() {
    this.progress = false;
  }

  tabChangeEvent(e) {
    if (e.index === 0) {
      this.buildRecpatcha();
    } else if (e.index === 1) {
      console.log('Focus: ', this.vRow);
      console.log(this.vRow.nativeElement.children[0].children[0].children[0].children[0].children[0]);

      // NOT THE BEST, BUT WORKS //
      setTimeout(() => {
        this.vRow.nativeElement.children[0].children[0].children[0].children[0].children[0].focus();
      }, 500);
    }
  }
}

@Component({
  selector: 'app-exit-dialog',
  template: `
    <h2 matDialogTitle>Exiting Setup</h2>
    <mat-dialog-content><p>Are you sure you want to exit</p></mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button color="accent" [matDialogClose]="false">CANCEL</button>
      <button mat-raised-button color="accent" [matDialogClose]="true">OK</button>
    <mat-dialog-actions>
  `
})
export class ExitPageDialogComponent {

  constructor(
    public dRef: MatDialogRef<ExitPageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

}
