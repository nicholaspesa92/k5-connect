import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

import { AuthenticationService } from './../../services/authentication.service';
import { User } from './../../models/user';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  placeholder = './../../assets/images/placeholder.png';

  fieldsForm: FormGroup;

  displayName: string;
  email: string;
  birthday: any;

  password: string;
  phone: string;
  googleLinked: boolean;
  facebookLinked: boolean;
  userObs: Observable<User>;

  showPw: boolean;

  bSettings: Observable<{}>;

  public mask = [/0-9/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

  constructor(
    private location: Location,
    private router: Router,
    private as: AuthenticationService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar
  ) {
    this.displayName = '';
    this.email = '';
    this.phone = '';
    this.googleLinked = false;
    this.facebookLinked = false;

    this.showPw = false;
  }

  ngOnInit() {
    // if (!this.as.phoneVerified) { this.router.navigate(['onboarding/step-1']); }
    if (this.as.currentUser) {
      this.displayName = this.as.currentUserDisplayName;
      this.email = this.as.currentUser.email;
      this.phone = this.as.currentUser.phoneNumber;
      this.userObs = this.as.userObs;

      // this.googleLinked = this.as.currentUser.getProviderData();
      this.as.currentUser.providerData.map((provider) => {
        if (provider.providerId === 'google.com') {
          this.googleLinked = true;
        } else if (provider.providerId === 'facebook.com') {
          this.facebookLinked = true;
        }
      });
    }
    this.createForm();
    this.bSettings = this.as.getCurrentBusinessSettings();
  }

  back() {
    this.location.back();
  }

  createForm() {
    this.fieldsForm = this.fb.group({
      name: [this.displayName, Validators.required],
      email: [this.email, [Validators.required, Validators.email]],
      birthday: ['', Validators.required]
    });
  }

  onSubmit() {
    this.saveUserDetails();
  }

  openSnack(message) {
    this.snackBar.open(message, null, { duration: 2000 });
  }

  saveUserDetails() {
    const form = this.fieldsForm.value;
    const userData = {
      emailVerified: true,
      displayName: form.name,
      email: form.email,
      birthday: form.birthday,
      facebookLinked: false,
      googleLinked: false
    };
    this.as.updateUserDisplayName(form.name)
      .then((res) => {
        this.as.updateUserEmail(form.email)
          .then((resx) => {
            this.as.saveUser(userData)
              .then((resy) => {
                this.as.saveMember()
                  .then((resz) => {
                    const navExtras: NavigationExtras = {
                      preserveQueryParams: true
                    };
                    this.router.navigate(['onboarding/step-4'], navExtras);
                  })
                  .catch((error) => {
                    this.openSnack('Member Issue');
                  });
              })
              .catch((error) => {
                this.openSnack('User Issue');
              });
          })
          .catch((error) => {
            this.openSnack('Email Issue');
          });
      })
      .catch((error) => {
        this.openSnack('Display Name Issue');
      });
  }

  linkEmail() {
    const form = this.fieldsForm.value;
    this.email = form.email;
    this.as.emailLink(this.email, this.password)
      .then((success) => {

      })
      .catch((error) => {

      });
  }

  googleLogin() {
    this.as.googleLogin()
      .then((success) => {
        this.googleLinked = true;
      })
      .catch((error) => {
      });
  }

  facebookLogin() {
  }

  changeVisibility() {
    this.showPw ? this.showPw = false : this.showPw = true;
  }

  saveUserData() {
    const formData = this.fieldsForm.value;
    this.displayName = formData.name;
    this.email = formData.email;
    const userData = {
      displayName: this.displayName,
      email: this.email,
      phoneNumber: this.phone,
      googleLinked: this.googleLinked,
      facebookLinked: this.facebookLinked,
      emailVerified: true
    };
    this.as.saveUser(userData)
      .then((res) => {
        this.as.saveMember()
          .then((resx) => {
            this.router.navigate(['onboarding/step-4']);
          })
          .catch((error) => {
          })
      })
      .catch((error) => {
      });
  }

  setUpAccount() {
    const form = this.fieldsForm.value;
    this.displayName = form.name;
    this.email = form.email;
    this.birthday = form.birthday

    this.as.emailLink(this.email, this.password)
      .then((success) => {
        const userData = {
          displayName: this.displayName,
          email: this.email,
          phoneNumber: this.phone,
          googleLinked: this.googleLinked,
          facebookLinked: this.facebookLinked,
          emailVerified: true
        };
        this.as.saveUser(userData)
          .then((res) => {
            this.as.saveMember()
              .then((resx) => {
                this.router.navigate(['onboarding/step-4']);
              })
              .catch((error) => {
                this.openSnack('Problem creating member, Please try again');
              });
          })
          .catch((error) => {
            this.openSnack('Problem creating user, Please try again');
          });
      })
      .catch((error) => {
        this.openSnack('Problem with password, try again');
      });
  }
}
