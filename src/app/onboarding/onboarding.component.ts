import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location, PlatformLocation } from '@angular/common';
import { Observable } from 'rxjs';
import 'rxjs-compat'
import { NavigationStart } from '@angular/router';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss'],
})
export class OnboardingComponent implements OnInit {
  showBack = false;
  showLogo = true;

  constructor(
    private router: Router,
    private location: Location,
    private pLoc: PlatformLocation,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.handleRouterEvents();
    this.pLoc.onPopState(() => {
      console.log('back pressed');
    });
  }

  handleRouterEvents() {
    this.router.events
    // Only continue with navigation end events
      .filter(event => event instanceof NavigationEnd)
      // Swap the observable for the current route
      .map(() => this.route)
      // Loop the routes to find the last activated child
      .map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        route = route.firstChild
        return route;
      })
      // Only continue for the primary router outlet
      .filter(route => route.outlet === 'primary')
      // Limit to just the data object
      .mergeMap(route => route.data)
      .subscribe((event: any) => {
        // Simple boolean
        this.showBack = event.showBack;
        this.showLogo = event.showLogo;
      });
  }

}
