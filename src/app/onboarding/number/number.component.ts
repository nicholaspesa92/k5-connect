import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';

import { WindowService } from './../../services/window.service';
import { AuthenticationService } from './../../services/authentication.service';

const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss']
})
export class NumberComponent implements OnInit, AfterViewInit, AfterViewChecked {
  placeholder = './../../assets/images/placeholder.png';

  loginForm = [];

  myCode = '+1';
  areaCodes = [
    {value: '+1', display: '+1'}
  ];

  windowRef: any;
  phoneNumber: string;

  phoneForm: FormGroup;

  bSettings: Observable<{}>

  constructor(
    private router: Router,
    private win: WindowService,
    private afAuth: AngularFireAuth,
    private authService: AuthenticationService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
    this.bSettings = this.authService.getCurrentBusinessSettings();
  }

  createForm() {
    this.phoneForm = this.fb.group({
      myCode: ['+1'],
      phone: [this.phoneNumber, [Validators.required, Validators.pattern(PHONE_REG_EX)]]
    });
  }

  ngAfterViewInit() {
    this.windowRef = this.win.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible'
    });

    this.windowRef.recaptchaVerifier.render();
  }

  ngAfterViewChecked() {

  }

  onSubmit() {
    this.sendLoginCode();
  }

  sendLoginCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const x = this.phoneForm;
    const num = x.get('myCode').value + x.get('phone').value;

    this.authService.phoneLogin(num, appVerifier)
      .then((verificationId) => {
        const navExtras: NavigationExtras = {
          preserveQueryParams: true
        };
        this.router.navigate(['onboarding/step-2'], navExtras);
      })
      .catch((error) => {
      });
  }

}
