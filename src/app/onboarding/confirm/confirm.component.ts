import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';

import { WindowService } from './../../services/window.service';
import { AuthenticationService } from './../../services/authentication.service';
import { UrlService } from './../../services/url.service';

const AUTH_INVALID_CODE = 'auth/invalid-verification-code';
const AUTH_EXPIRED_CODE = 'auth/code-expired';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  placeholder = './../../assets/images/placeholder.png';

  one: any = '';
  two: any = '';
  three: any = '';
  four: any = '';
  five: any = '';
  six: any = '';

  verificationForm: FormGroup;
  verifying: boolean;

  windowRef: any;

  bSettings: Observable<{}>;

  redirectUrl: string;

  constructor(
    private location: Location,
    private router: Router,
    private aRoute: ActivatedRoute,
    private win: WindowService,
    private as: AuthenticationService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private us: UrlService
  ) {
    this.redirectUrl = '/consumer/menu';
  }

  ngOnInit() {
    this.windowRef = this.win.windowRef;
    this.createForm();
    this.verifying = false;
    this.bSettings = this.as.getCurrentBusinessSettings();
    this.getRedirectUrl();
  }

  back() {
    this.location.back();
  }
  createForm() {
    this.verificationForm = this.fb.group({
      one: ['', Validators.required],
      two: ['', Validators.required],
      three: ['', Validators.required],
      four: ['', Validators.required],
      five: ['', Validators.required],
      six: ['', Validators.required]
    });
  }

  getRedirectUrl() {
    this.redirectUrl = this.aRoute.snapshot.queryParams['redirectUrl'] ? this.aRoute.snapshot.queryParams['redirectUrl'] : '/consumer/menu';
  }

  onSubmit() {
    this.verifyCode();
  }

  verifyCode() {
    this.start();
    /*this.as.phoneVerify(this.concatDigits())
      .then((res) => {
        this.as.phoneVerified = true;
        this.handleUserExisting(res);
      })
      .catch((error) => {
        this.stop();
        this.openSnackBar();
      });*/
  }

  handleUserExisting(res) {
    this.as.getCurrentUserFromFirebase(res.uid)
      .subscribe((user) => {
        if (user) {
          if (!user.emailVerified) {
            this.stop();
            const navExtras: NavigationExtras = {
              preserveQueryParams: true
            };
            this.router.navigate(['onboarding/step-3'], navExtras);
          } else  {
            this.stop();
            this.route(this.redirectUrl);
          }
        }
      });
  }

  handleUserNotExisting(res) {
    const userData = {
      phoneNumber: res.phoneNumber,
      emailVerified: false
    }
    this.as.saveUser(userData)
      .then(() => {
        this.stop();
        this.route('onboarding/step-3');
      })
      .catch((error) => {
        this.stop();
        this.openSnackBar();
      });
  }

  concatDigits(): string {
    const x = this.verificationForm;
    return x.get('one').value + x.get('two').value + x.get('three').value + x.get('four').value + x.get('five').value + x.get('six').value;
  }

  openSnackBar() {
    this.snackBar.openFromComponent(VerificationSnackComponent, {
      duration: 1000
    });
  }

  start() { this.verifying = true; }
  stop() { this.verifying = false; }
  route(to) { this.router.navigate([to]); }


}

@Component({
  selector: 'app-verification-snack',
  template: `<p>Verification Code Error, Please Try Again</p>`
})
export class VerificationSnackComponent {}

/*verifyCode() {
  this.as.phoneVerify(this.concatDigits())
    .then((res) => {
      console.log(res.code);
      if (res.code === AUTH_INVALID_CODE || res.code === AUTH_EXPIRED_CODE) {
        this.openSnackBar();
      } else {
        console.log('SUCCESS');
        this.router.navigate(['onboarding/step-3']);
      }
    })
    .catch(error => {
      this.verificationError = true;
      this.openSnackBar();
      console.log('ERROR');
    });
  }*/


  /*verifyCode() {
    this.start();
    this.as.phoneVerify(this.concatDigits())
      .then((res) => {
        this.as.getCurrentUserFromFirebase(res.uid).subscribe((user) => {
          if (user.emailVerified) {
            this.stop();
            this.router.navigate(['consumer/home']);
          } else if (!user.emailVerified || !user.facebookLinked || !user.googleLinked) {
            this.stop();
            this.router.navigate(['onboarding/step-3']);
          }
        });
      })
      .catch((error) => {
        this.stop();
        this.openSnackBar();
      });
  }*/
