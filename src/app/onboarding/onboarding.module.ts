import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {
  MatButtonModule, MatIconModule, MatInputModule, MatCheckboxModule, MatSidenavModule,
  MatSelectModule, MatListModule, MatToolbarModule, MatCardModule, MatSnackBarModule, MatProgressBarModule,
  MatDatepickerModule, MatNativeDateModule, MatTabsModule, MatDialogModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import * as firebase from 'firebase';
import { TextMaskModule } from 'angular2-text-mask';

import { OnboardingComponent } from './onboarding.component';
import { NumberComponent } from './number/number.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { InfoComponent } from './info/info.component';
import { NextInputDirective } from '../next-input.directive';
import { PreferencesComponent } from './preferences/preferences.component';
import { VerificationSnackComponent } from './confirm/confirm.component';
import { ExitPageDialogComponent } from './setup/setup.component';

import { WindowService } from './../services/window.service';
import { AuthenticationService } from './../services/authentication.service';
import { LocationService } from './../services/location.service';
import { UrlSanitizerService } from './../services/url-sanitizer.service';

import { AuthGuard } from './../guards/auth.guard';
import { OnboardingGuard } from './../guards/onboarding.guard';
import { SetupComponent } from './setup/setup.component';

const routes: Routes = [
  {
    path: '',
    component: OnboardingComponent,
    children: [
      {path: '', redirectTo: 'setup', pathMatch: 'full'},
      {path: 'step-1', component: NumberComponent, data: {showBack: false, showLogo: true}},
      {path: 'step-2', component: ConfirmComponent, data: {showBack: true, showLogo: true}},
      {path: 'step-3', component: InfoComponent, data: {showBack: true, showLogo: true}},
      {path: 'step-4', component: PreferencesComponent, data: {showBack: true, showLogo: false}},
      {path: 'setup', component: SetupComponent, data: {showBack: true, showLogo: true}, canDeactivate: [OnboardingGuard]}
    ]
  }
];

@NgModule({
  imports: [
    SharedModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatCardModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatDialogModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  exports: [RouterModule],
  declarations: [
    OnboardingComponent,
    NumberComponent,
    ConfirmComponent,
    InfoComponent,
    NextInputDirective,
    PreferencesComponent,
    VerificationSnackComponent,
    SetupComponent,
    ExitPageDialogComponent
  ],
  entryComponents: [VerificationSnackComponent, ExitPageDialogComponent],
  providers: [
    Location,
    WindowService,
    AuthenticationService,
    LocationService,
    AuthGuard,
    OnboardingGuard,
    UrlSanitizerService
  ]
})
export class OnboardingModule { }
