import { Observable } from 'rxjs';
import { BusinessLocation } from './businesslocation';
export interface LoyaltyMessage {
  id?: string;
  title: string;
  type: string;
  lapseDays?: number;
  location: any;
  _refLocation?: any;
  group: string;
  sendTime: Date;
  message: string;
  createdAt: Date;
  active: boolean;
  gift?: string;
}
