import { User } from './user';
export interface Review {
    name: string;
    rating: number;
    message: string;
    user: User;
}
