export class SupportRequest {
    _name: string;
    _phone: string;
    _email: string;
    _subject: string;
    _desc: string;
    _priority: string;
    _user?: any;

    constructor(name, phone, email, subject, desc, priority) {
        this._name = name;
        this._phone = phone;
        this._email = email;
        this._subject = subject;
        this._desc = desc;
        this._priority = priority;
    }

    set user(user) { this.user = user; }
    get user() { return this.user; }
}
