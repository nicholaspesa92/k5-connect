import { Observable } from 'rxjs';
import { LoyaltyMessage } from './loyalty-message';
export interface Gift {
  id?: string;
  title: string;
  type: string;
  points?: number;
  loyaltyMessage?: any|Observable<LoyaltyMessage>;
  photoUrl?: string;
  createdAt: Date;
}
