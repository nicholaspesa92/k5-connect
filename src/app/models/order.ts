import { Observable } from 'rxjs';
import { User } from './user';
import { OrderItem } from './order-item';

export interface Order {
    id?: string;
    user: User;
    userId: string;
    createdAt: Date;
    products: OrderItem[];
    total?: number;
    status: string;
    canceled?: boolean;
    archived?: boolean;
}
