import { Observable } from 'rxjs';
import { Menu } from './menu';

export interface OrderItem {
    id: string;
    name: string;
    unit: string;
    price: number;
    quantity: number;
    variations: any;
    category: string;
    _refProduct: any;
}
