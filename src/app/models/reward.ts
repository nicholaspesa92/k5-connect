export interface Reward {
  id?: string;
  description: string;
  group: any;
  location: string;
  points: number;
  redeemLimit: number;
  title: string;
  type: string;
  createdAt: Date;
  active: boolean;
  photoUrl?: string;
}
