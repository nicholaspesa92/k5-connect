import { Review } from './review';
import { Observable } from 'rxjs';
export class BusinessLocation {
    id?: string;
    name: string;
    photoUrl?: string;
    email?: string;
    address: string;
    address2?: string;
    city: string;
    state: string;
    zip: string;
    country: string;
    phoneNumber: string;
    website?: string;
    facebook?: string;
    googlePlaces?: string;
    hasMenu: boolean;
    hasDelivery: boolean;
    hasOnlineOrdering: boolean;
    hasLoyalty: boolean;
    menu: any;
    reviews: any|Observable<Review[]>;
    location: any;
    hours: {
      sun: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      mon: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      tue: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      wed: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      thu: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      fri: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      },
      sat: {
        open: boolean;
        openTime?: Date;
        closeTime?: Date;
      }
    };

    constructor() {
      console.log('new-instance');
    }
  }
