export interface User {
    displayName?: string;
    phoneNumber?: string;
    email?: string;
    photoUrl?: string;
    emailVerified?: boolean;
    id?: string;
    birthday?: string;
    address?: any;
    googleLinked?: boolean;
    facebookLinked?: boolean;
}
