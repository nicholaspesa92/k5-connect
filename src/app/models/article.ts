import { Observable } from 'rxjs';
import { User } from './user';

export interface Article {
    title: string;
    author: any|Observable<User>;
    body: string;
    catrgory: string;
    photoUrl: string;
    date: Date
}
