import { Observable } from 'rxjs';
import { User } from './user';
import { BusinessLocation } from './businesslocation';

export interface Member extends User {
  currentPoints?: number;
  lastCheckin?: Date;
  lifetimePoints?: number;
  memberSince?: Date;
  user?: any|Observable<User>;
  id?: string;
  marketingMessages?: boolean;
  totalCheckins?: number;
  followedLocations?: any|BusinessLocation[];
  interestedProducts?: any[];
}
