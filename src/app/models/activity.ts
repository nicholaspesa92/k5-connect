export interface Activity {
    title: string;
    points: number;
    createdAt: Date;
    id?: string;
}
