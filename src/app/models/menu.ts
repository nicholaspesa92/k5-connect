import { Observable } from 'rxjs';
import { Price } from './price';

export interface Menu {
    id?: string;
    name: string;
    createdAt: Date;
    category: string;
    type?: string;
    thc?: number;
    cbd?: number;
    description?: string;
    photoUrl?: string;
    price: any|Observable<Price[]>
    onSale: boolean;
}

/*

{
    "name": "Headband by Decibel Farm",
    "description": "16.7&#37; THC 0.2&#37; CBD Does not include 20&#37; TAX",
    "variations": [
        {
            "name": "Gram",
            "price": 8.25
        },
        {
            "name": "Two Grams",
            "price": 16.5
        },
        {
            "name": "Eighth",
            "price": 28.87
        },
        {
            "name": "Quarter",
            "price": 57.75
        },
        {
            "name": "Half",
            "price": 115.5
        },
        {
            "name": "Ounce",
            "price": 231
        }
    ]
},

*/
