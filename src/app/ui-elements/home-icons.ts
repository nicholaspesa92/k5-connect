const height = 100;
const width = 100;

// tslint:disable:max-line-length
export const locationSvg =
`
<?xml version="1.0" encoding="UTF-8"?>
<svg class="locsvg" id="31b2376c-d9cb-459c-a08c-900021ef8e68" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="${width}" height="${height}" viewBox="0 0 887.59 776.14">
    <defs>
        <linearGradient id="2d643a01-90c6-4282-8c89-060a579808b4" x1="148.91" y1="776.14" x2="148.91" y2="47.08" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="gray" stop-opacity="0.25"/>
            <stop offset="0.54" stop-color="gray" stop-opacity="0.12"/>
            <stop offset="1" stop-color="gray" stop-opacity="0.1"/>
        </linearGradient>
        <linearGradient id="a3e0f5af-5f14-4b24-ae95-6e04dd729135" x1="446.72" y1="776.14" x2="446.72" y2="47.08" xlink:href="#2d643a01-90c6-4282-8c89-060a579808b4"/>
        <linearGradient id="aa8d7746-6edb-4ff6-8601-5cedae83b174" x1="741.61" y1="776.14" x2="741.61" y2="47.08" xlink:href="#2d643a01-90c6-4282-8c89-060a579808b4"/>
        <linearGradient id="08fa861d-a5a6-45c5-828b-48370c6dd4d7" x1="889.9" y1="282.66" x2="889.9" y2="61.93" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="#b3b3b3" stop-opacity="0.25"/>
            <stop offset="0.54" stop-color="#b3b3b3" stop-opacity="0.1"/>
            <stop offset="1" stop-color="#b3b3b3" stop-opacity="0.05"/>
        </linearGradient>
        <linearGradient id="c138b5e9-aced-4e12-8d58-cfc81734ef31" x1="733.7" y1="87.3" x2="733.7" y2="44.48" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-opacity="0.12"/>
            <stop offset="0.55" stop-opacity="0.09"/>
            <stop offset="1" stop-opacity="0.02"/>
        </linearGradient>
        <linearGradient id="77e2ede8-9f9a-47ed-a740-ad5fa329fb2d" x1="311.15" y1="727" x2="311.15" y2="504.71" xlink:href="#08fa861d-a5a6-45c5-828b-48370c6dd4d7"/>
        <linearGradient id="ae6f7cb0-09a9-4206-a53c-57ed9ab3e9f7" x1="154.94" y1="530.7" x2="154.94" y2="487.57" gradientUnits="userSpaceOnUse">
            <stop offset="0.01" stop-opacity="0.12"/>
            <stop offset="0.55" stop-opacity="0.09"/>
            <stop offset="1" stop-opacity="0.05"/>
        </linearGradient>
    </defs>
    <title>map light</title>
    <polygon points="297.81 748.17 0 776.14 0 75.06 297.81 47.08 297.81 748.17" fill="url(#2d643a01-90c6-4282-8c89-060a579808b4)"/>
    <polygon points="297.81 748.17 595.62 776.14 595.62 75.06 297.81 47.08 297.81 748.17" fill="url(#a3e0f5af-5f14-4b24-ae95-6e04dd729135)"/>
    <polygon points="887.59 748.17 595.62 776.14 595.62 75.06 887.59 47.08 887.59 748.17" fill="url(#aa8d7746-6edb-4ff6-8601-5cedae83b174)"/>
    <polygon points="298.87 728.7 9.49 755.59 9.49 81.95 298.87 55.07 298.87 728.7" fill="#f5f5f5"/>
    <polygon points="298.87 728.7 588.24 755.59 588.24 81.95 298.87 55.07 298.87 728.7" fill="#fff"/>
    <polygon points="877.62 728.7 588.24 755.59 588.24 81.95 877.62 55.07 877.62 728.7" fill="#f5f5f5"/>
    <polygon points="298.87 116.94 79.06 124.65 79.06 154.69 79.06 195.81 79.06 225.85 265.66 225.85 265.66 189.26 298.87 188.1 349.47 203.72 349.47 132.56 298.87 116.94" fill="#4285f4" class="svg-accent" opacity="0.3"/>
    <polygon points="298.87 682.85 52.18 706.57 52.18 616.43 298.87 592.71 298.87 682.85" fill="#4285f4" class="svg-accent" opacity="0.3"/>
    <rect x="444.34" y="613.27" width="107.53" height="82.23" fill="#4285f4" class="svg-accent" opacity="0.3"/>
    <polygon points="836.5 657.55 668.89 671.78 668.89 562.67 836.5 548.44 836.5 657.55" fill="#4285f4" class="svg-accent" opacity="0.3"/>
    <path d="M958.28,132.76c0,39.12-68.38,149.9-68.38,149.9s-68.38-110.78-68.38-149.9,30.62-70.83,68.38-70.83S958.28,93.64,958.28,132.76Z" transform="translate(-156.2 -61.93)" fill="url(#08fa861d-a5a6-45c5-828b-48370c6dd4d7)"/>
    <ellipse cx="733.7" cy="65.89" rx="20.67" ry="21.41" fill="url(#c138b5e9-aced-4e12-8d58-cfc81734ef31)"/>
    <path d="M953.18,132.89c0,34.95-63.28,133.91-63.28,133.91s-63.28-99-63.28-133.91a63.28,63.28,0,1,1,126.55,0Z" transform="translate(-156.2 -61.93)" fill="#4285f4" class="svg-accent"/>
    <circle cx="733.7" cy="66.54" r="19.13" fill="#fff"/>
    <path d="M381.56,576c0,39.4-70.41,151-70.41,151s-70.41-111.56-70.41-151,31.52-71.33,70.41-71.33S381.56,536.65,381.56,576Z" transform="translate(-156.2 -61.93)" fill="url(#77e2ede8-9f9a-47ed-a740-ad5fa329fb2d)"/>
    <ellipse cx="154.94" cy="509.14" rx="21.29" ry="21.57" fill="url(#ae6f7cb0-09a9-4206-a53c-57ed9ab3e9f7)"/>
    <path d="M374.42,575.65c0,34.95-63.28,133.91-63.28,133.91s-63.28-99-63.28-133.91a63.28,63.28,0,0,1,126.55,0Z" transform="translate(-156.2 -61.93)" fill="#4285f4" class="svg-accent"/>
    <circle cx="154.94" cy="509.31" r="19.13" fill="#fff"/>
    <g opacity="0.4">
        <line x1="175.52" y1="649.64" x2="180.17" y2="645.85" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11"/>
        <line x1="188.95" y1="638.69" x2="289.83" y2="556.41" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11" stroke-dasharray="11.32 11.32"/>
        <polyline points="294.21 552.83 298.87 549.04 304.73 550.29" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11"/>
        <line x1="316.89" y1="552.88" x2="432.4" y2="577.51" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11" stroke-dasharray="12.43 12.43"/>
        <polyline points="438.48 578.81 444.34 580.06 446.01 574.3" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11"/>
        <line x1="449.4" y1="562.61" x2="562.74" y2="171.05" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11" stroke-dasharray="12.17 12.17"/>
        <polyline points="564.43 165.2 566.1 159.44 571.58 161.89" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11"/>
        <line x1="582.17" y1="166.62" x2="703.97" y2="221.04" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11" stroke-dasharray="11.6 11.6"/>
        <line x1="709.27" y1="223.41" x2="714.75" y2="225.85" fill="none" stroke="#64ffda" stroke-miterlimit="10" stroke-width="11"/>
    </g>
</svg>
`;

export const productSvg =
`
<?xml version="1.0" encoding="UTF-8"?>
<svg width="${width}" height="${height}" viewBox="0 0 877 552" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
    <title>undraw_search_2dfv</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <linearGradient x1="50.0005735%" y1="100%" x2="50.0005735%" y2="0%" id="linearGradient-1">
            <stop stop-color="#808080" stop-opacity="0.25" offset="0%"></stop>
            <stop stop-color="#808080" stop-opacity="0.12" offset="54%"></stop>
            <stop stop-color="#808080" stop-opacity="0.1" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="50.0328535%" y1="99.9720746%" x2="50.0328535%" y2="-0.0194486061%" id="linearGradient-2">
            <stop stop-color="#B3B3B3" stop-opacity="0.25" offset="0%"></stop>
            <stop stop-color="#B3B3B3" stop-opacity="0.1" offset="54%"></stop>
            <stop stop-color="#B3B3B3" stop-opacity="0.05" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="linearGradient-3">
            <stop stop-color="#FFFFFF" stop-opacity="0.12" offset="0%"></stop>
            <stop stop-color="#FFFFFF" stop-opacity="0.09" offset="55%"></stop>
            <stop stop-color="#FFFFFF" stop-opacity="0.02" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="88742%" y1="23700%" x2="88742%" y2="21930%" id="linearGradient-4">
            <stop stop-color="#FFFFFF" stop-opacity="0.12" offset="0%"></stop>
            <stop stop-color="#FFFFFF" stop-opacity="0.09" offset="55%"></stop>
            <stop stop-color="#FFFFFF" stop-opacity="0.02" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="43532%" y1="55199%" x2="43532%" y2="21438%" id="linearGradient-5">
            <stop stop-color="#808080" stop-opacity="0.25" offset="0%"></stop>
            <stop stop-color="#808080" stop-opacity="0.12" offset="54%"></stop>
            <stop stop-color="#808080" stop-opacity="0.1" offset="100%"></stop>
        </linearGradient>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="undraw_search_2dfv">
            <rect id="Rectangle-path" fill="url(#linearGradient-1)" fill-rule="nonzero" x="5.09" y="0" width="871.91" height="161.23"></rect>
            <rect id="Rectangle-path" fill="#FFFFFF" fill-rule="nonzero" x="10.18" y="5.28" width="860.45" height="147.38"></rect>
            <rect id="Rectangle-path" fill="#F5F5F5" fill-rule="nonzero" x="63.03" y="35.36" width="500.19" height="87.71"></rect>
            <path d="M757.26,41 C740.258558,23.3067294 712.408934,21.941874 693.758894,37.8879356 C675.108854,53.8339971 672.1302,81.5574887 686.967138,101.101443 C701.804076,120.645396 729.308001,125.227811 749.68,111.55 L783.16,146.38 C783.574242,146.812455 784.143789,147.061934 784.742526,147.073196 C785.341264,147.084457 785.919789,146.856572 786.35,146.44 L794.69,138.44 C795.122455,138.025758 795.371934,137.456211 795.383196,136.857474 C795.394457,136.258736 795.166572,135.680211 794.75,135.25 L761.27,100.42 C774.530849,82.2648797 772.839921,57.2087808 757.26,41 Z M746.83,97 C733.63956,109.679047 712.668461,109.265403 699.988262,96.0760701 C687.308062,82.8867372 687.719875,61.9156022 700.908101,49.2342512 C714.096326,36.5529002 735.067497,36.962882 747.75,50.15 C760.402178,63.3342545 759.990558,84.2731754 746.83,96.95 L746.83,97 Z" id="Shape" fill="url(#linearGradient-2)" fill-rule="nonzero"></path>
            <path d="M699.33,63.86 C695.6,63.86 695.6,69.65 699.33,69.65 C703.06,69.65 703.06,63.86 699.33,63.86 Z" id="Shape" fill="url(#linearGradient-3)" fill-rule="nonzero"></path>
            <path d="M725.3,45.31 C715.695822,45.0231236 706.623009,49.7107434 701.3,57.71 C699.72,60.19 704.21,62.38 705.79,59.91 C709.79,53.62 717.44,49.63 725.69,49.84 C734.158319,50.1323103 741.921159,54.6346825 746.38,61.84 C747.96,64.45 752.11,62.08 750.53,59.49 C745.5,51.19 735.66,45.54 725.3,45.31 Z" id="Shape" fill="url(#linearGradient-4)" fill-rule="nonzero"></path>
            <path d="M757.26,35.87 C740.258558,18.1767294 712.408934,16.811874 693.758894,32.7579356 C675.108854,48.7039971 672.1302,76.4274887 686.967138,95.9714425 C701.804076,115.515396 729.308001,120.097811 749.68,106.42 L783.16,141.25 C783.574242,141.682455 784.143789,141.931934 784.742526,141.943196 C785.341264,141.954457 785.919789,141.726572 786.35,141.31 L794.69,133.31 C795.122455,132.895758 795.371934,132.326211 795.383196,131.727474 C795.394457,131.128736 795.166572,130.550211 794.75,130.12 L761.27,95.29 C774.530849,77.1348797 772.839921,52.0787808 757.26,35.87 Z M746.83,91.87 C733.63956,104.549047 712.668461,104.135403 699.988262,90.9460701 C687.308062,77.7567372 687.719875,56.7856022 700.908101,44.1042512 C714.096326,31.4229002 735.067497,31.832882 747.75,45.02 C760.428938,58.2091546 760.017065,79.1788882 746.83,91.86 L746.83,91.87 Z" id="Shape" fill="#E91E63" class="svg-accent" fill-rule="nonzero"></path>
            <path d="M699.33,58.76 C695.6,58.76 695.6,64.55 699.33,64.55 C703.06,64.55 703.06,58.76 699.33,58.76 Z" id="Shape" fill="#E91E63" class="svg-accent" fill-rule="nonzero"></path>
            <path d="M725.3,40.22 C715.695822,39.9331236 706.623009,44.6207434 701.3,52.62 C699.72,55.1 704.21,57.29 705.79,54.82 C709.79,48.53 717.44,44.54 725.69,44.75 C734.158319,45.0423103 741.921159,49.5446825 746.38,56.75 C747.96,59.36 752.11,56.99 750.53,54.4 C745.5,46.1 735.66,40.45 725.3,40.22 Z" id="Shape" fill="#E91E63" class="svg-accent" fill-rule="nonzero"></path>
            <rect id="Rectangle-path" fill="url(#linearGradient-5)" fill-rule="nonzero" x="0" y="214.38" width="870.64" height="337.62"></rect>
            <rect id="Rectangle-path" fill="#FFFFFF" fill-rule="nonzero" x="10.18" y="222.62" width="850.03" height="318.58"></rect>
            <circle id="Oval" fill="#E91E63" class="svg-accent" fill-rule="nonzero" cx="121.09" cy="381.17" r="71.46"></circle>
            <rect id="Rectangle-path" fill="#BDBDBD" fill-rule="nonzero" x="244.65" y="324.6" width="297.73" height="35.73"></rect>
            <rect id="Rectangle-path" fill="#E0E0E0" fill-rule="nonzero" x="244.65" y="397.54" width="297.73" height="35.73"></rect>
            <rect id="Rectangle-path" fill="#E91E63" class="svg-accent" fill-rule="nonzero" x="580.34" y="222.62" width="281.36" height="318.58"></rect>
            <text id="$-copy" font-family="Roboto-Black, Roboto" font-size="146" font-weight="700" fill="#D41D5B">
                <tspan x="683" y="435">$</tspan>
            </text>
            <text id="$" font-family="Roboto-Black, Roboto" font-size="146" font-weight="700" fill="#FFFFFF">
                <tspan x="679" y="431">$</tspan>
            </text>
        </g>
    </g>
</svg>
`;

export const rewardSvg =
`
<?xml version="1.0" encoding="UTF-8"?>
<svg width="${width}" height="${height}" viewBox="0 0 622 609" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
    <title>undraw_gift1_sgf8</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <linearGradient x1="50.0001583%" y1="100.00015%" x2="50.0001583%" y2="0.0041204196%" id="linearGradient-1">
            <stop stop-color="#808080" stop-opacity="0.25" offset="0%"></stop>
            <stop stop-color="#808080" stop-opacity="0.12" offset="54%"></stop>
            <stop stop-color="#808080" stop-opacity="0.1" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="42004%" y1="80461%" x2="42004%" y2="32021%" id="linearGradient-2">
            <stop stop-color="#808080" stop-opacity="0.25" offset="0%"></stop>
            <stop stop-color="#808080" stop-opacity="0.12" offset="54%"></stop>
            <stop stop-color="#808080" stop-opacity="0.1" offset="100%"></stop>
        </linearGradient>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="undraw_gift1_sgf8" fill-rule="nonzero">
            <g id="Group" opacity="0.5" transform="translate(178.000000, 0.000000)" fill="url(#linearGradient-1)">
                <path d="M257.54,18.07 C244.64,5.13 225.83,-0.02 204.54,3.57 C184.65,6.93 164.75,17.7 148.54,33.9 C143.837118,38.6317666 139.884844,44.0543405 136.82,49.98 C133.834564,43.8573813 129.914305,38.2367689 125.2,33.32 C109.2,16.85 89.53,5.74 69.7,2.03 C48.51,-1.97 29.62,2.89 16.5,15.61 C3.38,28.33 -2.05,47.06 1.25,68.32 C4.33,88.32 14.77,108.32 30.77,124.81 C64.35,159.45 131.36,145.92 134.2,145.32 L135.04,145.15 L137.62,145.74 C140.45,146.38 207.21,161.06 241.37,127.01 C257.62,110.82 268.45,90.95 271.88,71.07 C275.53,49.84 270.44,31.01 257.54,18.07 Z M122.85,125.32 C106.66,127.5 66.3,130.46 46.26,109.79 C33.38,96.5 24.95,80.6 22.55,65.04 C20.35,50.84 23.55,38.77 31.49,31.04 C34.6046984,28.0482618 38.3299617,25.7660263 42.41,24.35 C49.09,21.98 57.04,21.54 65.73,23.17 C81.21,26.06 96.83,34.98 109.73,48.27 C129.77,68.99 125.53,109.24 122.85,125.32 Z M250.63,67.39 C247.96,82.91 239.26,98.65 226.15,111.72 C205.75,132.05 165.45,128.39 149.31,125.93 C146.9,109.77 143.37,69.46 163.76,49.13 C176.87,36.06 192.64,27.42 208.17,24.79 C216.89,23.32 224.83,23.89 231.47,26.37 C235.524179,27.8573487 239.20849,30.2043916 242.27,33.25 C250.1,41.14 253.07,53.26 250.63,67.41 L250.63,67.39 Z" id="Shape"></path>
            </g>
            <path d="M318.68,140.97 L312.3,142.32 C309.59,142.89 245.47,155.83 213.3,122.69 C198.02,106.93 187.99,87.74 185.04,68.69 C181.89,48.31 187.04,30.38 199.62,18.21 C212.2,6.04 230.26,1.43 250.53,5.21 C269.53,8.76 288.37,19.39 303.64,35.15 C335.76,68.3 320.82,131.98 320.16,134.67 L318.68,140.97 Z M224.47,26.58 C220.565726,27.9344218 217.000803,30.1177238 214.02,32.98 C206.4,40.36 203.36,51.91 205.46,65.5 C207.76,80.39 215.82,95.6 228.15,108.32 C247.32,128.1 285.94,125.32 301.43,123.18 C304,107.77 308.04,69.25 288.87,49.47 C276.54,36.75 261.59,28.22 246.78,25.47 C238.48,23.89 230.87,24.32 224.47,26.58 Z" id="Shape" fill="#38D39F" class="svg-accent"></path>
            <path d="M309.22,141.25 L307.77,134.89 C307.16,132.18 293.32,68.25 326.01,35.67 C341.55,20.17 360.6,9.87 379.63,6.67 C399.96,3.23 417.96,8.16 430.31,20.54 C442.66,32.92 447.52,50.94 444.02,71.26 C440.74,90.26 430.37,109.26 414.83,124.79 C382.16,157.32 318.28,143.32 315.57,142.7 L309.22,141.25 Z M405.39,28.5 C399.03,26.12 391.39,25.57 383.1,26.98 C368.24,29.49 353.1,37.76 340.6,50.27 C321.09,69.72 324.46,108.27 326.77,123.76 C342.22,126.11 380.77,129.62 400.3,110.16 C412.85,97.65 421.17,82.59 423.73,67.74 C426.07,54.19 423.23,42.59 415.73,35.08 C412.795611,32.1650336 409.264661,29.9202365 405.38,28.5 L405.39,28.5 Z" id="Shape" fill="#38D39F" class="svg-accent"></path>
            <g id="Group" opacity="0.5" transform="translate(0.000000, 124.000000)" fill="url(#linearGradient-2)">
                <polygon id="Shape" points="621.87 0.21 0.21 0.21 0.21 124.6 32.51 124.6 32.51 484.61 589.57 484.61 589.57 124.6 621.87 124.6"></polygon>
            </g>
            <rect id="Rectangle-path" fill="#F5F5F5" x="39.56" y="161.82" width="542.96" height="440.67"></rect>
            <rect id="Rectangle-path" fill="#38D39F" class="svg-accent" x="259.89" y="240.51" width="102.3" height="361.98"></rect>
            <rect id="Rectangle-path" fill="#000000" opacity="0.1" x="39.56" y="138.21" width="542.96" height="110.17"></rect>
            <rect id="Rectangle-path" fill="#F5F5F5" x="8.08" y="130.34" width="605.91" height="110.17"></rect>
            <rect id="Rectangle-path" fill="#38D39F" class="svg-accent" x="259.89" y="130.34" width="102.3" height="110.17"></rect>
        </g>
    </g>
</svg>
`;

export const activitySvg =
`
<?xml version="1.0" encoding="UTF-8"?>
<svg id="29e4b1a0-13d0-4a0f-9be2-3c3977a6a4ba" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="${width}" height="${height}" viewBox="0 0 785 753.73">
    <defs>
        <linearGradient id="b4c3f788-9d51-49bd-96f9-c54f9fd51398" x1="452.8" y1="753.73" x2="452.8" gradientUnits="userSpaceOnUse">
            <stop offset="0" stop-color="gray" stop-opacity="0.25"/><stop offset="0.54" stop-color="gray" stop-opacity="0.12"/>
            <stop offset="1" stop-color="gray" stop-opacity="0.1"/>
        </linearGradient>
        <linearGradient id="778ed1c4-cafd-494e-8a04-dc2b91aee6fd" x1="209.37" y1="339.46" x2="209.37" y2="101.61" xlink:href="#b4c3f788-9d51-49bd-96f9-c54f9fd51398"/>
        <linearGradient id="92c6cb1b-63b4-4a00-ae9b-05b36bfadf3e" x1="32.38" y1="227.8" x2="174.2" y2="227.8" xlink:href="#b4c3f788-9d51-49bd-96f9-c54f9fd51398"/>
        <linearGradient id="3111d6ac-9e05-4a73-9878-bd628330955d" x1="211.6" y1="606.34" x2="211.6" y2="582.89" xlink:href="#b4c3f788-9d51-49bd-96f9-c54f9fd51398"/>
        <linearGradient id="2e076d7a-ee6b-46b6-b939-69cbad87608e" x1="572.28" y1="718" x2="572.28" y2="646.54" xlink:href="#b4c3f788-9d51-49bd-96f9-c54f9fd51398"/>
        <linearGradient id="acad6429-7160-42af-9a00-db2d81d2388a" x1="630.34" y1="245.66" x2="630.34" y2="91.56" xlink:href="#b4c3f788-9d51-49bd-96f9-c54f9fd51398"/>
    </defs>
    <title>account</title>
    <rect x="120.6" width="664.4" height="753.73" fill="url(#b4c3f788-9d51-49bd-96f9-c54f9fd51398)"/>
    <rect x="130.65" y="14.52" width="643.19" height="725.82" fill="#fff"/>
    <rect x="65.88" y="134" width="365.14" height="232.26" fill="#03a9f4" class="svg-accent" opacity="0.2"/>
    <rect y="101.61" width="418.74" height="237.84" fill="url(#778ed1c4-cafd-494e-8a04-dc2b91aee6fd)"/>
    <rect x="4.47" y="136.23" width="408.69" height="197.65" fill="#fff"/>
    <rect x="32.38" y="152.98" width="141.81" height="149.63" fill="url(#92c6cb1b-63b4-4a00-ae9b-05b36bfadf3e)"/>
    <rect x="36.85" y="156.33" width="134" height="142.93" fill="#03a9f4" class="svg-accent"/>
    <path d="M348.77,285.9c-1-8.49-2.5-17-5.75-24.94s-8.47-15.24-15.74-19.75a18,18,0,0,0-8.7-3c-5.69-.2-10.95,3.64-13.88,8.52a39.74,39.74,0,0,0-2.59,5.27,12.27,12.27,0,0,0-10.18,11.24,31.28,31.28,0,0,0,4.42,59.06v5.33a31.39,31.39,0,0,0-23.45,30.27v14.52h62.53V357.88A31.39,31.39,0,0,0,312,327.61v-5.33A31.28,31.28,0,0,0,335.45,292c0-.07,0-.15,0-.22l3.52,7.83c3.35,7.46,6.78,15.46,5.47,23.54C350.86,312.15,350.33,298.54,348.77,285.9Z" transform="translate(-207.5 -73.13)" fill="#fff"/>
    <rect x="4.47" y="106.08" width="408.69" height="30.15" fill="#03a9f4" class="svg-accent"/>
    <rect x="225.56" y="198.2" width="155.21" height="10.05" fill="#e0e0e0"/>
    <rect x="225.56" y="240.64" width="155.21" height="10.05" fill="#f5f5f5"/>
    <rect x="224.45" y="219.42" width="91.56" height="10.05" fill="#03a9f4" class="svg-accent" opacity="0.2"/>
    <rect x="224.45" y="261.85" width="62.53" height="10.05" fill="#69f0ae" class="svg-accent" opacity="0.2"/>
    <rect x="298.14" y="261.85" width="62.53" height="10.05" fill="#69f0ae" class="svg-accent"/>
    <rect x="185.36" y="419.86" width="539.34" height="18.98" fill="#e0e0e0"/>
    <rect x="185.36" y="461.17" width="179.78" height="18.98" fill="#e0e0e0"/>
    <rect x="544.92" y="461.17" width="179.78" height="18.98" fill="#69f0ae" class="svg-accent"/>
    <rect x="185.36" y="502.49" width="539.34" height="18.98" fill="#e0e0e0"/>
    <rect x="185.36" y="543.81" width="539.34" height="18.98" fill="#e0e0e0"/>
    <rect x="183.13" y="582.89" width="56.95" height="23.45" fill="url(#3111d6ac-9e05-4a73-9878-bd628330955d)"/>
    <rect x="185.36" y="585.12" width="53.6" height="18.98" fill="#03a9f4" class="svg-accent"/>
    <rect x="426.56" y="585.12" width="53.6" height="18.98" fill="#69f0ae" class="svg-accent"/>
    <rect x="671.1" y="585.12" width="53.6" height="18.98" fill="#69f0ae" class="svg-accent"/>
    <rect x="413.16" y="646.54" width="318.24" height="71.47" fill="url(#2e076d7a-ee6b-46b6-b939-69cbad87608e)"/>
    <rect x="418.74" y="654.35" width="305.96" height="54.72" fill="#03a9f4" class="svg-accent"/>
    <rect x="418.74" y="654.35" width="305.96" height="54.72" opacity="0.2"/>
    <rect x="529.29" y="91.56" width="202.11" height="154.1" fill="url(#acad6429-7160-42af-9a00-db2d81d2388a)"/>
    <rect x="537.11" y="101.61" width="187.6" height="134" fill="#03a9f4" class="svg-accent"/>
    <rect x="537.11" y="101.61" width="187.6" height="134" fill="#fff" opacity="0.2"/>
</svg>
`;
