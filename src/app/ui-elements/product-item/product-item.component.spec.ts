import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductItemComponent } from './product-item.component';
import { SharedModule } from './../../shared/shared.module';
import { TitlePipe } from '../../shared/title.pipe';
import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';
import { MenuService } from './../../services/menu.service';
import { AuthenticationService } from './../../services/authentication.service';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

describe('ProductItemComponent', () => {
  let component: ProductItemComponent;
  let fixture: ComponentFixture<ProductItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductItemComponent,
        TitlePipe
      ],
      imports: [
        SharedModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig),
        MatCardModule,
        MatSnackBarModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule
      ],
      providers: [
        MenuService,
        AuthenticationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductItemComponent);
    component = fixture.componentInstance;
    component.item = {
      category: '',
      name: '',
      vendor: '',
    };
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
    expect(component.item).toBeTruthy();
  });
});
