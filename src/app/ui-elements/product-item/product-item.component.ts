import { Component, OnInit, Input } from '@angular/core';
import { MenuService } from './../../services/menu.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  @Input() itemx: any;
  @Input() key: any;
  @Input() index: number;
  item: any;

  constructor(
    private ms: MenuService
  ) {
    this.item = { ...this.itemx };
  }

  ngOnInit() {
    this.item = { ...this.itemx };
    if (this.item.onSale && !this.item.hasOwnProperty('saleVariations')) { this.item.onSale = false; }
    if (!this.item.photoUrl) {
      this.item.photoUrl = this.ms.getBackDrop(this.item);
      this.item.noPhoto = true;
    } else {
      this.item.noPhoto = false;
    }
    if (this.key === 'specials' || this.key === 'search') { this.item.banner = true; }
    this.item.category = this.ms.replace(this.item.category);
    this.item.vendor ? this.item.vendor = this.ms.replace(this.item.vendor) : console.log('no-vendor');
    if (this.item.type) { this.item.type = this.ms.replace(this.item.type); }
    const noNum = this.item.name.replace(/[0-9]/g, '').trim().charAt(0);
    noNum === '' ? this.item.letter = this.item.category.trim().charAt(0) : this.item.letter = noNum;
  }

}
/*
if (this.item.onSale) {
  try {
    this.item.variations = this.ms.sortAndOrderVariations(this.item.variations);
    this.item.saleVariations = this.ms.sortAndOrderVariations(this.item.saleVariations);
    this.item.saleVariations.length > 0
      ? this.item.displayedPrice = this.item.variations.filter(i => i.name === this.item.saleVariations[0].name)
      : this.item.onSale = false;
  } catch (err) {
    console.log(err);
  }
} else if (!this.item.onSale) {
  this.item.variations = this.ms.sortAndOrderVariations(this.item.variations);
}
*/
