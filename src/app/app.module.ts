import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { UrlService } from './services/url.service';
import { PriceChipComponent } from './ui-elements/price-chip/price-chip.component';

import { OverlayModule } from '@angular/cdk/overlay';

// REMOVE //
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

export class HammerConfig extends HammerGestureConfig {
  overrrides = <any>{
    'swipe': { velocity: 0.4, threshold: 20, direction: Hammer.DIRECTION_ALL }
  }
}

import { VirtualScrollModule } from 'angular2-virtual-scroll';
import { FourohfourComponent } from './ui-elements/fourohfour/fourohfour.component';

const routes: Routes = [
  {path: '', redirectTo: 'consumer', pathMatch: 'full'},
  {path: 'consumer', loadChildren: 'app/consumer/consumer.module#ConsumerModule'},
  {path: 'onboarding', loadChildren: 'app/onboarding/onboarding.module#OnboardingModule'},
  {path: 'menu-iframe', loadChildren: 'app/menu-iframe/menu-iframe.module#MenuIframeModule'},
  {path: 'ui-element-testing', component: PriceChipComponent},
  {path: '**', component: FourohfourComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    PriceChipComponent,
    FourohfourComponent
    // StatusBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    OverlayModule,
    VirtualScrollModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule, // REMOVE
    MatTabsModule, // REMOVE
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  exports: [RouterModule],
  providers: [
    UrlService,
    { provide: HAMMER_GESTURE_CONFIG, useClass: HammerConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
