import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryMatch',
  pure: false
})
export class CategoryMatchPipe implements PipeTransform {

  transform(items: any, filter: any): Array<any> {
    if (items) {
      if (filter) {
        if (filter.types.length < 1 && filter.vendors.length < 1) { return items; }
        return items.filter((item) => {
          return (filter.types.length > 0 && filter.types.indexOf(item.type) > -1)
            || (filter.vendors.length > 0 && filter.vendors.indexOf(item.vendor) > -1);
        });
      }
      return items;
    }
    return [];
  }

  isRewardValidForLocation(reward, location) {
  }
}
