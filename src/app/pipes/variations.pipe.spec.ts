import { VariationsPipe } from './variations.pipe';
import { CurrencyPipe } from '@angular/common';

describe('VariationsPipe', () => {
  const cp: CurrencyPipe = new CurrencyPipe('USD');
  it('create an instance', () => {
    const pipe = new VariationsPipe(cp);
    expect(pipe).toBeTruthy();
  });
});
