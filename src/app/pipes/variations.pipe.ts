import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'variations'
})
export class VariationsPipe implements PipeTransform {

  constructor(
    private cp: CurrencyPipe
  ) {}

  transform(value: any, saleVars: any, onSale: boolean): any {
    let v, s;
    if (value) {
      v = this.sortAndOrder(value);
      if (onSale) {
        s = this.sortAndOrder(saleVars);
        if (s.length > 0) {
          const p = this.cur(v[0].price);
          // tslint:disable-next-line:max-line-length
          return `<span class="strike">${this.cur(v[0].price)}</span> <span>${this.cur(s[0].price)}</span> <span class="unit">${s[0].name}</span>`;
        } else {
          return `<span>${this.cur(v[0].price)}</span> <span class="unit">${v[0].name}</span>`
        }
      } else {
        return `<span>${this.cur(v[0].price)}</span> <span class="unit">${v[0].name}</span>`;
      }
    }
  }

  sortAndOrder(vars) {
    const prices = [];
    if (vars) {
      Object.keys(vars).forEach(key => {
        if (vars[key] > 0) {
          const price = {
            name: this.replace(key).toUpperCase(),
            price: vars[key]
          };
          prices.push(price);
        }
      });
    }
    return prices.sort(this.compare);
  }

  compare(a, b) {
    return a.price < b.price ? -1 : 1;
  }

  cur(val) {
    return this.cp.transform(val, 'USD', 'symbol', '1.0-2');
  }

  replace(string) {
    if (string) {
      if (string.indexOf('-') > -1) {
        return string.replace(/-/g, ' ');
      } else if (string.indexOf('_') > -1) {
        return string.replace(/_/g, ' ');
      } else {
        return string;
      }
    }
  }
}
