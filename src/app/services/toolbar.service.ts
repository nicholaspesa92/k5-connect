import { Injectable } from '@angular/core';

import { Observable ,  Subject } from 'rxjs';

@Injectable()
export class ToolbarService {

  toolbarTitle: Observable<string>;
  toolbarTitleObserv: Subject<string>;

  toolbarArrow: Observable<boolean>;
  toolbarArrowObserv: Subject<boolean>;

  toolbarShadow: Observable<boolean>
  toolbarShadowObserv: Subject<boolean>;

  toolbarColor: Observable<boolean>;
  toolbarColorObserv: Subject<boolean>;

  _locationsAmount: number;

  constructor() {
    this.toolbarTitleObserv = new Subject<string>();
    this.toolbarTitle = this.toolbarTitleObserv.asObservable();
    this.setNextTitle('Home');

    this.toolbarArrowObserv = new Subject<boolean>();
    this.toolbarArrow = this.toolbarArrowObserv.asObservable();

    this.toolbarShadowObserv = new Subject<boolean>();
    this.toolbarShadow = this.toolbarShadowObserv.asObservable();

    this.toolbarColorObserv = new Subject<boolean>();
    this.toolbarColor = this.toolbarColorObserv.asObservable();

    this._locationsAmount = 1;
  }

  setNextTitle(title) {
    this.toolbarTitleObserv.next(title);
  }

  setToolbarArrow(state) {
    this.toolbarArrowObserv.next(state);
  }

  setToolbarShadow(flag) {
    this.toolbarShadowObserv.next(flag);
  }

  setToolbarColor(color) {
    this.toolbarColorObserv.next(color);
  }

  set locationsAmount(n) {
    this._locationsAmount = n;
  }
  get locationsAmount() {
    return this._locationsAmount;
  }

}
