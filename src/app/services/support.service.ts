import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';

import { SupportRequest } from './../models/supportRequest';
import { AuthenticationService } from './authentication.service';
import { environment } from './../../environments/environment';

@Injectable()
export class SupportService {

  bId: string;
  srCollection: string;

  constructor(
    private as: AuthenticationService,
    private afs: AngularFirestore
  ) {
    this.bId = environment.businessId
    this.srCollection = `supportRequests/${this.bId}/requests`;
  }

  saveRequest(r: SupportRequest) {
    const x = {
      businessId: this.bId,
      name: r._name,
      phone: r._phone,
      email: r._email,
      subject: r._subject,
      desc: r._desc,
      date: new Date()
    }
    return this.afs.collection(`supportRequests`).add(x);
  }

}
