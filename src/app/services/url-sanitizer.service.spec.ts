import { TestBed, inject } from '@angular/core/testing';

import { UrlSanitizerService } from './url-sanitizer.service';

describe('UrlSanitizerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlSanitizerService]
    });
  });

  it('should be created', inject([UrlSanitizerService], (service: UrlSanitizerService) => {
    expect(service).toBeTruthy();
  }));
});
