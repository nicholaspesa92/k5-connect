import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthenticationService } from './authentication.service';
import { LocationService } from './location.service';
import { environment } from './../../environments/environment';
import { Reward } from './../models/reward';
import 'rxjs-compat'

@Injectable()
export class RewardService {

  private businessId: string;
  private uid: string;
  private businessCollectionPath: string;
  private rewardsCollectionPath: string;

  private currentLocation: string;

  constructor(
    private afs: AngularFirestore,
    private as: AuthenticationService,
    private ls: LocationService
  ) {
    // this.currentLocation = this.ls.currentLocation;
    this.uid = this.as.currentUserId;
    this.businessId = environment.businessId;
    this.businessCollectionPath = `businesses/${this.businessId}`;
    this.rewardsCollectionPath = `${this.businessCollectionPath}/rewards`;
  }

  getRewards() {
    return this.afs.collection<Reward>(this.rewardsCollectionPath, (ref) => {
      return ref.where('active', '==', true)
                .orderBy('points', 'asc');
    }).valueChanges();
  }

  getAvailableRewards(points) {
    return this.afs.collection(this.rewardsCollectionPath, (ref) => {
      return ref.where('points', '<=', points)
                .where('active', '==', true)
    }).snapshotChanges().map((rewards) => {
      return rewards.length;
    });
  }

  getSettings() {
    return this.afs.doc(`${this.businessCollectionPath}/settings/general`).valueChanges();
  }

  mock() {
    return this.afs.collection<Reward>(this.rewardsCollectionPath).valueChanges().first();
  }

}
