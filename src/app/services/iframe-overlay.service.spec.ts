import { TestBed, inject } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';

import { IframeOverlayService } from './iframe-overlay.service';

describe('IframeOverlayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule
      ],
      providers: [IframeOverlayService]
    });
  });

  it('should be created', inject([IframeOverlayService], (service: IframeOverlayService) => {
    expect(service).toBeTruthy();
  }));
});
