import { Injectable, ElementRef } from '@angular/core';
import {
  Overlay,
  OverlayConfig,
  OverlayRef,
  ConnectedPositionStrategy,
  OriginConnectionPosition,
  OverlayConnectionPosition,
  CloseScrollStrategy,
  VerticalConnectionPos
} from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';

import { ViewItemComponent } from './../menu-iframe/view-item/view-item.component';
import { OverlayWrapperComponent } from './../menu-iframe/design-one/design-one.component';

const DEFAULT_CONFIG: IframeOverlayConfig = {
  hasBackdrop: true,
  backdropClass: 'dark-backdrop',
  panelClass: 'item-overlay'
}

@Injectable()
export class IframeOverlayService {

  oRef: OverlayRef;

  item: any;

  constructor(
    private overlay: Overlay
  ) {
    this.item = {};
  }

  open(event, item) {
    console.log(item);
    this.item = item;

    const oConfig = { ...DEFAULT_CONFIG };

    this.oRef = this.createOverlay(oConfig, event, item);
    const viewItemPortal = new ComponentPortal(OverlayWrapperComponent);
    this.oRef.attach(viewItemPortal);
  }

  private createOverlay(config: IframeOverlayConfig, event, item) {
    const oConfig = this.getOverlayConfig(config, event, item);
    return this.overlay.create(oConfig);
  }

  private getOverlayConfig(config: IframeOverlayConfig, event, item) {
    /* const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally(); */

    const ovY: any = event.screenY + 500;

    const originPos: OriginConnectionPosition = {
      originX: null,
      originY: 'center'
    };

    const overlayPos: OverlayConnectionPosition = {
      overlayX: null,
      overlayY: 'bottom'
    };

    const elem = new ElementRef(document.getElementById(item.id));
    const postStrat = this.overlay.position().connectedTo(elem, originPos, overlayPos);

    const overlayConfig = new OverlayConfig({
      hasBackdrop: config.hasBackdrop,
      backdropClass: config.backdropClass,
      panelClass: config.panelClass,
      scrollStrategy: this.overlay.scrollStrategies.close(),
      positionStrategy: postStrat
    });

    return overlayConfig;
  }

  close() {
    this.item = {};
    this.oRef.dispose();
  }

  getItem() {
    return this.item;
  }

}

interface IframeOverlayConfig {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
}
