import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material';
import Spy = jasmine.Spy;
import { Observable } from 'rxjs/Observable';

import { MenuService } from './menu.service';

import { AuthenticationService } from './authentication.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../environments/environment';

describe('MenuService', () => {
  const exItem = {
    name: 'Ex',
    createdAt: new Date(),
    category: 'cat',
    price: 123,
    onSale: true
  };

  const valueChangesSpy: Spy = jasmine.createSpy('valueChanges').and.callFake((): Observable<any[]> => {
    return Observable.of([exItem]);
  });
  const snapshotChangesSpy: Spy = jasmine.createSpy('snapshotChanges').and.callFake((): Observable<any[]> => {
    return Observable.of([exItem]);
  });
  const collectionSpy: Spy = jasmine.createSpy('collection').and.returnValue({
    snapshotChanges: snapshotChangesSpy
  });
  const docSpy: Spy = jasmine.createSpy('doc').and.returnValue({
    snapshotChanges: snapshotChangesSpy
  });
  const dbStub: any = {
    collection: collectionSpy,
    doc: docSpy
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [{ provide: MenuService, useValue: dbStub }, { provide: AuthenticationService, useValue: dbStub }]
    });
  });

  it('should be created', inject([MenuService], (service: MenuService) => {
    expect(service).toBeTruthy();
  }));
});
