import { Injectable } from '@angular/core';

import { AuthenticationService } from './../services/authentication.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { environment } from './../../environments/environment';

import { Member } from './../models/member';
import { User } from './../models/user';


@Injectable()
export class MemberService {

  businessId: string;
  memberId: string;

  uid: string;
  memberDocPath: string;

  user: Observable<User>;
  member: Member;

  editableFieldName: string;
  editableFieldValue: string;
  editableFieldType: number;
  emailForPw: string;

  constructor(
    private afs: AngularFirestore,
    private as: AuthenticationService
  ) {
    this.businessId = environment.businessId;
    this.as.user.subscribe((user) => {
      if (user) {
        this.memberId = user.uid;
      }
    });
    this.memberDocPath = `businesses/${this.businessId}/members`;
    this.editableFieldName = '';
    this.editableFieldValue = '';
    this.editableFieldType = 0;
    this.emailForPw = '';
  }

  getMember(uid) {
      return this.afs.doc<Member>(`${this.memberDocPath}/${uid}`).snapshotChanges()
        .map(actions => {
            const data = actions.payload.data() as Member;
            const id = actions.payload.id;
            this.member = data;
            return {id, ...data};
        });
  }

  saveMemberField(key, value, uid) {
    const d = {};
    d[key] = value;
    return this.afs.collection(`${this.memberDocPath}`).doc(`${uid}`).update(d);
  }

  saveSettings(settings, uid) {
    return this.afs.collection(`${this.memberDocPath}`).doc(`${uid}`).update(settings);
  }

  getAllMembers() {
    return this.afs.collection(`${this.memberDocPath}`)
      .snapshotChanges()
      .map((members) => {
        return members.map((member) => {
          const id = member.payload.doc.id;
          const data = member.payload.doc.data() as Member;
          return { id, ...data };
        });
      });
  }

  setFieldName(name) { this.editableFieldName = name; }
  getFieldName() { return this.editableFieldName; }
  setFieldValue(value) { this.editableFieldValue = value; }
  getFieldValue() { return this.editableFieldValue; }
  setFieldType(type) { this.editableFieldType = type; }
  getFieldType() { return this.editableFieldType; }
  setEmailForPw(email) { this.emailForPw = email; }
  getEmailForPw() { return this.emailForPw; }

}
