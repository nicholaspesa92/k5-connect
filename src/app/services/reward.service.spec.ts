import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material';

import { RewardService } from './reward.service';

import { AuthenticationService } from './authentication.service';
import { LocationService } from './location.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../environments/environment';

describe('RewardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [RewardService, AuthenticationService, LocationService]
    });
  });

  it('should be created', inject([RewardService], (service: RewardService) => {
    expect(service).toBeTruthy();
  }));
});
