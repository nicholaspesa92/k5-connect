import { Injectable } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';
import { Router } from '@angular/router';


import { MatSnackBar } from '@angular/material';

import { environment } from './../../environments/environment';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

import { User } from './../models/user';
import { Member } from './../models/member';

@Injectable()
export class AuthenticationService {

  authState: any = null;
  // user: Observable<firebase.User>;
  phoneVerificationId: string = null;

  _phoneVerified = false;
  googleVerified = false;
  facebookVerified = false;

  userObs: Observable<User>;

  private _DB: any;

  // Tim Changes
  public isLoggedIn = false;
  private uid: string | false = false;
  public user: Observable<firebase.User>;
  displayName: string | null;
  businessId: string;

  private _returnPath: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private sb: MatSnackBar
  ) {
    this.returnPath = 'consumer/menu';
    this.user = this.afAuth.authState;
    this.user.subscribe((auth) => {
      if (auth) {
        this.authState = auth;
      }
    });
    this.phoneVerificationId = '';

    this.businessId = environment.businessId;
    this._DB = firebase.firestore();
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  get currentUserObservable(): any {
    return this.afAuth.authState;
  }

  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  get currentUserDisplayName(): string {
    if (!this.authState) {
      return 'Guest';
    } else {
      return this.authState['displayName'] || '';
    }
  }

  set phoneVerified(flag) {
    this._phoneVerified = flag;
  }
  get phoneVerified() {
    return this._phoneVerified;
  }

  set returnPath(path) {
    this._returnPath = path;
  }
  get returnPath() {
    return this._returnPath;
  }

  checkIfUserExists() {
    return this.afs.doc<User>(`users/${this.currentUserId}`) as User !== null;
  }

  getCurrentUserFromFirebaseAsUser() {
    return this.afs.doc<User>(`users/${this.currentUserId}`).valueChanges();
  }

  getCurrentUserFromFirebase(uid) {
    return this.afs.doc<User>(`users/${uid}`).valueChanges();
  }

  getCurrentlyStoredUser() {
    return this.userObs;
  }

  getCurrentBusinessSettings() {
    return this.afs.doc(`businesses/${this.businessId}/settings/general`).valueChanges();
  }

  getCurrentBusinessDoc() {
    return this.afs.doc(`businesses/${this.businessId}`).valueChanges();
  }

  getCurrentBusinessCategories() {
    return this.afs.collection(`businesses/${this.businessId}/settings/general/categories`).snapshotChanges()
      .map((categories) => {
        return categories.map((category) => {
          const id = category.payload.doc.id;
          const data = category.payload.doc.data();
          return { id, ...data };
        });
      });
  }

  getCurrentBusinessTypes() {
    return this.afs.collection(`businesses/${this.businessId}/settings/general/types`).snapshotChanges()
      .map((types) => {
        return types.map((type) => {
          const id = type.payload.doc.id;
          const data = type.payload.doc.data();
          return { id, ...data };
        });
      });
  }

  getCurrentBusinessVendors() {
    return this.afs.collection(`businesses/${this.businessId}/settings/general/vendors`).snapshotChanges()
      .map((vendors) => {
        return vendors.map((vendor) => {
          if (vendor.payload.doc.exists) {
            const id = vendor.payload.doc.id;
            const data = vendor.payload.doc.data();
            return { id, ...data };
          }
        });
      });
  }

  // Phone Login //
  phoneLogin(num, appVerifier) {
    const provider = new firebase.auth.PhoneAuthProvider;
    return provider.verifyPhoneNumber(num, appVerifier);
  }
  phoneVerify(vHash, vCode) {
    const cred = firebase.auth.PhoneAuthProvider.credential(vHash, vCode);
    return this.afAuth.auth.signInWithCredential(cred);
  }
  // End Phone Login //

  // Phone Updater //
  phoneUpdate(num, appVerfier) {
    const provider = new firebase.auth.PhoneAuthProvider;
    return provider.verifyPhoneNumber(num, appVerfier);
  }
  phoneUpdateVerifier(vHash, vCode) {
    const cred = firebase.auth.PhoneAuthProvider.credential(vHash, vCode);
    return this.updateUserPhoneNumber(cred);
  }
  // End Phone Update //

  // Phone Linking //
  phoneLinking(num, appVerifier) {
    const provider = new firebase.auth.PhoneAuthProvider;
    return provider.verifyPhoneNumber(num, appVerifier);
  }
  phoneLinkVerifier(vHash, vCode) {
    const cred = firebase.auth.PhoneAuthProvider.credential(vHash, vCode);
    return this.afAuth.auth.currentUser.linkWithCredential(cred);
  }
  // End Phone Linking //

  getUserToken() {
    return this.afAuth.auth.currentUser.getIdToken();
  }

  emailLink(email, password) {
    const cred = firebase.auth.EmailAuthProvider.credential(email, password);
    return this.emailLinking(cred);
  }

  emailSignIn(email, pw) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, pw);
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider;
    return this.socialLinking(provider);
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider;
    return this.socialLinking(provider);
  }

  anonymousLogin() {
    return this.afAuth.auth.signInAnonymously();
  }

  saveUser(userData) {
    return this.saveUserData(userData);
  }

  saveMember() {
    return this.setMemberData();
  }

  updateUser(fieldName, fieldValue) {
    const data = {};
    data[fieldName] = fieldValue;
    this.afs.collection('users').doc(this.currentUserId).update(data);
  }

  updateUserDisplayName(name: string) {
    const x = {
      displayName: name,
      photoURL: ''
    }
    return this.afAuth.auth.currentUser.updateProfile(x);
  }

  updateUserEmail(email) {
    return this.afAuth.auth.currentUser.updateEmail(email);
  }

  updateUserPhoneNumber(number) {
    return this.afAuth.auth.currentUser.updatePhoneNumber(number);
  }

  updatePassword(newPw) {
    return this.afAuth.auth.currentUser.updatePassword(newPw);
  }

  private socialSignIn(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.authState = credential.user;
      })
      .catch(error => console.log(error));
  }

  private socialLinking(provider) {
    return this.afAuth.auth.currentUser.linkWithPopup(provider);
  }

  private emailLinking(cred) {
    return this.afAuth.auth.currentUser.linkWithCredential(cred);
  }

  private saveUserData(data) {
    return this.afs.collection('users').doc(this.currentUserId).update(data);
  }

  memberExists() {
    return this._DB.doc(`businesses/${this.businessId}/members/${this.currentUserId}`);
  }

  isMemberX() {
    return this.afs.doc(`businesses/${this.businessId}/members/${this.currentUserId}`).snapshotChanges()
      .map(action => {
        console.log('here');
        console.log(action.payload.exists);
        return action.payload.exists;
      });
  }

  isMemberY() {
    return this.afs.doc(`businesses/${this.businessId}/members/${this.currentUserId}`).snapshotChanges()
      .map((member) => {
        const exists = member.payload.exists;
        let id;
        let data;
        if (exists) {
          id = member.payload.id;
          data = member.payload.data() as Member;
        }
        return { id, exists, ...data };
      });
  }

  isMember(mId) {
    return this.afs.doc(`businesses/${this.businessId}/members/${mId}`).snapshotChanges()
      .map((member) => {
        const exists = member.payload.exists;
        let id;
        let data;
        if (exists) {
          id = member.payload.id;
          data = member.payload.data() as Member;
        }
        return { id, exists, ...data };
      });
  }

  setMemberData() {
    const mData = {
      user: this.afs.doc(`users/${this.currentUserId}`).ref,
      lastCheckin: new Date(),
      memberSince: new Date(),
      appInstalled: true
    }
    return this.afs.collection(`businesses/${this.businessId}/members`).doc(`${this.currentUserId}`).set(mData, {merge: true});
  }

  setMemberDataX(uid, data) {
    return this.afs.collection(`businesses/${this.businessId}/members`).doc(`${uid}`).set(data, { merge: true });
  }

  setMemberDataUsingFirebase() {
    const mData = {
      user: this.afs.doc(`users/${this.currentUserId}`).ref,
      lastCheckin: new Date(),
      memberSince: new Date(),
      appInstalled: true
    }
    return this._DB.doc(`businesses/${this.businessId}/members/${this.currentUserId}`).set(mData, {merge: true});
  }

  updateMemberData() {
    const mData = {
      appInstalled: true
    }
    return this.afs.collection(`businesses/${this.businessId}/members`).doc(`${this.currentUserId}`).update(mData);
  }

  signOut() {
    this.afAuth.auth.signOut();
    this.router.navigate(['']);
    this.snack('User Logged Out', 'OK', 3000);
  }

  snack(message, action, duration) {
    this.sb.open(message, action, { duration: duration });
  }

}

/*phoneVerify(verificationCode) {
  const cred = firebase.auth.PhoneAuthProvider.credential(this.phoneVerificationId, verificationCode);
  return this.afAuth.auth.signInWithCredential(cred)
    .then((user) => {
      this.authState = user;
      console.log(user);
      return user;
    })
    .catch((error) => {
      console.log(error);
      return error;
    });
}*/
