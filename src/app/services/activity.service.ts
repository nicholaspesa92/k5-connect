import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { AuthenticationService } from './authentication.service';
import { environment } from './../../environments/environment';

import { Activity } from './../models/activity';

@Injectable()
export class ActivityService {

  businessId: string;
  memberId: string;

  constructor(
    private afs: AngularFirestore,
    private as: AuthenticationService
  ) {
    this.businessId = environment.businessId;
    this.as.user.subscribe((user) => {
      if (user) {
        this.memberId = user.uid;
      }
    });
  }

  getActivity(): Observable<Activity[]> {
    return this.afs.collection<Activity[]>(`/businesses/${this.businessId}/members/${this.memberId}/activities`, (ref) => {
      return ref.orderBy('createdAt', 'desc');
    })
    .snapshotChanges().map(actions => {
      return actions.map(a => {
          const data = a.payload.doc.data() as Activity;
          const id = a.payload.doc.id;
          return {id, ...data};
      });
    });
  }

  getSettings() {
    return this.afs.doc(`businesses/${this.businessId}/settings/general`).valueChanges();
  }

}
