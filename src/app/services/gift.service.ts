import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

import { Gift } from './../models/gift';
import { LoyaltyMessage } from './../models/loyalty-message';

import { environment } from './../../environments/environment';

@Injectable()
export class GiftService {

  bId: string;

  constructor(
    private afs: AngularFirestore
  ) {
    this.bId = environment.businessId;
  }

  getGiftsFromBusiness() {
    return this.afs.collection<Gift>(`businesses/${this.bId}/gifts`).valueChanges();
  }

  getGiftsFromMember(uid) {
    return this.afs.collection<Gift>(`businesses/${this.bId}/members/${uid}/gifts`).snapshotChanges()
      .map((snapshot) => {
        return snapshot.map((s) => {
          const data = s.payload.doc.data() as Gift;
          const id = s.payload.doc.id;
          return { id, ...data };
        });
      });
  }

}
