import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { AngularFirestore } from 'angularfire2/firestore';

import { Order } from './../models/order';
import { OrderItem } from './../models/order-item';

import { environment } from './../../environments/environment';

@Injectable()
export class OrderService {

  bId: string;
  lId: string;

  orderItems: OrderItem[];
  total: number;

  lengthSub: Subject<number> = new Subject<number>();

  statuses: any[];

  _orderAllowed: boolean;

  constructor(
    private afs: AngularFirestore
  ) {
    this.bId = environment.businessId;
    this.lId = localStorage.getItem('lId');
    this.orderItems = [];
    this.statuses = [ 'new', 'pending', 'ready', 'complete' ];
    this._orderAllowed = true;
  }

  addItem(menuItem, variation, quantity) {
    const i = {
      id: menuItem.id,
      name: menuItem.name,
      unit: variation.name,
      price: variation.price,
      quantity: quantity,
      variations: menuItem.variations,
      saleVariations: menuItem.saleVariations ? menuItem.saleVariations : {},
      category: menuItem.category,
      _refProduct: this.afs.doc(`businesses/${this.bId}/locations/${this.lId}/menu/${menuItem.id}`).ref
    };
    this.orderItems.push(i);
    this.lengthSub.next(this.orderItems.length);
  }
  removeItem(item) {
    const x = this.orderItems.indexOf(item);
    this.orderItems.splice(x, 1);
    this.lengthSub.next(this.orderItems.length);
  }
  getCart() {
    return this.orderItems;
  }

  createOrder(lId, data) {
    return this.afs.collection(`businesses/${this.bId}/locations/${lId}/orders`).add(data);
  }

  getOrders(lId, uid) {
    return this.afs.collection<Order[]>(`businesses/${this.bId}/locations/${lId}/orders`, (ref) => {
      return ref.where('userId', '==', uid)
                .orderBy('createdAt', 'asc');
    }).snapshotChanges()
      .map((orders) => {
        return orders.map((order) => {
          const id = order.payload.doc.id;
          const data = order.payload.doc.data() as Order;
          return { id, ...data };
        });
      });
  }

  getOrder(lId, id) {
    return this.afs.doc<Order>(`businesses/${this.bId}/locations/${lId}/orders/${id}`).snapshotChanges()
      .map((order) => {
        const xid = order.payload.id;
        const xdata = order.payload.data() as Order;
        return { xid, ...xdata };
      });
  }

  cancelOrder(id) {
    const x = {
      canceled: true
    }
    return this.afs.doc<Order>(`businesses/${this.bId}/locations/${this.lId}/orders/${id}`).update(x);
  }

  clearCart() {
    this.lengthSub.next(0);
    this.orderItems = [];
  }

  lengthObs() {
    return this.lengthSub.asObservable();
  }

  set orderAllowed(flag) {
    this._orderAllowed = flag;
  }
  get orderAllowed() {
    return this._orderAllowed;
  }

}
