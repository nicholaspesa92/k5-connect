import { TestBed, inject } from '@angular/core/testing';

import { SearchStringService } from './search-string.service';

describe('SearchStringService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchStringService]
    });
  });

  it('should be created', inject([SearchStringService], (service: SearchStringService) => {
    expect(service).toBeTruthy();
  }));
});
