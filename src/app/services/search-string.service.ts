import { Injectable } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';

@Injectable()
export class SearchStringService {

  private subject = new Subject<any>();

  lastSearch = { text: '', searching: false };

  changeString(string, searching) {
    this.subject.next({ text: string, searching: searching });
    this.lastSearch = { text: string, searching: searching };
  }

  getString(): Observable<any> {
    return this.subject.asObservable();
  }

  getLastSearch() {
    return this.lastSearch;
  }

}
