import { Injectable } from '@angular/core';

const SMALL_WIDTH = 1024;

@Injectable()
export class WindowService {

  constructor() { }

  get windowRef() {
    return window;
  }

}
