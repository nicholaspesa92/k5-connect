import { Injectable } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Injectable()
export class UrlSanitizerService {

  constructor(
    private sani: DomSanitizer
  ) { }

  getSanitizedUrl(url) {
    return this.sani.bypassSecurityTrustResourceUrl(url);
  }

  getSanitizedStyle(url) {
    return this.sani.bypassSecurityTrustStyle(`url(${url})`);
  }

  getSanitizedHtml(html) {
    return this.sani.bypassSecurityTrustHtml(html);
  }

}
