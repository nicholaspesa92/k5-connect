import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthenticationService } from './authentication.service';
import { environment } from './../../environments/environment';
import { BusinessLocation } from './../models/businesslocation';
import { Observable } from 'rxjs';
import { Menu } from '../models/menu';
import { Price } from '../models/price';

@Injectable()
export class MenuService {

  businessId: string;
  businessCollectionPath: string;
  locationId: string;
  menuCollectionPath: string;

  dropUrls: any[];
  largeDropUrls: any[];

  constructor(
    private afs: AngularFirestore,
    private as: AuthenticationService
  ) {
    this.businessId = environment.businessId;
    this.businessCollectionPath = `businesses/${this.businessId}`;
    this.menuCollectionPath = `${this.businessCollectionPath}/locations/${this.locationId}/menu`;
    this.setDropUrls();
    this.setLargeDropUrls();
  }

  getMenuItems(locationId): Observable<Menu[]> {
    return this.afs.collection<Menu[]>(`businesses/${this.businessId}/locations/${locationId}/menu`, (ref) => {
      return ref.where('active', '==', true)
                .where('inStock', '==', true)
      }).snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Menu;
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
  }

  getBusinessMenuItems(lId?) {
    return this.afs.collection(`businesses/${this.businessId}/menu`, (ref) => {
      return ref.where('active', '==', true)
                .where('inStock', '==', true)
                .where(`locations.${lId}`, '==', true)
    }).snapshotChanges().map((items) => {
      return items.map((item) => {
        if (item.payload.doc.exists) {
          const id = item.payload.doc.id;
          const data = item.payload.doc.data();
          return { id, ...data };
        }
      });
    });
  }

  getMenuItem(locationId, itemId): Observable<Menu> {
    return this.afs.doc<Menu>(`businesses/${this.businessId}/locations/${locationId}/menu/${itemId}`).snapshotChanges().map(actions => {
      const data = actions.payload.data() as Menu;
      const id = actions.payload.id;
      return { id, ...data };
    });
  }

  getBusinessMenuItem(itemId) {
    return this.afs.doc<Menu>(`businesses/${this.businessId}/menu/${itemId}`).snapshotChanges().map((item) => {
      const id = item.payload.id;
      const data = item.payload.data() as Menu;
      return { id, ...data };
    });
  }

  transformArray(array: Array<any>, field) {
    if (array) {
      const groupedObj = array.reduce((prev, cur) => {
        if (!prev[cur[field]]) {
          prev[cur[field]] = [cur];
        } else {
          prev[cur[field]].push(cur);
        }
        return prev;
      }, {});
      return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key], filteredValue: groupedObj[key] }));
    }
    return [];
  }

  getField(array: Array<any>, field) {
    if (array) {
      const groupedObj = array.reduce((prev, cur) => {
        if (!prev[cur[field]]) {
          prev[cur[field]] = [cur];
        } else {
          prev[cur[field]].push(cur);
        }
        return prev;
      }, {});
      if (groupedObj) { return Object.keys(groupedObj).map(key => ({ key })); }
      return [];
    }
    return [];
  }

  sortAndOrderVariations(variations) {
    const prices = [];
    if (variations) {
      Object.keys(variations).forEach(key => {
        if (variations[key] > 0) {
          const price = {
            name: this.replace(key),
            price: variations[key]
          };
          prices.push(price);
        }
      });
    }
    return prices.sort(this.compare);
  }

  sortAndOrderSaleVariations(variations) {
    const prices = [];
    Object.keys(variations).forEach(key => {
      if (variations[key] > 0) {
        const price = {
          name: key,
          price: variations[key]
        };
        prices.push(price);
      }
    });
    return prices.sort(this.compare);
  }

  compare(a, b) {
    return a.price < b.price ? -1 : 1;
  }

  replace(string) {
    if (string) {
      if (string.indexOf('-') > -1) {
        return this.replaceDash(string);
      } else if (string.indexOf('_') > -1) {
        return this.replaceUnderscore(string);
      } else {
        return string;
      }
    }
  }
  replaceDash(string) {
    return string.replace(/-/g, ' ');
  }
  replaceUnderscore(string) {
    return string.replace(/_/g, ' ');
  }

  getBackDrop(item) {
    const hash = this.getHash(item.name);
    const n = Math.abs((hash) % this.largeDropUrls.length);
    return this.largeDropUrls[n];
  }

  getHash(name) {
    let hash = 0, i, chr;
    if (!name || name.length === 0) {
      return hash;
    }
    for (i = 0; i < name.length; i++) {
      chr = name.charCodeAt(i);
      /* tslint:disable:no-bitwise */
      hash = ((hash << 5) - hash) + chr;
      hash |= 0;
    }
    return hash;
  }

  // tslint:disable:max-line-length
  setDropUrls() {
    this.dropUrls = [
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-blue.png?alt=media&token=4275e4b0-c4db-4b42-822d-72a5049982b5',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-green.png?alt=media&token=9bc4662f-9b79-4252-9dcd-3abc622c8dd6',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-pink.png?alt=media&token=2145d615-7a74-4ae6-a125-c6498d4bcb4f',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-purple.png?alt=media&token=373a6d51-22c6-4f90-aa94-a690ba0e46a3',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-red.png?alt=media&token=377f7c75-ed4a-4b76-8460-fee80aabd201',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fcross-yellow.png?alt=media&token=8142f8e2-1a08-42f6-87be-9b6e66f19583',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-blue.png?alt=media&token=78137fe9-9417-41bd-9d55-f5ce2d917cea',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-green.png?alt=media&token=725b7d34-352e-42fc-9964-70e674018672',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-pink.png?alt=media&token=15b9958a-e788-4723-a084-14edba56c4d6',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-purple.png?alt=media&token=05302e34-e0c4-48f5-b9b9-325cd0d2937f',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-red.png?alt=media&token=dece544d-d3f7-452b-a107-f5e4ef8c4a13',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fdiamond-yellow.png?alt=media&token=228b9bd3-0d15-4667-9e27-6dcebe5bb40e',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-blue.png?alt=media&token=31f3017a-1740-4550-bf13-9df750d9dab0',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-green.png?alt=media&token=0563558a-5be5-4f63-91c7-4a3fc6bd0151',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-pink.png?alt=media&token=20abe666-9b56-4d93-8204-80b50fdd7d96',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-purple.png?alt=media&token=9ab824e0-7146-4784-9f29-fb8626cff2b6',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-red.png?alt=media&token=cb19be71-fc63-4e26-90ed-ccf9094913ed',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fhoneycomb-yellow.png?alt=media&token=32d75e3d-200e-4c74-b932-e6403731ec91',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-blue.png?alt=media&token=28ba2cc0-2c5f-4a9d-9443-8dd320ec5e6c',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-green.png?alt=media&token=a609c231-03b6-4230-a730-51e8376381a6',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-pink.png?alt=media&token=a2f79229-4c63-4c00-b2a8-3b143933f976',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-purple.png?alt=media&token=022819f6-499a-4131-8bd5-4a07ca0589a8',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-red.png?alt=media&token=05182338-efcb-43d3-9d0f-fb83cd29a9f8',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fswirls-yellow.png?alt=media&token=3d6d9b01-5909-4954-ae20-7f5a4344c9d0',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-blue.png?alt=media&token=c77e4a82-fabf-491d-8cdb-e1b817e52361',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-green.png?alt=media&token=c16e6ce5-35da-4407-8f1b-cf24b060a4ea',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-pink.png?alt=media&token=e9f6f33a-d3a3-4067-b657-054ef225242d',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-purple.png?alt=media&token=b20dc861-8ab2-410b-91f2-726057de38eb',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-red.png?alt=media&token=27fea640-9ab9-497a-b70a-ffd3d58c5c9a',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Fzag-yellow.png?alt=media&token=31132d42-137f-4b90-ad12-45f4298424d8'
    ];
  }

  setLargeDropUrls() {
    this.largeDropUrls = [
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-blue.png?alt=media&token=66de1e7f-054c-4ba6-afae-b03636823b54',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-cyan.png?alt=media&token=84748eee-5531-470d-be19-528598c2037e',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-green.png?alt=media&token=5d76725d-e66e-4f43-8e9d-87afe2c4eeef',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-orange.png?alt=media&token=234cd88f-2ed2-41ef-a0c9-84e24844dd61',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-pink.png?alt=media&token=4cc5c54d-3552-41ce-b973-f857a57fb3eb',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-purple.png?alt=media&token=b885eef1-addb-4f76-8eff-78a68e3b1840',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-red.png?alt=media&token=2b89de7f-294a-4ff0-b536-12534d7a0e8e',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fcross-teal.png?alt=media&token=97bc7b43-412d-4a44-827f-4c0842d73c66',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-blue.png?alt=media&token=6120bbec-0e0c-4c40-a0ec-bf1ecadc92cd',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-cyan.png?alt=media&token=50f3a8d6-9180-460b-b8ce-22c69bda4351',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-green.png?alt=media&token=0d550f0b-7836-4577-b91b-ccb590301ff1',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-orange.png?alt=media&token=038faf6a-f1d1-4e18-b7af-0ffece291533',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-pink.png?alt=media&token=aaf4398b-e4ab-4e0e-840a-f5ebdd8c8f08',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-purple.png?alt=media&token=00a789e6-55f0-418f-8759-19fac037a46c',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-red.png?alt=media&token=d9e03e80-90c2-49df-95fa-3e8cb69cbce9',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fdiamond-teal.png?alt=media&token=28521937-9893-42a5-a283-eab13abdccae',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-blue.png?alt=media&token=462734fb-17e3-4900-b66e-360f90b5dc14',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-cyan.png?alt=media&token=863cdcc0-704f-420c-9fbb-f5210fd64833',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-green.png?alt=media&token=d497a7aa-bdd4-4b88-9ffe-233a016234e5',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-orange.png?alt=media&token=fe2a710c-25d5-477b-8887-f2c60b77d3c5',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-pink.png?alt=media&token=bc5e858e-20a7-4e6a-b6a8-531538e40139',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-purple.png?alt=media&token=e1e5927a-6cca-48b3-aa66-4b01127b4bf5',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-red.png?alt=media&token=29b700dd-3c34-4481-88aa-2dfe3d42cc61',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fhoneycomb-teal.png?alt=media&token=c91f0e74-f56d-4a9a-90e7-957ce5df6a7f',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-blue.png?alt=media&token=dc9e64de-ba0f-488a-b2cc-6a809a59018f',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-cyan.png?alt=media&token=bc4f2e8d-1c7b-4448-af0a-138b02ca800a',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-green.png?alt=media&token=41f39342-8990-47a1-bf3d-3c8bae3d6d39',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-orange.png?alt=media&token=e2d3b9a4-85c1-46aa-a0db-0c9dc4ac12dc',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-pink.png?alt=media&token=1d17646a-1a38-4621-a00a-2e0989fef3ed',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-purple.png?alt=media&token=2ee330a9-8e57-4eaa-9a57-8436e8ad970c',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-red.png?alt=media&token=d3bbf87e-5758-43c9-82c3-f3f93c1bc648',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fswirls-teal.png?alt=media&token=fc3d3716-b1f4-4df2-9186-af6560363b97',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-blue.png?alt=media&token=7c8c9102-28af-466a-b57c-491f9d76deee',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-cyan.png?alt=media&token=54161604-d21b-4c39-954d-0792dbeab06e',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-green.png?alt=media&token=2dae158b-3a9d-443a-92d0-f3154d92a38a',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-orange.png?alt=media&token=815635b8-85f0-4594-adcd-be257079b1b6',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-pink.png?alt=media&token=3ade3bf3-2f2a-48d1-90d8-4d6e3ec1b290',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-purple.png?alt=media&token=a57a0188-9d52-40c8-9ad0-b480a94b41ac',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-red.png?alt=media&token=4f53c670-5f90-4eb9-9d68-e4ee41191bd1',
      'https://firebasestorage.googleapis.com/v0/b/connect-admin-18d73.appspot.com/o/generic-backdrops%2Flarge%2Fzag-teal.png?alt=media&token=0e0c8c0e-36a5-45e0-bcfa-a15def4ccc36'
    ];
  }

}
