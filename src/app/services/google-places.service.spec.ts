import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { GooglePlacesService } from './google-places.service';

describe('GooglePlacesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [GooglePlacesService]
    });
  });

  it('should be created', inject([GooglePlacesService], (service: GooglePlacesService) => {
    expect(service).toBeTruthy();
  }));
});
