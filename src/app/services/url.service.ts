import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {

  _lastPath: string;

  constructor() {
    this._lastPath = '/consumer';
  }

  set lastPath(lastPath) {
    this._lastPath = lastPath;
  }
  get lastPath() {
    return this._lastPath;
  }

}
