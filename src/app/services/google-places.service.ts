import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Injectable()
export class GooglePlacesService {

  reqUrlBase: string;
  data = [];

  host: string;
  path: string;
  bearer: string;

  placeid: string;
  fbaseid: string;

  constructor(
    private httpClient: HttpClient
  ) {
    this.placeid = 'ChIJM73BljrGuFQR6OMs9AWS0wM';
    this.fbaseid = environment.firebase.projectId;
    this.reqUrlBase = 'https://maps.googleapis.com/maps/api/place/details/json?';
    this.host = `https://us-central1-${this.fbaseid}.cloudfunctions.net`;
    this.path = `mapsApi/placeDetail?placeId`;
    this.setBearer();
  }

  getGooglePlaceDetails(token) {
    return this.httpClient.get(this.getUrl(), { headers: this.getHeaders(token) });
  }

  postGooglePlaceDetails(placeId, apiKey) {
    return this.httpClient.post(this.formatUrl(placeId, apiKey), this.data);
  }

  getPlaceDetails(placeid, token) {
    return this.httpClient.get(this.url(placeid), { headers: this.getHeaders(token) });
  }

  formatUrl(placeId, apiKey) {
    return this.reqUrlBase
      + 'placeid=' + placeId
      + '&key=' + apiKey;
  }

  getUrl() {
    return this.host
      + this.path;
  }

  url(placeid) {
    return `${this.host}/${this.path}=${placeid}`;
  }

  getHeaders(token) {
    return new HttpHeaders().set('Authorization', `Bearer ${token}`);
  }

  setBearer() {
    // tslint:disable-next-line:max-line-length
    this.bearer = 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjQzYzljZGMxZTY0MGEwNWViMGZjOTc2YjYxYWNlMGU0NGZlZmUwODQifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vY2FubmFmb3dlYnNpdGUiLCJhdWQiOiJjYW5uYWZvd2Vic2l0ZSIsImF1dGhfdGltZSI6MTUxMjY4NTg3MywidXNlcl9pZCI6IkpPbTFwcUVCQkxSOUg3cEhjcHViY2pkOFZLSTMiLCJzdWIiOiJKT20xcHFFQkJMUjlIN3BIY3B1YmNqZDhWS0kzIiwiaWF0IjoxNTEyNzQ5OTg4LCJleHAiOjE1MTI3NTM1ODgsImVtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJ1c2VyQGV4YW1wbGUuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.J_Kumr-nj97ATVr-LeLX5ztxH8ODnOSujDA0NslQTLAzi4Sm4QevxGt4wVviTgJqYTWfQD7pTVXUXwKdl4Hb1mC2l6eaAByHtrprL1eDwV4btGceCUUV3Z-vfVoCOiP50iVZXpk__gp_UV60yW2w2kjsd_SLH2HGSVWXKBCcEl-bf8TU6kFqLiGv1Uq8IrxDgLXF0KUjdY1znFWd0Jd_80ODaPsmMiUjFb2vcIKYCtBk9xVVJZe5CnOLpBi1Y2mqinqJpVboKkrvcEnTXEa-ieF2O5LAxAsL2wMCx8wdo-ZGGn4y9joPTNxY_5WFfbgSJRBf7GeZ0J38s2iPykkyuQ';
  }

}
