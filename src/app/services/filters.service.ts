import { Injectable } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';

@Injectable()
export class FiltersService {

  private subject = new Subject<any>();
  private locationSub = new Subject<any>();

  filters: any;

  _filter: any = { types: [], vendors: [] };

  changeFilter(array, vendors) {
    this.subject.next({ filters: array, vendors: vendors });
    this.filters = {filters: array, vendors: vendors};
  }

  changeLocation(data) {
    this.locationSub.next(data);
  }

  getFilters(): Observable<any> {
    return this.subject.asObservable();
  }

  getLocationFilter(): Observable<any> {
    return this.locationSub.asObservable();
  }

  getFiltersArr() {
    return this.filters;
  }

  set filter(filter) {
    this._filter = filter;
  }

  get filter() {
    return this._filter;
  }

}
