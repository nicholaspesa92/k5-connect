import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthenticationService } from './authentication.service';
import { environment } from './../../environments/environment';
import { BusinessLocation } from './../models/businesslocation';
import { Review } from './../models/review';

const MAPS_API_KEY = 'AIzaSyAIC7XTWnix5-Lm04swXfmUUPe8NHryZFI';

@Injectable()
export class LocationService {

  businessId: string;
  locationId: string;

  businessCollectionPath: string;
  locationsCollectionPath: string;

  private locationsCollection: AngularFirestoreCollection<BusinessLocation>;
  locations: Observable<BusinessLocation[]>;
  reviews: Observable<Review[]>;
  reviewsArr: Review[];

  _favoriteLocation: string;
  _hasOnlineOrdering: boolean;

  locationChange: Subject<string> = new Subject();

  constructor(
    private afs: AngularFirestore,
    private authService: AuthenticationService
  ) {
    this.businessId = environment.businessId;
    this.businessCollectionPath = `businesses/${this.businessId}`;
    this.locationsCollectionPath = `${this.businessCollectionPath}/locations`;

    this._favoriteLocation = '';
    this._hasOnlineOrdering = true;

    this.locationsCollection = afs.collection<BusinessLocation>(this.locationsCollectionPath, ref => {
      return ref.where('isActive', '==', true)
    });
  }

  getLocations() {
    return this.afs.collection<BusinessLocation[]>(`businesses/${this.businessId}/locations`, ref => {
      return ref.where('isActive', '==', true)
    }).snapshotChanges().map((locations) => {
      return locations.map((location) => {
        const data = location.payload.doc.data() as BusinessLocation;
        const id = location.payload.doc.id;
        return { id, ...data };
      });
    });
  }

  locationsAsObservable(): Observable<BusinessLocation[]> {
    return this.locationsCollection.valueChanges();
  }

  locationsAsObservableWithId(): Observable<BusinessLocation[]> {
    let size = 0;
    return this.locationsCollection
    .snapshotChanges().map(actions => {
      return actions.map(a => {
          const data = a.payload.doc.data() as BusinessLocation;
          const id = a.payload.doc.id;
          const reviews = this.afs.collection<Review[]>(`${this.locationsCollectionPath}/${id}/reviews`)
          .snapshotChanges().map(xactions => {
            return xactions.map(xa => {
              const xdata = xa.payload.doc.data() as Review;
              const xid = xa.payload.doc.id;
              let user = null;
              size++;
              if (xdata.user) {
                user = this.afs.doc(`users/${xdata.user.id}`).valueChanges();
              }
              return { xid, ...xdata, user, size};
            });
          });
          return {id, ...data, reviews};
        });
    });
  }

  getMenuItems(): Observable<BusinessLocation[]> {
    return this.afs.collection<BusinessLocation[]>(`businesses/${this.businessId}/locations`)
      .snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as BusinessLocation;
          const id = a.payload.doc.id;
          const price = this.afs.collection(`businesses/${this.businessId}/locations/${id}/reviews`)
          .snapshotChanges().map(xaction => {
            return xaction.map(xa => {
              const xdata = xa.payload.doc.data() as Review;
              const xid = xa.payload.doc.id;
              return {xid, ...xdata};
            });
          });
          return {id, ...data, price};
        });
      });
  }

  saveFollowedLocations(locations) {
    return this.afs.doc(`businesses/${this.businessId}/members/${this.authService.currentUserId}`).update(locations);
  }

  set currentLocation(locationId) {
    this.locationId = locationId;
  }
  get currentLocation(): string {
    return this.locationId;
  }

  set favoriteLocation(locationId) {
    this._favoriteLocation = locationId;
  }
  get favoriteLocation() {
    return this._favoriteLocation;
  }

  set hasOnlineOrdering(flag) {
    this._hasOnlineOrdering = flag;
  }
  get hasOnlineOrdering() {
    return this._hasOnlineOrdering;
  }

  getLocationDetails(id): Observable<BusinessLocation> {
    return this.afs.doc<BusinessLocation>(`${this.locationsCollectionPath}/${id}`).valueChanges();
  }

  nextLocation(lId) {
    this.locationChange.next(lId);
  }

}
