import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { environment } from './../../environments/environment';

import { Article } from './../models/article';
import { User } from './../models/user';

@Injectable()
export class ArticleService {

  bId: string;

  constructor(
    private afs: AngularFirestore
  ) {
    this.bId = environment.businessId;
  }

  getArticlesWithId() {
    return this.afs.collection<Article[]>(`businesses/${this.bId}/articles`, (ref) => {
      return ref.orderBy('date', 'desc')
    }).snapshotChanges()
      .map((article) => {
        return article.map((a) => {
          const data = a.payload.doc.data() as Article;
          const id = a.payload.doc.id;
          let author = null;
          if (data.author) {
            author = this.afs.doc<User>(data.author.path).valueChanges();
          }
          return { id, ...data, author };
        });
      });
  }

  getArticle(artId) {
    return this.afs.doc(`businesses/${this.bId}/articles/${artId}`).snapshotChanges()
      .map((article) => {
        const data = article.payload.data() as Article;
        const id = article.payload.id;
        let author = null;
        if (data.author) {
          author = this.afs.doc<User>(data.author.path).valueChanges();
        }
        return { id, ...data, author };
      });
  }

}
