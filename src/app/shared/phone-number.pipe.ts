import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'phoneNumber'
})
export class PhoneNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    const phoneTest = new RegExp(/^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/);
    if ( typeof value === 'string') {
      value = value.trim();
    }
    const results = phoneTest.exec(value);
    if (results !== null && results.length > 8) {
      return '(' + results[3] + ') ' + results[4] + ' - ' + results[5] + (typeof results[8] !== 'undefined' ? ' x' + results[8] : '');
    } else {
      return value;
    }
  }

}
