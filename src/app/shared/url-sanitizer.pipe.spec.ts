import { UrlSanitizerPipe } from './url-sanitizer.pipe';
import { TestBed, inject } from '@angular/core/testing';
import { UrlSanitizerService } from './../services/url-sanitizer.service';

describe('UrlSanitizerPipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [UrlSanitizerService] });
  });
  it('should create an instance', () => {
    const urlS = TestBed.get(UrlSanitizerService);
    const pipe = new UrlSanitizerPipe(urlS);
    expect(pipe).toBeTruthy();
  });
});
