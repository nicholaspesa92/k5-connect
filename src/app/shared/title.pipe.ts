import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'title'
})
export class TitlePipe implements PipeTransform {

 // public pieces: any;

  transform(value: any) {
    if (value) {
      value = value.toLowerCase();
      const pieces = value.split(' ');
      for (let i = 0; i < pieces.length; i++) {
        pieces[i] = pieces[i].charAt(0).toUpperCase() + pieces[i].slice(1);
      }
      return pieces.toString().replace(/,/g, ' ');
    }
  }
}
