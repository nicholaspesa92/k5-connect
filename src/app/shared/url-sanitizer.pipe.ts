import { Pipe, PipeTransform } from '@angular/core';
import { UrlSanitizerService } from './../services/url-sanitizer.service';

@Pipe({
  name: 'urlSanitizer'
})
export class UrlSanitizerPipe implements PipeTransform {

  states: any[];

  constructor(
    private urlS: UrlSanitizerService
  ) {
    this.states = [
      'style',
      'resource'
    ];
  }

  transform(value: any, args?: any): any {
    if (value) {
      if (args) {
        switch (args) {
          case this.states[0]:
            return this.urlS.getSanitizedStyle(value);
          case this.states[1]:
            return this.urlS.getSanitizedUrl(value);
          default:
            return value;
        }
      }
    }
  }

}
