import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterMenu',
  pure: false
})
export class FilterMenuPipe implements PipeTransform {
  sativa: boolean;
  indica: boolean;
  hybrid: boolean;

  transform(items: any[], field: string, value: any): any[] {
    if (items) {
      if (value) {
        const types = [];
        for (let i = 0; i < Object.keys(value).length; i++) {
          if (value[Object.keys(value)[i]] === true) {
            types.push(Object.keys(value)[i]);
          }
        }
        return items.filter(it => {
          return types.indexOf(it.type) > -1
        });
        // return items.filter(item => item.type === types.indexOf(item.type));
      }
      return items;
    }
    return [];
  }
}
