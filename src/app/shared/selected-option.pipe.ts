import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectedOption',
  pure: false
})
export class SelectedOptionPipe implements PipeTransform {

  transform(items: Array<any>, option: boolean): Array<any> {
    return items.filter(item => item.selected === option);
  }

}
