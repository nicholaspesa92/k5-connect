import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'menuByCategory',
})
export class MenuByCategoryPipe implements PipeTransform {

  transform(items: Array<any>, category: boolean): Array<any> {
    if (items) {
      return items.filter(item => item.category === category);
    }
  }

}
