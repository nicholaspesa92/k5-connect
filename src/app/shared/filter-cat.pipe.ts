import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCat'
})
export class FilterCatPipe implements PipeTransform {

  transform(items: any, filters?: any): any {
    if (items) {
      if (filters) {
        console.log('has-filters');
        return items.filter(it => {
          return (this.query(filters.categories, it, 'category') &&
                  this.query(filters.types, it, 'type'))
        });
      }
      return items;
    }
    return [];
  }

  query(array, item, type) {
    if (array.length === 0) {
      return array;
    } else {
      return array.indexOf(item[type]) >= 0;
    }
  }

}
