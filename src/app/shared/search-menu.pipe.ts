import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'searchMenu',
  pure: false
})

@Injectable()
export class SearchMenuPipe implements PipeTransform {

  transform(items: any[], field: string, value: string): any[] {
    if (items) {
      if (value) {
        // return items.filter(it => it[field].toLowerCase().indexOf(value.toLowerCase()) > -1);
        return items.filter((item) => {
          const x = {
            name: item.name ? item.name : '',
            vendor: item.vendor ? item.vendor : ''
          };
          return JSON.stringify(x).toLowerCase().indexOf(value.toLowerCase()) > -1
        });
      }
      return items;
    }
    return [];
  }

  abc(a, b) {
    return a.name < b.name ? -1 : 1;
  }

}
