import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FocusDirective } from '../focus.directive';
import { GroupDatePipe } from './group-date.pipe';
import { TabInputDirective } from './../directives/tab-input.directive';
import { PhoneInputDirective } from './../directives/phone-input.directive';
import { FilterCatPipe } from './filter-cat.pipe';
import { FilterTypePipe } from './filter-type.pipe';
import { GroupByPipe } from './group-by.pipe';
import { SearchMenuPipe } from './search-menu.pipe';
import { UrlSanitizerPipe } from './url-sanitizer.pipe';
import { VariationsPipe } from './../pipes/variations.pipe';
import { CategoryMatchPipe } from './../pipes/category-match.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FocusDirective,
    GroupDatePipe,
    TabInputDirective,
    PhoneInputDirective,
    FilterCatPipe,
    FilterTypePipe,
    GroupByPipe,
    SearchMenuPipe,
    UrlSanitizerPipe,
    VariationsPipe,
    CategoryMatchPipe
  ],
  exports: [
    FocusDirective,
    GroupDatePipe,
    TabInputDirective,
    PhoneInputDirective,
    FilterCatPipe,
    FilterTypePipe,
    GroupByPipe,
    SearchMenuPipe,
    UrlSanitizerPipe,
    VariationsPipe,
    CategoryMatchPipe
  ]
})
export class SharedModule { }
