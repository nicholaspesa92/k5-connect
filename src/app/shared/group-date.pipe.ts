import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'groupDate'
})
export class GroupDatePipe implements PipeTransform {

  transform(value: Array<any>): Array<any> {
    if (value) {
        const groupedObj = value.reduce((prev, cur) => {
        if (!prev[cur['createdAt'].toDateString()]) {
          // they are different
          prev[cur['createdAt'].toDateString()] = [cur];
        } else {
          // they are the same
          prev[cur['createdAt'].toDateString()].push(cur);
        }
        return prev;
      }, {});
      return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
    }
  }
}
