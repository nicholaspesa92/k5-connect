import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterType'
})
export class FilterTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      if (args) {
        let construct = [];
        for (let i = 0; i < args.length; i++) {
          const x = value.filter(item => item.type === args[i]);
          construct = construct.concat(x);
        }
        return construct;
      }
      return value;
    }
    return [];
  }

}
