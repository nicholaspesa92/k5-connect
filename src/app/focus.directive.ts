import { Directive, AfterViewInit, ElementRef, Renderer, ChangeDetectorRef, Inject } from '@angular/core';

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective implements AfterViewInit {

  constructor(
    public renderer: Renderer,
    @Inject(ElementRef) public elementRef: ElementRef,
    private cdr: ChangeDetectorRef
  ) {}

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(
      this.elementRef.nativeElement, 'focus', []);
      // this.cdr.detectChanges();
  }

}
