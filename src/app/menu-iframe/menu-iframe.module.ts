import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';

import { MenuIframeComponent } from './menu-iframe.component';
import { DesignOneComponent } from './design-one/design-one.component';
import { ViewItemComponent } from './view-item/view-item.component';
import { OverlayWrapperComponent } from './design-one/design-one.component';
import { ExternalSignupComponent } from './external-signup/external-signup.component';

import { MenuService } from './../services/menu.service';
import { AuthenticationService } from './../services/authentication.service';
import { LocationService } from './../services/location.service';
import { IframeOverlayService } from './../services/iframe-overlay.service';
import { UrlSanitizerService } from './../services/url-sanitizer.service';
import { OrderService } from './../services/order.service';
import { WindowService } from './../services/window.service';
import { PaginationService } from './../services/pagination.service';

import { GroupByPipe } from './../shared/group-by.pipe';
import { SearchMenuPipe } from './../shared/search-menu.pipe';

import { TextMaskModule } from 'angular2-text-mask';

// Material Imports //
import {
  MatSnackBarModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatDialogModule,
  MatTabsModule,
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
// End Material Imports //

import {
  OverlayModule
} from '@angular/cdk/overlay';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

const routes: Routes = [
  {
    path: '',
    component: MenuIframeComponent,
    children: [
      { path: '', redirectTo: 'ext-signup', pathMatch: 'full' },
      { path: 'design-one', component: DesignOneComponent },
      { path: 'ext-signup', component: ExternalSignupComponent }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatCardModule,
    CloudinaryModule.forRoot(cloudinary, cloudinaryConfig),
    MatDialogModule,
    OverlayModule,
    MatProgressSpinnerModule,
    TextMaskModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule
  ],
  exports: [RouterModule],
  declarations: [
    MenuIframeComponent,
    DesignOneComponent,
    ViewItemComponent,
    OverlayWrapperComponent,
    ExternalSignupComponent
  ],
  providers: [
    MenuService,
    AuthenticationService,
    LocationService,
    IframeOverlayService,
    UrlSanitizerService,
    OrderService,
    WindowService,
    PaginationService
  ],
  entryComponents: [
    ViewItemComponent,
    OverlayWrapperComponent
  ]
})
export class MenuIframeModule { }
