import { Component, OnInit, OnDestroy, Optional, Inject } from '@angular/core';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.scss']
})
export class ViewItemComponent implements OnInit, OnDestroy {

  item: any;

  constructor(
  ) { }

  ngOnInit() {
    console.log('init');
    // this.item = this.ifs.getItem();
  }
  ngOnDestroy() {
    console.log('destroyed');
  }

  close() {
  }

}
