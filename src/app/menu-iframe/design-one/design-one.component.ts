import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs-compat'
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { MatSelect, MatDialog } from '@angular/material';

import { MenuService } from './../../services/menu.service';
import { LocationService } from './../../services/location.service';
import { AuthenticationService } from './../../services/authentication.service';
import { IframeOverlayService } from './../../services/iframe-overlay.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { OrderService } from './../../services/order.service';
import { WindowService } from './../../services/window.service';
import { PaginationService } from './../../services/pagination.service';

import { Menu } from './../../models/menu';
import { BusinessLocation } from './../../models/businesslocation';
import { Order } from './../../models/order';

import { environment } from './../../../environments/environment';
import * as firebase from 'firebase';

import { ViewItemComponent } from './../view-item/view-item.component';

const SMALL_WIDTH_BREAKPOINT = 600;
const PHONE_REG_EX = /^((\+1)|1)? ?\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})( ?(ext\.? ?|x)(\d*))?$/;
const PHONE_MASK = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

@Component({
  selector: 'app-design-one',
  templateUrl: './design-one.component.html',
  styleUrls: ['./design-one.component.scss']
})
export class DesignOneComponent implements OnInit {

  bId: string;
  lId: string;

  menu$: any|Observable<Menu[]>;
  menu: any|Menu[];
  bareMenu: Menu[];
  filteredMenu: any;

  categoriesCtrl: FormControl;
  categories$: Observable<any>;
  categories: any[];

  typesCtrl: FormControl;
  types$: Observable<any>;
  types: any[];

  locationsCtrl: FormControl;
  locations$: Observable<BusinessLocation[]>;
  locations: BusinessLocation[];

  searchQuery: string;

  filters: any;

  weedPlaceholder: string;

  stretchInput: boolean;
  showFilters: boolean;

  cardImgH: any;
  cardImgW: any;

  top: any|number;
  height: number;
  fabTop: any|number;
  currentItem: any|Menu;
  firstPrice: any;
  sortedPrices: any;

  dummyItem: any;
  modalIndex: number;
  orderItems: any;
  selectedPrice: number;
  quantity: number;
  total: number;

  order$: Observable<Order>;
  order: Order;

  pForm: FormGroup;
  vForm: FormGroup;
  phoneStep: number;

  aUser$: any;
  aUser: any;
  fUser: any;

  window: any;
  vHash: any;
  vCode: any;

  progress: boolean;

  subs: any;

  loading: boolean;
  done: boolean;

  constructor(
    private ms: MenuService,
    private ls: LocationService,
    private as: AuthenticationService,
    private aRoute: ActivatedRoute,
    private dialog: MatDialog,
    private ifs: IframeOverlayService,
    private urlS: UrlSanitizerService,
    private fb: FormBuilder,
    private os: OrderService,
    private ws: WindowService,
    public ps: PaginationService
  ) {
    this.bId = environment.businessId;
    this.weedPlaceholder = environment.weedPlaceholder;
    this.menu = [];
    this.filteredMenu = [];
    this.locations = [];
    this.categories = [];
    this.types = [];
    this.stretchInput = false;
    this.showFilters = false;
    this.categoriesCtrl = new FormControl();
    this.typesCtrl = new FormControl();
    this.locationsCtrl = new FormControl();
    this.filters = {
      categories: [],
      types: []
    };
    this.top = 1 + 'px';
    this.fabTop = 1 + 'px';
    this.height = 39 + 32;
    this.currentItem = null;
    this.sortedPrices = [];
    this.modalIndex = 0;
    this.orderItems = [];
    this.selectedPrice = 0;
    this.quantity = 1;
    this.total = 0;
    this.phoneStep = 1;
    this.progress = false;
    this.loading = true;
    this.done = false;
  }

  ngOnInit() {
    this.cardImgW = (window.innerWidth - 16);
    this.top = 1 + 'px';
    this.fabTop = 1 + 'px';
    this.buildPForm();
    this.buildVForm();
    window.addEventListener('message', ((e) => {
      this.top = e.data.top < 0 ? 0 : (e.data.top);
      if (e.data.rect) { this.fabTop = (e.data.rect.height - 72); }
      if (e.data.height) { this.height = (e.data.height - 48) };
    }));

    this.locations$ = this.ls.locationsAsObservableWithId();
    this.locations$.subscribe((locations) => {
      this.locations = locations;
      if (this.aRoute.snapshot.queryParams['location'] && !localStorage.getItem('menulId')) {
        this.lId = this.aRoute.snapshot.queryParams['location']
      } else if (localStorage.getItem('menulId') && !this.aRoute.snapshot.queryParams['location']) {
        this.lId = localStorage.getItem('menulId');
      } else if (this.aRoute.snapshot.queryParams['location'] && localStorage.getItem('menulId')) {
        this.lId = this.aRoute.snapshot.queryParams['location'];
      } else {
        this.lId = locations[0].id
      }
      this.startsSubs();
    });
  }

  startsSubs() {
    const path = `businesses/${this.bId}/locations/${this.lId}/menu`;
    this.aUser$ = this.as.user;
    this.types$ = this.as.getCurrentBusinessTypes();
    this.categories$ = this.as.getCurrentBusinessCategories();
    this.filteredMenu = [];
    this.loading = true;
    this.subs = Observable.combineLatest(this.aUser$, this.types$, this.categories$);
    this.subs.subscribe(([u, t, c]) => {
      this.types = [];
      this.categories = [];
      this.aUser = u;
      t.map((type) => {
        this.types.push(type.slug);
      });
      /*
      c.map((cat) => {
        this.categories.push(cat.slug);
      });
      /*
      this.bareMenu = m;
      this.menu = this.ms.transformArray(m, 'category');
      this.filteredMenu = this.ms.transformArray(this.transform(this.bareMenu, this.filters), 'category');
      this.filteredMenu.map((item) => {
        this.categories.push(item.key);
      });
      this.loading = false;
      console.log('Loading', this.loading);
      */
    });
    this.ps.init(path, 'category', {});
    this.ps.data.subscribe((m) => {
      console.log(m);
      this.categories = [];
      this.bareMenu = m;
      this.menu = this.ms.transformArray(m, 'category');
      this.filteredMenu = this.ms.transformArray(this.transform(this.bareMenu, this.filters), 'category');
      this.filteredMenu.map((item) => {
        this.categories.push(item.key);
      });
      this.loading = false;
    });
    this.ps.done.subscribe((flag) => {
      this.done = flag;
    })
  }

  catsSelect(event) {
    this.filters.categories = this.categoriesCtrl.value;
    this.filteredMenu = this.ms.transformArray(this.transform(this.bareMenu, this.filters), 'category');
  }

  typesSelect(event) {
    this.filters.types = this.typesCtrl.value;
    this.filteredMenu = this.ms.transformArray(this.transform(this.bareMenu, this.filters), 'category');
  }

  async locationsSelect(event) {
    console.log(event);
    if (event.value === this.lId) { return; }
    this.ps.reset();
    localStorage.menulId = event.value;
    this.lId = event.value;
    this.startsSubs();
  }

  transform(items: any, filters?: any): any {
    if (items) {
      if (filters) {
        return items.filter(it => {
          return (this.query(filters.categories, it, 'category') &&
                  this.query(filters.types, it, 'type'))
        });
      }
      return items;
    }
    return [];
  }

  query(array, item, type) {
    if (array.length === 0) {
      return array;
    } else {
      return array.indexOf(item[type]) >= 0;
    }
  }

  applyCategoryFilters() {
    const x = this.categoriesCtrl.value;
    if (x.length === 0) { return this.ms.transformArray(this.bareMenu, 'category'); }
    let construct = [];
    for (let i = 0; i < x.length; i++) {
      const y = this.bareMenu.filter(item => item.category === x[i]);
      construct = construct.concat(y);
    }
    return this.ms.transformArray(construct, 'category');
  }

  applyTypesFilters() {
    const x = this.typesCtrl.value;
    if (x.length === 0) { return this.ms.transformArray(this.bareMenu, 'category'); }
    let construct = [];
    for (let i = 0; i < x.length; i++) {
      const y = this.bareMenu.filter(item => item.type === x[i]);
      construct = construct.concat(y);
    }
    return this.ms.transformArray(construct, 'category');
  }

  getUrl(url) {
    if (url === null || url === '' || url === undefined) { return this.weedPlaceholder; }
    return url;
  }

  getFirstPrice(variations) {
    return this.ms.sortAndOrderVariations(variations)[0];
  }

  onFocus() {
    this.stretchInput = true;
  }
  onFocusOut() {
    this.stretchInput = false;
  }
  onBlur() {
    this.stretchInput = true;
  }
  onBlurOut() {
    this.stretchInput = false;
  }

  isScreenSmall(): boolean {
    return window.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`).matches;
  }

  view(item, e) {
    this.currentItem = item;
    this.setFirstPrice(item.variations);
    item.variations && item.saleVariations ?
      this.sortPricesAndMatchOnSale(item.variations, item.saleVariations) :
      this.setFirstPrice(item.varitaions);
  }

  add() {
    const i = {
      name: this.currentItem.name,
      unit: this.sortedPrices[this.selectedPrice].name,
      price: this.sortedPrices[this.selectedPrice].price,
      quantity: this.quantity
    };
    this.orderItems.push(i);
    this.calculateTotal(this.orderItems);
  }
  remove(item) {
    const x = this.orderItems.indexOf(item);
    this.orderItems.splice(x, 1);
    this.calculateTotal(this.orderItems);
  }

  addQ(item) {
    item.quantity += 1;
    this.calculateTotal(this.orderItems);
  }
  subQ(item) {
    item.quantity -= 1;
    this.calculateTotal(this.orderItems);
  }

  calculateTotal(items) {
    this.total = 0;
    items.map((item) => {
      this.total += (item.price * item.quantity);
    });
  }

  close(e?) {
    e ? e.stopPropagation() : console.log('no-event');
    this.modalIndex = 0;
    this.selectedPrice = 0;
    this.quantity = 1;
    this.currentItem = null;
  }

  chill(e) {
    e.stopPropagation();
  }

  sortPricesAndMatchOnSale(variations, saleVariations) {
    this.sortedPrices = this.ms.sortAndOrderVariations(variations);
    for (let i = 0; i < this.sortedPrices.length; i++) {
      if (saleVariations[this.sortedPrices[i].name] > 0) {
        this.sortedPrices[i].price = saleVariations[this.sortedPrices[i].name];
      }
      this.sortedPrices[i].name = this.replace(this.sortedPrices[i].name);
    }
  }

  setFirstPrice(variations) {
    if (variations === null || variations === undefined) { this.firstPrice = {}; return; }
    this.sortedPrices = this.ms.sortAndOrderVariations(variations);
    this.firstPrice = this.sortedPrices[0];
    for (let i = 0; i < this.sortedPrices.length; i++) {
      this.sortedPrices[i].name = this.replace(this.sortedPrices[i].name);
    }
  }

  replace(string) {
    if (string.indexOf('-') > -1) {
      return this.replaceDash(string);
    } else if (string.indexOf('_') > -1) {
      return this.replaceUnderscore(string);
    } else {
      return string;
    }
  }
  replaceDash(string) {
    return string.replace(/-/g, ' ');
  }
  replaceUnderscore(string) {
    return string.replace(/_/g, ' ');
  }

  sanitizeBackground(url) {
    if (url === null || url === '' || url === undefined) { return this.urlS.getSanitizedStyle(this.weedPlaceholder); }
    return this.urlS.getSanitizedStyle(url);
  }

  buildPForm() {
    this.pForm = this.fb.group({
      phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_REG_EX)]]
    });
  }

  buildVForm() {
    this.vForm = this.fb.group({
      1: ['', Validators.required],
      2: ['', Validators.required],
      3: ['', Validators.required],
      4: ['', Validators.required],
      5: ['', Validators.required],
      6: ['', Validators.required]
    });
  }

  handleCart() {
    if (this.aUser.isAnonymous) {
      this.gotoLogin();
    } else {
      this.getFUserAndSendOrder();
    }
  }

  gotoLogin() {
    this.modalIndex = 2;
    setTimeout(() => {
      this.window = this.ws.windowRef;
      this.window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
      this.window.recaptchaVerifier.render();
    }, 200);
  }

  submitCart(uid, fUser) {
    const o = {
      userId: uid,
      user: fUser,
      createdAt: new Date(),
      products: this.orderItems,
      total: this.total,
      status: 'new',
      canceled: false,
      archived: false
    };
    this.os.createOrder(this.lId, o)
      .then((res) => {
        this.orderItems = [];
        this.calculateTotal(this.orderItems);
        this.getOrderAfterCreation(res.id);
      }).catch((error) => {
      });
  }

  async sendVCode() {
    const number = '+1' + this.trimNumber(this.pForm.value.phoneNumber);
    const appVerifier = this.window.recaptchaVerifier;
    try {
      this.vHash = await this.as.phoneLogin(number, appVerifier);
      this.phoneStep = 2;
    } catch (err) {
      this.stop();
    }
  }

  async verifyCode() {
    const vCode = this.concatCode();
    try {
      this.aUser = await this.as.phoneVerify(this.vHash, vCode);
      this.getFUserAndSendOrder();
    } catch (err) {
      switch (err.code) {
        case 'auth/invalid-verification-code':
          break;
      }
    }
  }

  getFUserAndSendOrder() {
    this.start();
    const fSub = this.as.getCurrentUserFromFirebase(this.aUser.uid).first();
    fSub.subscribe((fuser) => {
      this.submitCart(this.aUser.uid, fuser);
    });
  }

  getOrderAfterCreation(id) {
    this.order$ = this.os.getOrder(this.lId, id);
    this.order$.subscribe((order) => {
      this.order = order;
      this.stop();
      this.modalIndex = 3;
    });
  }

  concatCode() {
    const x = this.vForm.value;
    let code = '';
    for (let i = 0; i < 6; i++) {
      code += x[i + 1];
    }
    return code;
  }

  trimNumber(number) {
    return number.replace(/[^0-9]/g, '');
  }

  start() {
    this.progress = true;
  }
  stop() {
    this.progress = false;
  }

  handleTop(x) {
    return x < 0 ? 0 : x;
  }

}

@Component({
  selector: 'app-overlay-wrapper',
  template: `<app-view-item></app-view-item>`
})
export class OverlayWrapperComponent {

}
