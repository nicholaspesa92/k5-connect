import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DesignOneComponent } from './design-one.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupByPipe } from './../../shared/group-by.pipe';
import { SearchMenuPipe } from './../../shared/search-menu.pipe';
import {
  MatIconModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTabsModule,
  MatDialogModule
} from '@angular/material';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { MenuService } from './../../services/menu.service';
import { AuthenticationService } from './../../services/authentication.service';
import { LocationService } from './../../services/location.service';
import { IframeOverlayService } from './../../services/iframe-overlay.service';
import { UrlSanitizerService } from './../../services/url-sanitizer.service';
import { OrderService } from './../../services/order.service';
import { WindowService } from './../../services/window.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from './../../../environments/environment';

import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};
export const cloudinaryConfig: CloudinaryConfiguration = {
  cloud_name: 'cconnect',
  secure: true
};

describe('DesignOneComponent', () => {
  let component: DesignOneComponent;
  let fixture: ComponentFixture<DesignOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatSelectModule,
        MatSnackBarModule,
        MatTabsModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        CloudinaryModule.forRoot(cloudinary, cloudinaryConfig),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule
      ],
      declarations: [
        DesignOneComponent,
        GroupByPipe,
        SearchMenuPipe
      ],
      providers: [
        MenuService,
        AuthenticationService,
        LocationService,
        IframeOverlayService,
        UrlSanitizerService,
        OrderService,
        WindowService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
