import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './../../services/authentication.service';
import { WindowService } from './../../services/window.service';
import 'rxjs-compat'
import { MatSnackBar } from '@angular/material';
import { AngularFirestore } from 'angularfire2/firestore';

import { environment } from './../../../environments/environment';
import * as firebase from 'firebase';

const PHONE_MASK = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

@Component({
  selector: 'app-external-signup',
  templateUrl: './external-signup.component.html',
  styleUrls: ['./external-signup.component.scss']
})
export class ExternalSignupComponent implements OnInit, AfterViewInit {

  bId: string;
  uid: string;
  tab: number;

  authForm: FormGroup;
  vHash: any;
  vCode: any;
  step: number;
  mask: any;
  char: any;
  windowRef: any;
  requesting: boolean;

  user$: Observable<any>;
  user: any;
  authuser: any;

  member$: Observable<any>;
  member: any;

  detailsForm: FormGroup;

  joinedSub: any;

  bSettings$: Observable<any>;
  bSettings: any;

  biz$: Observable<any>;
  biz: any;

  business: any;

  constructor(
    private auth: AuthenticationService,
    private ws: WindowService,
    private fb: FormBuilder,
    private sb: MatSnackBar,
    private afs: AngularFirestore
  ) {
    this.bId = environment.businessId;
    this.step = 0;
    this.mask = PHONE_MASK;
    this.char = '\u2000';
    this.requesting = false;
    this.tab = 0;
  }

  ngOnInit() {
    this.buildAuthForm();
    this.buildDetailsForm();
    this.bSettings$ = this.auth.getCurrentBusinessSettings();
    this.biz$ = this.auth.getCurrentBusinessDoc();
    this.business = Observable.combineLatest(this.bSettings$, this.biz$);
    this.business.subscribe(([settings, bDoc]) => {
      this.bSettings = settings;
      this.biz = bDoc;
      console.log(this.biz);
    });
  }

  ngAfterViewInit() {
    if (this.tab === 0) {
      this.windowRef = this.ws.windowRef;
      this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        'size': 'invisible'
      });
      this.windowRef.recaptchaVerifier.render();
    }
  }

  buildAuthForm() {
    this.authForm = this.fb.group({
      phoneNumber: ['', Validators.required],
      vCode: ['', Validators.required]
    });
  }

  buildDetailsForm() {
    this.detailsForm = this.fb.group({
      displayName: ['', Validators.required],
      email: ['', Validators.required],
      birthday: ['', Validators.required]
    });
  }

  async sendCode() {
    this.requesting = true;
    const n = `+1${this.trimNumber(this.authForm.value.phoneNumber)}`;
    const appVerifier = this.windowRef.recaptchaVerifier;
    try {
      this.vHash = await this.auth.phoneLogin(n, appVerifier);
      this.requesting = false;
      this.step = 1;
    } catch (err) {
      this.requesting = false;
      console.log(err);
    }
  }

  async verifyCode() {
    this.requesting = true;
    const vCode = this.authForm.value.vCode;
    try {
      this.authuser = await this.auth.phoneVerify(this.vHash, vCode);
      this.uid = this.authuser.uid;
      this.user$ = this.auth.getCurrentUserFromFirebase(this.authuser.uid);
      this.member$ = this.auth.isMember(this.authuser.uid);
      this.joinedSub = Observable.combineLatest(this.user$, this.member$).first()
        .subscribe(([u, m]) => {
          console.log(u, m);
          this.requesting = false;
          if (!m.exists) {
            // Create Member
            this.tab = 1;
          } else if (m.exists) {
            if (!m.appInstalled) {
              // Get User Info
              this.tab = 1;
            } else {
              this.tab = 3;
            }
          }
        });
    } catch (err) {
      this.requesting = false;
      console.log(err);
      switch (err.code) {
        case 'auth/invalid-verification-code':
          this.snack('Invalid Verification Code', '', { duration: 3000 });
          break;
        default:
          this.snack('There was an error, Please try again soon', 'OK', { duration: 3000 });
          break;
      }
    }
  }

  async saveDetails() {
    this.requesting = true;
    const d = this.detailsForm.value;
    const uData = {
      displayName: d.displayName,
      email: d.email,
      birthday: d.birthday
    };
    const mData = {
      user: this.afs.doc(`users/${this.uid}`).ref,
      appInstalled: true
    };
    try {
      await this.auth.updateUserDisplayName(d.displayName);
      if (!this.authuser.emailVerified) { await this.auth.updateUserEmail(d.email).catch(error => console.log(error)); }
      await this.auth.saveUser(uData);
      await this.auth.setMemberDataX(this.uid, mData);
      this.requesting = false;
      this.tab = 2;
    } catch (err) {
      this.requesting = false;
      console.log(err);
      switch (err.code) {
        default:
          this.snack('There was an error, Please try again soon', 'OK', { duration: 3000 });
          break;
      }
    }
  }

  trimNumber(number) {
    return number.replace(/[^0-9]/g, '');
  }

  snack(m, a, c) {
    this.sb.open(m, a, c);
  }

}
