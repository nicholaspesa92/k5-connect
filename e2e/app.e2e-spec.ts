import { ConnectConsumerPage } from './app.po';

describe('connect-consumer App', () => {
  let page: ConnectConsumerPage;

  beforeEach(() => {
    page = new ConnectConsumerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
